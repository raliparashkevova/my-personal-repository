package com.springresttemplate.services;

import com.springresttemplate.models.BeerDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class BeersServiceImpl implements BeerService{


    @Override
    public BeerDto getBeerById(UUID beerId) {
        return BeerDto.builder().id(UUID.randomUUID())
                .beerName("Galaxy Cat")
                .beerStyle("Pale Ale")
                .build();
    }

    @Override
    public BeerDto saveNewBeer(BeerDto beerDto) {
        return BeerDto.builder().id(UUID.randomUUID()).build();
    }
}
