package com.vendingmachineapp.models;

public enum Coin {

    TEN(0.10),
    TWENTY(0.20),
    FIFTY(0.50),
    ONE(1.00),
    TWO(2.00);
    private double value;

    Coin(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {

        switch (this) {
            case TEN:
                return "0.10st";
            case TWENTY:
                return "0.20st";
            case FIFTY:
                return "0.50st";
            case ONE:
                return "1.00lv";
            case TWO:
                return "2.00lv";
            default:
                throw new UnsupportedOperationException("This coin is not supported by Fresher");

        }
    }
}
//10st, 20st, 50st, 1lv, 2lv