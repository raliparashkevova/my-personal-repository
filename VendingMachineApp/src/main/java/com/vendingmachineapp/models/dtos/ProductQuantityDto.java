package com.vendingmachineapp.models.dtos;

public class ProductQuantityDto {

    private int quantity;

    public ProductQuantityDto() {
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
