package com.vendingmachineapp.models.dtos;

public class ProductDto {

    private String type;
    private double price;
    private int quantity;

    public ProductDto(String type, double price) {
        this.type = type;
        this.price = price;
    }

    public ProductDto() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
