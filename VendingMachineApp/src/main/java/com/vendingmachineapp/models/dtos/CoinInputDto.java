package com.vendingmachineapp.models.dtos;

import java.util.List;

public class CoinInputDto {

    private List<Double> quantity;


    public CoinInputDto() {
    }

    public List<Double> getQuantity() {
        return quantity;
    }

    public void setQuantity(List<Double> quantity) {
        this.quantity = quantity;
    }
}
