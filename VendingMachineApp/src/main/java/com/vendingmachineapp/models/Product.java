package com.vendingmachineapp.models;

import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class Product {

    private int id;
    private String type;
    private double price;


    public Product() {
    }

    public Product(int id, String type, double price) {
        this.id = id;
        this.type = type;
        this.price = price;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 && Objects.equals(type, product.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, price);
    }
}
