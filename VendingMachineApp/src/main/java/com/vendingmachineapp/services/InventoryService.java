package com.vendingmachineapp.services;

import com.vendingmachineapp.models.Product;

import java.util.Map;

public interface InventoryService {

    void add(Product product, int quantity);

    void updateProduct(Product product, int quantity);

    void removeProduct(Product product);

    Map<Product, Integer> getAll();


    Product getById(int id);

}
