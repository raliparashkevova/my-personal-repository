package com.vendingmachineapp.services;

import com.vendingmachineapp.exceptions.EntityNotFoundException;
import com.vendingmachineapp.exceptions.LimitExceededException;
import com.vendingmachineapp.exceptions.NotSufficientSumException;
import com.vendingmachineapp.models.Coin;
import com.vendingmachineapp.models.Product;
import com.vendingmachineapp.repositories.InventoryRepository;
import com.vendingmachineapp.repositories.VendingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendingServiceImpl implements VendingService {

    private final VendingRepository vendingRepository;
    private final InventoryRepository inventoryRepository;

    @Autowired
    public VendingServiceImpl(VendingRepository vendingRepository, InventoryRepository inventoryRepository) {
        this.vendingRepository = vendingRepository;
        this.inventoryRepository = inventoryRepository;
    }

    /**
     * @param 'List of coins submitted by a customer'
     * @return 'List of all valid coins'
     */
    @Override
    public List<Double> insertCoin(List<Double> coin) {
        double sum = 0.0;
        for (Double userQuantity : coin) {
            if (userQuantity == Coin.TEN.getValue()) {
                vendingRepository.add(Coin.TEN.getValue());
            } else if (userQuantity == Coin.TWENTY.getValue()) {
                vendingRepository.add(Coin.TWENTY.getValue());
            } else if (userQuantity == Coin.FIFTY.getValue()) {
                vendingRepository.add(Coin.FIFTY.getValue());
            } else if (userQuantity == Coin.ONE.getValue()) {
                vendingRepository.add(Coin.ONE.getValue());
            } else if (userQuantity == Coin.TWO.getValue()) {
                vendingRepository.add(Coin.TWO.getValue());
            } else {
                throw new EntityNotFoundException
                        (String.format("Insert another kind of money as %s or %s or %s or %s or %s",
                                Coin.TEN.toString(), Coin.TWENTY.toString(), Coin.FIFTY.toString()
                                , Coin.ONE.toString(), Coin.TWO.toString()));
            }
        }

        return vendingRepository.getInsertedCoins();

    }

    /**
     * @param 'The product selected by a customer and List of his submitted coins'
     * @return All invalid coins, that are not sufficient for the product price'
     */
    @Override
    public void resetCoin(Product product, List<Double> insertedCoins) {

        double sum = insertedCoins.stream().mapToDouble(Double::doubleValue).sum();
        if (product.getPrice() > sum) {
            throw new NotSufficientSumException(sum);
        }
        insertedCoins.clear();
    }

    /**
     * @param 'The product selected by a customer and List of his submitted coins'
     * @return 'Successfully purchased product'
     * @return
     */
    @Override
    public Product buyProduct(Product product, List<Double> quantity) {
        double productPrice = product.getPrice();
        double sum = quantity.stream().mapToDouble(Double::doubleValue).sum();
        if (sum > productPrice) {
            throw new LimitExceededException();
        }

        inventoryRepository.reduceQuantityOfProduct(product);

        return product;
    }


}
