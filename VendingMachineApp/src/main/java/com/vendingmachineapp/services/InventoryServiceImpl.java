package com.vendingmachineapp.services;

import com.vendingmachineapp.exceptions.DuplicateEntityException;
import com.vendingmachineapp.exceptions.EntityNotFoundException;
import com.vendingmachineapp.exceptions.LimitExceededException;
import com.vendingmachineapp.models.Product;
import com.vendingmachineapp.repositories.InventoryRepository;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class InventoryServiceImpl implements InventoryService {

    private final InventoryRepository inventoryRepository;

    public InventoryServiceImpl(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    /**
     * @param 'Product that is pre-created'
     * @action 'The given product should ve added to inventory collection'
     */
    @Override
    public void add(Product product, int quantity) {

        if (inventoryRepository.getAll().size() >= 10) {
            throw new LimitExceededException();
        } else {
            inventoryRepository.add(product, quantity);
            System.out.println(String.format("The product with type %s and quantity has been added", product.getType(), quantity));
        }

    }

    /**
     * @param 'Product on which a field needs to be updated'
     */
    @Override
    public void updateProduct(Product product, int quantity) {
        boolean duplicateExists = true;
        try {

            Product productToUpdate = inventoryRepository.getById(product.getId());
            if (productToUpdate.getId() == product.getId()) {
                duplicateExists = false;
            }

        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Product", "type", product.getType());
        }

        inventoryRepository.update(product, quantity);
    }

    /**
     * @param "The Product that will be removed from inventory collection"
     */
    @Override
    public void removeProduct(Product product) {
        inventoryRepository.delete(product.getId());

    }

    /**
     * @return 'All available products in the vending machine'
     */
    @Override
    public Map<Product, Integer> getAll() {
        return inventoryRepository.getAll();
    }

    /**
     * @param 'The Id of given product'
     * @return 'Product available in the inventory'
     */
    @Override
    public Product getById(int id) {
        for (Map.Entry<Product, Integer> product : getAll().entrySet()) {
            if (product.getKey().getId() == id) {
                return product.getKey();
            }
        }
        throw new EntityNotFoundException(String.format("There is no such product with %d", id));
    }


}
