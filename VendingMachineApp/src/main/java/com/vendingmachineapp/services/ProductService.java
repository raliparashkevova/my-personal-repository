package com.vendingmachineapp.services;

import com.vendingmachineapp.models.Product;

import java.util.List;

public interface ProductService {


//    Product create(Product product);

    Product getById(int id);

    List<Product> getAll();


//    CRUD operations for the products (add, update, remove)
}
