package com.vendingmachineapp.services;

import com.vendingmachineapp.models.Product;

import java.util.List;

public interface VendingService {

//     void add(Product product);

    List<Double> insertCoin(List<Double> coin);

    void resetCoin(Product product, List<Double> insertedCoins);

    Product buyProduct(Product product, List<Double> quantity);

//    CRUD operations for the products (add, update, remove)

}
