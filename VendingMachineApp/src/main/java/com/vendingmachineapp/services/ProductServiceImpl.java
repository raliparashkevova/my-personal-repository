package com.vendingmachineapp.services;

import com.vendingmachineapp.models.Product;
import com.vendingmachineapp.repositories.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    /**
     * @param 'The id of given product'
     * @return 'Product found on the filed ID'
     */
    @Override
    public Product getById(int id) {
        return productRepository.getById(id);
    }

    /**
     * @return 'All available products'
     */
    @Override
    public List<Product> getAll() {
        return productRepository.getAll();
    }


}
