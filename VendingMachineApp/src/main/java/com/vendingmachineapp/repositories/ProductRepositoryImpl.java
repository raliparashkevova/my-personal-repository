package com.vendingmachineapp.repositories;

import com.vendingmachineapp.exceptions.EntityNotFoundException;
import com.vendingmachineapp.models.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private final List<Product> productList;
    private int id;


    public ProductRepositoryImpl() {
        this.productList = new ArrayList<>();
        createProducts();
    }


    @Override
    public List<Product> getAll() {
//        createProducts();
        return productList;
    }

    private void createProducts() {
        productList.add(new Product(++id, "Fresh Orange", 2.50));
        productList.add(new Product(++id, "Fresh Apple", 3.50));
        productList.add(new Product(++id, "Fresh Kiwi", 3.00));
        productList.add(new Product(++id, "Fresh Orange and Apple", 4.50));
        productList.add(new Product(++id, "Fresh Orange and Carrot", 5.00));
        productList.add(new Product(++id, "Fresh BeetRoot", 4.50));
        productList.add(new Product(++id, "Fresh Orange and Kiwi", 4.00));
        productList.add(new Product(++id, "Fresh Apple and BeetRoot", 5.00));
        productList.add(new Product(++id, "Fresh Orange and Apple and Carrot", 6.00));
        productList.add(new Product(++id, "Fresh Strawberry", 7.00));
    }

    @Override
    public Product getById(int id) {

        for (Product product : getAll()) {
            if (product.getId() == id) {
                return product;
            }


        }
        throw new EntityNotFoundException(String.format("There is no such product with %d", id));
    }
}
