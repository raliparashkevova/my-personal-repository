package com.vendingmachineapp.repositories;

import java.util.List;

public interface VendingRepository {
    void add(double value);

    List<Double> getInsertedCoins();
}
