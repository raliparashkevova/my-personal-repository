package com.vendingmachineapp.repositories;

import com.vendingmachineapp.models.Product;

import java.util.Map;

public interface InventoryRepository {
    Map<Product, Integer> getAll();

    Product getById(int id);

    void update(Product product, int quantity);

    void delete(int id);

    void add(Product product, int quantity);

    void reduceQuantityOfProduct(Product product);
}
