package com.vendingmachineapp.repositories;

import com.vendingmachineapp.models.Product;

import java.util.List;

public interface ProductRepository {

//    List<Product> getProducts();

//    void create();

    Product getById(int id);

//    void create();

    List<Product> getAll();
}
