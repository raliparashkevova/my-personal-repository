package com.vendingmachineapp.repositories;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class VendingRepositoryImpl implements VendingRepository {

    private List<Double> inputCoins;

    public VendingRepositoryImpl() {
        this.inputCoins = new ArrayList<>();
    }

    @Override
    public void add(double value) {
        inputCoins.add(value);
    }

    @Override
    public List<Double> getInsertedCoins() {
        return new ArrayList<>(inputCoins);
    }
}
