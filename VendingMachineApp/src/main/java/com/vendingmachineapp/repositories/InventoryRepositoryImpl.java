package com.vendingmachineapp.repositories;

import com.vendingmachineapp.exceptions.DuplicateEntityException;
import com.vendingmachineapp.exceptions.EntityNotFoundException;
import com.vendingmachineapp.exceptions.LimitExceededException;
import com.vendingmachineapp.models.Product;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class InventoryRepositoryImpl implements InventoryRepository {

    private final Map<Product, Integer> inventory;


    public InventoryRepositoryImpl() {
        this.inventory = new HashMap<>();
    }


    @Override
    public Map<Product, Integer> getAll() {

        return new HashMap<>(inventory);
    }

    @Override
    public Product getById(int id) {


        for (Map.Entry<Product, Integer> productIntegerEntry : inventory.entrySet()) {
            if (productIntegerEntry.getKey().getId() == id) {
                return productIntegerEntry.getKey();
            }
        }
        throw new EntityNotFoundException(String.format("There is no such product with %d", id));
    }

    @Override
    public void update(Product product, int quantity) {
        for (Map.Entry<Product, Integer> productIntegerEntry : inventory.entrySet()) {
            if (productIntegerEntry.getKey().getId() == product.getId()) {
                inventory.put(product, quantity);
            }
        }
    }

    @Override
    public void delete(int id) {

        Product product = getById(id);
        for (Map.Entry<Product, Integer> productIntegerEntry : inventory.entrySet()) {
            if (productIntegerEntry.getKey().getId() == product.getId()) {
                inventory.remove(product);
            } else {
                throw new EntityNotFoundException(String.format("There is no such product with %d", id));
            }
        }

    }

    @Override
    public void add(Product product, int quantity) {
        if (inventory.containsKey(product)) {
            throw new DuplicateEntityException("Product", "type", product.getType());
        }

        inventory.put(product, quantity);

    }

    @Override
    public void reduceQuantityOfProduct(Product product) {
        for (Map.Entry<Product, Integer> inventoryProduct : inventory.entrySet()) {
            if (inventoryProduct.getKey().getId() == product.getId()) {
                if (inventoryProduct.getValue() > 0) {
                    inventory.replace(product, inventory.get(product) - 1);
                } else {
                    throw new LimitExceededException();
                }
            }
        }
    }

}
