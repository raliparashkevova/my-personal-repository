package com.vendingmachineapp.exceptions;

public class LimitExceededException extends RuntimeException {


    public LimitExceededException() {
        super("The limit of products has been exceeded");
    }
}
