package com.vendingmachineapp.exceptions;

public class NotSufficientSumException extends RuntimeException {

    public NotSufficientSumException(double sum) {
        super(String.format("This sum %d is not sufficient for chosen product", sum));
    }
}
