package com.vendingmachineapp.controllers.rest;

import com.vendingmachineapp.exceptions.EntityNotFoundException;
import com.vendingmachineapp.exceptions.LimitExceededException;
import com.vendingmachineapp.exceptions.NotSufficientSumException;
import com.vendingmachineapp.models.Product;
import com.vendingmachineapp.models.dtos.CoinInputDto;
import com.vendingmachineapp.services.InventoryService;
import com.vendingmachineapp.services.VendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/vendingMachine")
public class VendingController {

    private final VendingService vendingService;
    private final InventoryService inventoryService;

    @Autowired
    public VendingController(VendingService vendingService, InventoryService inventoryService) {
        this.vendingService = vendingService;
        this.inventoryService = inventoryService;
    }

    @PostMapping("{id}")
    public void buyProduct(@PathVariable int id, @RequestBody CoinInputDto coinInputDto) {

        try {
            Product product = inventoryService.getById(id);
            List<Double> insertedCoins = vendingService.insertCoin(coinInputDto.getQuantity());
            vendingService.resetCoin(product, insertedCoins);
            vendingService.buyProduct(product, insertedCoins);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (LimitExceededException e) {
            throw new ResponseStatusException(HttpStatus.CREATED, e.getMessage());
        } catch (NotSufficientSumException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }


}
