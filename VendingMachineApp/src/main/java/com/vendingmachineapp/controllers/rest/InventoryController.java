package com.vendingmachineapp.controllers.rest;

import com.vendingmachineapp.controllers.mappers.ProductModelMapper;
import com.vendingmachineapp.exceptions.DuplicateEntityException;
import com.vendingmachineapp.exceptions.EntityNotFoundException;
import com.vendingmachineapp.exceptions.LimitExceededException;
import com.vendingmachineapp.models.Product;
import com.vendingmachineapp.models.dtos.ProductDto;
import com.vendingmachineapp.models.dtos.ProductQuantityDto;
import com.vendingmachineapp.services.InventoryService;
import com.vendingmachineapp.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/inventory")
public class InventoryController {

    private final InventoryService inventoryService;
    private final ProductService productService;
    private final ProductModelMapper productModelMapper;

    @Autowired
    public InventoryController(InventoryService inventoryService, ProductService productService, ProductModelMapper productModelMapper) {
        this.inventoryService = inventoryService;
        this.productService = productService;
        this.productModelMapper = productModelMapper;
    }

    @GetMapping()
    public Map<Product, Integer> getAll() {
        try {

            return inventoryService.getAll();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        try {

            return productService.getAll();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }
    @PostMapping("{id}")
    public Product add(@PathVariable int id, @RequestBody ProductQuantityDto productQuantityDto) {

        try {

            Product product = productService.getById(id);

            inventoryService.add(product, productQuantityDto.getQuantity());
            return product;
        } catch (LimitExceededException e) {
            throw new ResponseStatusException(HttpStatus.CREATED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Product update(
            @PathVariable int id, @RequestBody ProductDto productDto) {

        try {


            Product product = productModelMapper.fromDto(productDto, id);

            inventoryService.updateProduct(product, productDto.getQuantity());

            return product;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {

        try {

            Product product = productService.getById(id);
            inventoryService.removeProduct(product);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());

        }
    }
}