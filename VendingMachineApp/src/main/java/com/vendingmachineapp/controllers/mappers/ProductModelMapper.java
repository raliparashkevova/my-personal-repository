package com.vendingmachineapp.controllers.mappers;

import com.vendingmachineapp.models.Product;
import com.vendingmachineapp.models.dtos.ProductDto;
import com.vendingmachineapp.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductModelMapper {


    private final ProductRepository productRepository;

    @Autowired
    public ProductModelMapper(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public Product fromDto(ProductDto productDto) {
        Product product = new Product();
        dtoToObject(productDto, product);

        return product;
    }

    private void dtoToObject(ProductDto productDto, Product product) {

        product.setType(productDto.getType());
        product.setPrice(productDto.getPrice());

    }

    public Product fromDto(ProductDto productDto, int id) {
        Product product = productRepository.getById(id);
        dtoToObject(productDto, product);
        return product;
    }
}
