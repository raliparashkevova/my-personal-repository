# Vending Machine

This is a small web application that serves the needs of a every customer to buy your fresh juice until the day.

**Overview**

A simple web vending machine that offers different products. The machine hold up to 10 products of the same type, that
are 10 type of fresh juice every with own price. The vending machine accepts only coins of 10st, 20st, 50st, 1lv, 2lv;
The machine do not return any change. It is accessible by Postman as web client. The following actions are available:

**Inventory**
• CRUD operations for the products (add, update, remove) from inventory.

**Vending**

• Buy a product, after -- the customer has chosen his product. -- Insert coins -- If the collected coins are not
sufficient, they will be reset of their owner. -- If the collected coins are more than product price, the machine do not
return any change.

**Postman Guide:**
Example:
Request URL - localhost:8080/api/vendingMachine/1 with body quantity - array of coins. Request URL - localhost:
8080/api/inventory/1 with body "quantity"



