package inhetitance;

public interface Performer {

    Instrument getInstrument() ;

    void useInstrument(Instrument instrument);
    void perform();
}
