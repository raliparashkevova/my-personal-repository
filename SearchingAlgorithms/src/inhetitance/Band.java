package inhetitance;

import java.util.ArrayList;
import java.util.List;

public class Band implements Bandable,Performer{

    private Instrument instrument;


    private String name;
    private int numberOfMembers;

    public Band(String name, int numberOfMembers, Instrument instrument) {
        this.name = name;
        this.numberOfMembers = numberOfMembers;
        this.instrument = instrument;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfMembers() {
        return numberOfMembers;
    }

    public void setNumberOfMembers(int numberOfMembers) {
        this.numberOfMembers = numberOfMembers;
    }

    @Override
    public void perform(List<Performer> performers) {
        for (Performer performer : performers) {
            performer.perform();
        }
    }

    @Override
    public Instrument getInstrument() {
        return instrument;
    }

    @Override
    public void useInstrument(Instrument instrument) {
        System.out.printf("%s - %s%n",getClass().getSimpleName(),getInstrument().getType());
    }

    @Override
    public void perform() {
        useInstrument(instrument);
    }
}
