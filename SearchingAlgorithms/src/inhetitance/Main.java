package inhetitance;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        Band band = new Vocalist("Petar",10,new Instrument("Microphon"));

        Band drummer = new Drummer("Iva",11,new Instrument("Baraban"));

        Band guitarist = new Guitarist("Stefan",7,new Instrument("Guitar"));

        List<Performer> performers = new ArrayList<>();
        performers.add(band);
        performers.add(drummer);
        performers.add(guitarist);

        band.perform(performers);
     }
}
