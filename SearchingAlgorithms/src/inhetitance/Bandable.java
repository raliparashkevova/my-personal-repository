package inhetitance;

import java.util.List;

public interface Bandable {

    void perform(List<Performer> performers);
}
