package tasks;

import java.util.*;

public class ItemsStrings {

     /*
        There is a string "sdfgabcwetrrytruyrtuabcpotre!@#abcprtort".
        The task is to implement the following method:

        private HashMap<string, string> processString(string inputStr, string separator);

        The result needs to contain the following keys:
        Count : count all substrings (itemstrings)  infront of which there is a separator string (if xxx is the string and A is the separator here: xxxAxxxAxxxAxxx, you need to return 3);
        prefix : if any string exists before the first separator, please provide the text
        sortedItems : a string with all itemstrings concatinated in alphabetical order
        evenChars : a string with concatinated all even indexed chars (2,4,6,8,10th)

		notes:
			1. if there is no separator found in input string then the whole inputString is counted as 1 itemString
			2. zero length strings should not be includded in count
			3. prefix should not be includded in itemstrings
			4. prefix schould not be includded in count
			5. itemstrings schould be displayed with space (" ") between each of them in the output

		Implement all results display inside Main method in following format:

		Count: some number
		Prefix: some string
		sortedItems: some string
		evenChars: some string


		Example output when executed with inputString = "abcdefSEPgabcwetSEPsdsSEPdsfgSEPfro", separator = "SEP"

		Count: 4
		Prefix: abcdef
		sortedItems: dsfg fro gabcwet sds
		evenChars: aceSPaceSPdSPsgEfo
    */

    public static void main(String[] args) {
        String inputString = "sdfgabcwetrrytruyrtuabcpotre!@#abcprtort";
        ArrayList<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>();

        resultList.add(processString(inputString,"abc"));
        resultList.add(processString(inputString,"s"));
        resultList.add(processString(inputString,"r"));
        resultList.add(processString(inputString,"zi"));

        printResult(resultList);

    }

    private static HashMap<String, String> processString(String inputStr, String separator)
    {
        HashMap<String, String> result = new HashMap<String, String>();

      if (!inputStr.contains(separator)){
          result.put("Count", String.valueOf(1));
          result.put("Prefix", inputStr);

          result.put("sortedItems",inputStr.chars()
                  .sorted()
                  .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                  .toString());
          result.put("evenChars", getAllEvenCharsFromInputString(inputStr));
      }else if (inputStr.isEmpty() || separator.isEmpty() || inputStr.indexOf(separator) == 0){
          result.put("Count", String.valueOf(0));
          result.put("Prefix", "Zero Length String");
          result.put("sortedItems","Zero Length String" );
          result.put("evenChars", "Zero Length String");
      }else {

          int count = (inputStr.split(separator).length) -1;
          result.put("Count", String.valueOf(count));
          result.put("Prefix", inputStr.substring(0,inputStr.indexOf(separator)));
          result.put("sortedItems",inputStr.replaceAll(separator," "));
          result.put("evenChars", getAllEvenCharsFromInputString(inputStr));
      }





        return result;
    }

    private static String getAllEvenCharsFromInputString(String inputStr) {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < inputStr.length(); i++) {
            if (i % 2 == 0){
                result.append(inputStr.charAt(i));
            }
        }

        return result.toString().trim();
    }

    private static void printResult(ArrayList<HashMap<String, String>> resultList) {
        /*
    		Below is an example output when executed with inputString = "abcdefSEPgabcwetSEPsdsSEPdsfgSEPfro", separator = "SEP"

    		Count: 4
    		Prefix: abcdef
    		sortedItems: dsfg fro gabcwet sds
    		evenChars: aceSPaceSPdSPsgEfo
		*/

        //Add the implementation here

        for (HashMap<String, String> stringStringHashMap : resultList) {
            for (Map.Entry<String, String> entry : stringStringHashMap.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
        }
    }
}
