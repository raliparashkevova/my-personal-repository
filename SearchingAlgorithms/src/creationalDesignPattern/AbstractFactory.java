package creationalDesignPattern;


interface IMobile {
    IAndroid GetAndroidPhone();
    Iios GetIosPhone();
}

class Samsung implements IMobile{


    @Override
    public IAndroid GetAndroidPhone() {
        return new SamsungGalaxy();
    }

    @Override
    public Iios GetIosPhone() {
        return new Iphone();
    }
}

interface IAndroid {
    String GetModelDetail();
}

interface Iios {
    String GetModelDetail();
}

class SamsungGalaxy implements IAndroid {

    @Override
    public String GetModelDetail() {
        return "Model: Samsung Galaxy - RAM 28GB";
    }
}

class Iphone implements Iios{

    @Override
    public String GetModelDetail() {
        return "Model: Iphone 2 - RAM 99GB";
    }
}

class MobileClient {
    IAndroid androidPhone;
    Iios iiosPhone;

   public MobileClient(IMobile factory){
       androidPhone = factory.GetAndroidPhone();
       iiosPhone = factory.GetIosPhone();
   }

   public String GetAndroidPhoneDetails (){
       return androidPhone.GetModelDetail();
    }

    public String GetIosPhoneDetails (){
       return iiosPhone.GetModelDetail();
    }
}

public class AbstractFactory {

    public static void main(String[] args) {

        IMobile samsungMobile = new Samsung();
        MobileClient iPhoneClient = new MobileClient(samsungMobile);
        System.out.println(iPhoneClient.GetAndroidPhoneDetails());
        System.out.println(iPhoneClient.GetIosPhoneDetails());
    }
}
