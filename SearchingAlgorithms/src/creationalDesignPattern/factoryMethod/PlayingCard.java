package creationalDesignPattern.factoryMethod;

class PlayCard {

    public String type;
    public int value;
    public String suit;
}

class HoleyPlayingCard extends PlayCard {

    private String type;
    private int value;
    private String suit;

    public HoleyPlayingCard(int value, String suit) {
        setType();
        setValue();
        setSuit();


    }

    public void setType() {
        this.type = "Holey";
    }

    public void setValue() {
        this.value = value;
    }

    public void setSuit() {
        this.suit = suit;
    }
}

class Congress extends PlayCard {

    private String type;
    private int value;
    private String suit;

    public Congress(int value, String suit) {
        this.type = "Congress";
        this.value = value;
        this.suit = suit;
        setType();
        setValue();
        setSuit();
    }

    public void setType() {
        this.type = type;
    }

    public void setValue() {
        this.value = value;
    }

    public void setSuit() {
        this.suit = suit;
    }
}

abstract  class CardFactory {

    abstract PlayCard GetPlayingCard();
}

class HoleyFactory extends CardFactory {

    private String type;
    private int value;
    private String suit;

    public HoleyFactory(int value, String suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
  public   PlayCard GetPlayingCard() {
        return new HoleyPlayingCard(value,suit);
    }
}

class CongressFactory extends CardFactory {

    private String type;
    private int value;
    private String suit;

    public CongressFactory(int value, String suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public PlayCard GetPlayingCard() {
        return new Congress(value,suit);
    }
}

public class PlayingCard {

    public static void main(String[] args) {

       CardFactory cardFactory = new HoleyFactory(5,"Spades");
       PlayCard playingCard = cardFactory.GetPlayingCard();

        System.out.println("Card Type: " + playingCard.type);
        System.out.println("Card Value: " + playingCard.value);
        System.out.println("Card Suit: " + playingCard.suit);
    }
}
