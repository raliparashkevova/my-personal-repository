package creationalDesignPattern.prototype;



class Person implements Cloneable {
    private String firstName;
    private String lastName;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    protected Person clone() throws CloneNotSupportedException {
        return (Person) super.clone();
    }
}
public class PrototypePattern {

    public static void main(String[] args) {

        Person person = new Person("John", "Smith");

        try {
            Person person1 = person.clone();
            System.out.println(person1.getFirstName() + " " + person1.getLastName());
        }catch (CloneNotSupportedException e){
            e.printStackTrace();
        }


    }

}
