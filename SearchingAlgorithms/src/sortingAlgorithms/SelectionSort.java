package sortingAlgorithms;

import java.util.Arrays;

public class SelectionSort {

    public static void main(String[] args) {

        int [] elements = new int[]{64,25,12,22,11};

        elements = selectionSort(elements);

        System.out.println(Arrays.toString(elements));
    }

    private static int[] selectionSort(int[] elements) {

        int size = elements.length;

        for (int i = 0; i < size -1; i++) {

            int minIndex = i;

            for (int j = i+1; j < size; j++) {

                if (elements[j] < elements[minIndex]){
                    minIndex = j;
                }
            }

            int temp = elements[minIndex];
            elements[minIndex] = elements[i];
            elements[i] =  temp;

        }
        return elements;
    }
}
