package sortingAlgorithms;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {

        int [] elements = new int[]{64,25,12,22,11};

        elements = bubbleSort(elements);

        System.out.println(Arrays.toString(elements));
    }

    private static int[] bubbleSort(int[] elements) {

        int size = elements.length;

        for (int i = size -1 ; i >= 0 ; i--) {
            for (int j = 1; j <= i; j++) {

                if (elements[j-1] > elements[j]){

                    int temp = elements[j -1];
                    elements [j-1] = elements[j];
                    elements[j] = temp;
                }
            }
        }
        return elements;
    }
}
