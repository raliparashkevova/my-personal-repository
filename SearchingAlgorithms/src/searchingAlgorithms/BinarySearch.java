package searchingAlgorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class BinarySearch {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        List<Integer> elements = Arrays.stream(scanner.nextLine().split(",")).map(Integer::parseInt).collect(Collectors.toList());

        int searchElement = 3;
        int start = 0;
        int stop = elements.size();
        int middle = (start+stop) /2;


        while (elements.get(middle) != searchElement && start <stop){
            stop = searchElement < elements.get(middle) ? middle -1 : middle+1;
            middle = (start+stop) /2;
        }

        System.out.println(elements.get(middle) != searchElement  ? -1 :middle);
    }
}
