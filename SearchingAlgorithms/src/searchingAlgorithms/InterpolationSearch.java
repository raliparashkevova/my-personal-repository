package searchingAlgorithms;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class InterpolationSearch {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        List<Integer> elements = Arrays.stream(scanner.nextLine().split(",")).map(Integer::parseInt).collect(Collectors.toList());

        int searchingElement = 84;
        int low = 0;
        int hight = elements.size()-1;

        findElementsInterpolationAlgoritm(elements, searchingElement, low, hight);


    }

    private static void  findElementsInterpolationAlgoritm(List<Integer> elements, int searchingElement, int low, int hight) {
        while (low <= hight && searchingElement >= elements.get(low) && searchingElement <= elements.get(hight)) {

            if (low == hight) {
                if (elements.get(low) == searchingElement) {
                    System.out.println(low);
                } else {
                    System.out.println(-1);
                }
            }

            int probe = low + ((hight - low) * (searchingElement - elements.get(low)))/ (elements.get(hight)- elements.get(low));

            if (elements.get(probe) == searchingElement){
                System.out.println(probe);
                break;
            }

            if (elements.get(probe) < searchingElement){
                low = probe +1;
            }else if (elements.get(probe) > searchingElement){
                hight = probe -1;
            }
        }
    }
}
