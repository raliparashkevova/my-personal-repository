package searchingAlgorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LinearSearch {

static int findSearchingIndex(List<Integer> elements, int number){
    for (int i = 0; i < elements.size(); i++) {
        if (elements.get(i) == number){
            return i;
        }
    }
    return -1;
}
    public static void main(String[] args) {


        // Find the index of searching element 3 in the list


        int number = 3;
        List<Integer> numbers = new ArrayList<>(List.of(1,2,3,4,5));


        System.out.println(findSearchingIndex(numbers,number));
    }
}
