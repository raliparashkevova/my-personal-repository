package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Cream;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.ScentType;

public class CreamImpl extends ProductImpl implements Cream {

    private final ScentType scent;


    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        this.scent = scent;
    }

    @Override
    public String print() {

        StringBuilder sb = new StringBuilder();
        sb.append(super.print());
        sb.append(System.lineSeparator());
        sb.append(String.format("#Scented type: %s", getScentType()));


        return sb.toString();
    }

    @Override
    public ScentType getScentType() {
        return this.scent;
    }
}
