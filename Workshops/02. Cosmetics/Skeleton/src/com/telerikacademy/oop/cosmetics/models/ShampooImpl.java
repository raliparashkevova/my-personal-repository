package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Shampoo;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.UsageType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;

public class ShampooImpl extends ProductImpl implements Shampoo {


    private int milliliters;
    private final UsageType usageType;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usageType) {
        super(name, brand, price, gender);
        validateMillilitres(milliliters);
        this.usageType = usageType;
    }

    @Override
    public int getMillilitres() {
        return this.milliliters;
    }

    @Override
    public UsageType getUsageType() {
        return this.usageType;
    }

    @Override
    public void validateMillilitres(int milliliters) {

        if (milliliters < 0) {
            throw new IllegalArgumentException(ParsingHelpers.INVALID_MILLILITRES);
        }
        this.milliliters = milliliters;
    }

    @Override
    public String print() {

        StringBuilder sb = new StringBuilder();

        sb.append(super.print());
        sb.append(System.lineSeparator());
        sb.append(String.format("#Milliliters: %d", getMillilitres()));
        sb.append(System.lineSeparator());
        sb.append(String.format("#Usage: %s", getUsageType()));
        sb.append(System.lineSeparator());


        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShampooImpl shampoo = (ShampooImpl) o;
        return getName().equals(shampoo.getName()) &&
                getBrandName().equals(shampoo.getBrandName()) &&
                getPrice() == shampoo.getPrice() &&
                getGenderType().equals(shampoo.getGenderType()) &&
                getMillilitres() == shampoo.getMillilitres() &&
                getUsageType().equals(shampoo.getUsageType());
    }
}
