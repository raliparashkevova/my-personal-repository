package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.ShampooImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.UsageType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class CreateShampooCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 6;

    private final CosmeticsRepository cosmeticsRepository;

    public CreateShampooCommand(CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsRepository = cosmeticsRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = Double.parseDouble(parameters.get(2));
        GenderType genderType = ParsingHelpers.tryParseGender(parameters.get(3).toUpperCase());
        int millilitres = Integer.parseInt(parameters.get(4));
        UsageType usageType = ParsingHelpers.tryParseUsageType(parameters.get(5).toUpperCase());

        return createShampoo(name, brand, price, genderType, millilitres, usageType);
    }

    private String createShampoo(String name, String brandName,
                                 double price, GenderType genderType,
                                 int millilitres, UsageType usageType) {

        if (cosmeticsRepository.productExist(name)) {
            throw new IllegalArgumentException(String.format(ParsingHelpers.PRODUCT_NAME_ALREADY_EXISTS, name, brandName));

        }
        String output = "Shampoo";
        cosmeticsRepository.createShampoo(name, brandName, price, genderType, millilitres, usageType);
        return String.format(ParsingHelpers.PRODUCT_CREATED, output, name);
    }
}






