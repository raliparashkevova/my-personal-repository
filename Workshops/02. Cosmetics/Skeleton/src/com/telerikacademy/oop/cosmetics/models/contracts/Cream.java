package com.telerikacademy.oop.cosmetics.models.contracts;

import com.telerikacademy.oop.cosmetics.models.enums.ScentType;
import com.telerikacademy.oop.cosmetics.models.enums.UsageType;

public interface Cream extends Product {

    ScentType getScentType();
}
