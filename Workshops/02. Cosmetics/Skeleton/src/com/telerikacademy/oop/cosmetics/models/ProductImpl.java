package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

public abstract class ProductImpl implements Product {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final int BRAND_NAME_MIN_LENGTH = 2;
    public static final int BRAND_NAME_MAX_LENGTH = 10;


    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        validateName(name);
        validateBrand(brand);
        validatePrice(price);
        this.gender = gender;
    }


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getBrandName() {
        return this.brand;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public GenderType getGenderType() {
        return this.gender;
    }

    @Override
    public String print() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(String.format("#%s %s", getName(), getBrandName()));
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(String.format("#Price: $%.2f", getPrice()));
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(String.format("#Gender: %s", getGenderType().toString()));


        return stringBuilder.toString();
    }

    @Override
    public void validateName(String name) {

        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, this.name);
        this.name = name;


    }

    @Override
    public void validateBrand(String brand) {

        ValidationHelpers.validateStringLength(brand, BRAND_NAME_MIN_LENGTH, BRAND_NAME_MAX_LENGTH, this.brand);
        this.brand = brand;
    }

    @Override
    public void validatePrice(double price) {

        if (price < 0.0) {
            throw new IllegalArgumentException(ParsingHelpers.INVALID_PRICE);
        }
        this.price = price;

    }

}
