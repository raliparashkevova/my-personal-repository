package com.telerikacademy.oop.cosmetics.models.contracts;

import com.telerikacademy.oop.cosmetics.models.enums.GenderType;

public interface Product {

    String getName();

    String getBrandName();

    double getPrice();

    GenderType getGenderType();

    String print();

    void validateName(String name);

    void validateBrand(String brandName);

    void validatePrice(double price);

    //void validateGender(GenderType gender);


}
