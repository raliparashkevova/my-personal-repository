package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class Category {
    public static final int NAME_MIN_LENGTH = 2;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String NAME_LENGTH_MESSAGE = "The name should be between 2 and 15 characters";

    private String name;
    private ArrayList<Product> products;


    public Category(String name) {
        setName(name);
        this.products = new ArrayList<>();
    }

    /**
     * Validate the String name using method validateStringLength
     *
     * @param name
     * @author Ralitsa Parashkevova
     */
    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, NAME_LENGTH_MESSAGE);
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    /**
     * This method return the collection of products
     *
     * @return A List of products
     */
    public List<Product> getProducts() {
        return new ArrayList<>(products);

    }

    /**
     * This method add product to the collection of products.
     *
     * @param product
     */

    public void addProduct(Product product) {
        products.add(product);
    }

    /**
     * This method remove the product if it exists in the collection, otherwize throw exeption.
     *
     * @param product
     */
    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException("This product does not exist.");
        }
        products.remove(product);
    }

    /**
     * This method return the String print method
     *
     * @return A String that represent the output
     */
    public String print() {
        StringBuilder sb = new StringBuilder();


        sb.append(String.format("Category: %s", name));
        sb.append(System.lineSeparator());
        if (products.isEmpty()) {
            sb.append("No product in this category");
        } else {
            for (Product product : products) {
                sb.append(product.print());
            }


//       #Category: {category name}
//#Name Brand
//#Price: {price}
//#Gender: {genderType}
//===
//#Name Brand
//#Price: {price}
//#Gender: {genderType}
//===
//Category: Shampoos
//#No product in this category
        }
        return sb.toString();

    }
}
