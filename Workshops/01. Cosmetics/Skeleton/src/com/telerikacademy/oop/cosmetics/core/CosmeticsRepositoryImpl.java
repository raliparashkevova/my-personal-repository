package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.Product;
import com.telerikacademy.oop.cosmetics.models.ShoppingCart;

import java.util.ArrayList;
import java.util.List;

public class CosmeticsRepositoryImpl implements CosmeticsRepository {

    private final List<Product> products;
    private final List<Category> categories;
    private final ShoppingCart shoppingCart;

    public CosmeticsRepositoryImpl() {
        this.products = new ArrayList<>();
        this.categories = new ArrayList<>();

        this.shoppingCart = new ShoppingCart();
    }

    @Override
    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    @Override
    public List<Category> getCategories() {
        return new ArrayList<>(categories);
    }

    @Override
    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    /**
     * This method should return a product which is found by productName, otherwise throw exception if it not found
     *
     * @param productName
     * @return product
     */
    @Override
    public Product findProductByName(String productName) {

        for (Product product : products) {
            if (product.getName().equals(productName)) {
                return product;
            }
        }

        throw new IllegalArgumentException("Product with this name does not exist");
    }

    /**
     * This method should return a product which is found by categoryName, otherwise throw exception if it not found
     *
     * @param categoryName
     * @return category
     */
    @Override
    public Category findCategoryByName(String categoryName) {

        for (Category category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }

        throw new IllegalArgumentException("Category with this name does not exist");
    }

    @Override
    public void createCategory(String categoryName) {

        if (!categoryExist(categoryName)) {
            Category category = new Category(categoryName);

            categories.add(category);
        }
    }


    @Override
    public void createProduct(String name, String brand, double price, GenderType gender) {


        if (!productExist(name)) {
            Product product = new Product(name, brand, price, gender);
            products.add(product);
        }
    }

    @Override
    public boolean categoryExist(String categoryName) {

        for (Category category : categories) {
            if (category.getName().equals(categoryName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method should return if the product exists in the collection of product.
     *
     * @param productName
     * @return boolean true if the product exists or boolean false if it does not exist in the collection.
     */
    @Override
    public boolean productExist(String productName) {

        for (Product product : products) {
            if (product.getName().equals(productName)) {
                return true;
            }
        }

        return false;
    }
}
