package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.Objects;

public class Product {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final int BRAND_MIN_LENGTH = 2;
    public static final int BRAND_MAX_LENGTH = 10;
    public static final String NAME_LENGTH_MESSAGE = "The name should be between 3 and 10 characters";
    public static final String BRAND_LENGTH_MESSAGE = "The brand name should be between 2 and 10 characters";

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    // "Each product in the system has name, brand, price and gender."

    public Product(String name, String brand, double price, GenderType gender) {
        // finish the constructor and validate data
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }


    /**
     * This method validate if the price is passed as negative
     *
     * @param price
     */
    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("The price cannot be negative");
        }
        this.price = price;
    }

    /**
     * This method  validate the input of name length
     *
     * @param name
     */
    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, NAME_LENGTH_MESSAGE);
        this.name = name;
    }

    /**
     * This method validate the input of brand length
     *
     * @param brand
     */
    private void setBrand(String brand) {
        ValidationHelpers.validateStringLength(brand, BRAND_MIN_LENGTH, BRAND_MAX_LENGTH, BRAND_LENGTH_MESSAGE);
        this.brand = brand;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String getName() {
        return this.name;
    }

    public double getPrice() {
        return this.price;
    }

    public String getBrand() {
        return this.brand;
    }

    public GenderType getGender() {
        return this.gender;
    }

    /**
     * Method return a output text
     *
     * @return A String text from the variables in the Product class
     */
    public String print() {

        return String.format("%s %s \n" +
                "Price: $%.2f\n" +
                "Gender: %s", name, brand, price, gender.toString());

    }

    @Override
    public boolean equals(Object p) {
        if (this == p) return true;
        if (p == null || getClass() != p.getClass()) return false;
        Product product = (Product) p;
        return Double.compare(this.getPrice(), product.getPrice()) == 0 &&
                Objects.equals(this.getName(), product.getName()) &&
                Objects.equals(this.getBrand(), product.getBrand()) &&
                this.getGender() == product.getGender();
    }
}
