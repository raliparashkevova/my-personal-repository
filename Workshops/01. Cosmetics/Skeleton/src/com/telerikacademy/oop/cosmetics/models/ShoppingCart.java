package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> products;

    public ShoppingCart() {
        this.products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    /**
     * This method add product to the collection of Products
     *
     * @param product added product in the collection
     */
    public void addProduct(Product product) {


        products.add(product);
    }

    /**
     * This method remove the given product if it exist in the collection of Products
     *
     * @param product
     */
    public void removeProduct(Product product) {

        if (!getProducts().contains(product)) {
            throw new IllegalArgumentException("This product does not exist.");
        }
        products.remove(product);
    }

    /**
     * This method checks if the given product is contained in the collection of products.
     *
     * @param product
     * @return boolean true if the product exists or boolean false if it does not exist in the collection.
     */
    public boolean containsProduct(Product product) {
        return products.contains(product);
    }

    public double totalPrice() {
        double totalPrice = 0.0;

        for (Product product : products) {
            totalPrice += product.getPrice();
        }

        return totalPrice;
    }

}
