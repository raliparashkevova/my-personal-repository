package com.telerikacademy.models;

public enum Genre {
    DRAMA, COMEDY, ACTION, HORROR, FANTASY
}
