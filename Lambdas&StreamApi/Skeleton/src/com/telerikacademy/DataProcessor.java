package com.telerikacademy;

import com.telerikacademy.models.Customer;
import com.telerikacademy.models.Genre;
import com.telerikacademy.models.Movie;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("ForLoopReplaceableByForEach")
public class DataProcessor {

    public static long countCustomersAboveTargetAge(List<Customer> customers, int targetAge) {
        int result = 0;

        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getAge() > targetAge) {
                result++;
            }
        }

        return customers.stream().filter(customer -> customer.getAge() > targetAge).count();
    }

    /**
     * Hint: Is there a method on streams that asks the question "Do all elements match a given condition?"
     */
    public static boolean findIfAllCustomersAreAboveTargetAge(List<Customer> customers, int targetAge) {
        boolean result = true;


        // for (int i = 0; i < customers.size(); i++) {
        //     if (customers.get(i).getAge() < targetAge) {
        //         result = false;
        //         break;
        //     }
        // }

        return customers.stream().allMatch(customer -> customer.getAge() < targetAge);
    }

    /**
     * Hint: Is there a method on streams that asks the question "Does any element match a given condition?"
     */
    public static boolean findIfAnyCustomersHasTargetName(List<Customer> customers, String targetName) {
        boolean result = false;

        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getName().equals(targetName)) {
                result = true;
                break;
            }
        }

        // here we map Customer:: getName to list of string and then we are looking about targetName
        //return customers.stream().map(Customer::getName).anyMatch(name->name.equals(targetName));
        return customers.stream().anyMatch(customer -> customer.getName().equals(targetName));
    }

    /**
     * Hint: Is there a method on streams that asks the question "Do all element match a given condition?"
     */
    public static boolean findIfAllCustomersDislikeMovie(List<Customer> customers, Movie targetMovie) {


        return customers
                .stream()
                .allMatch(customer -> customer.getDislikedMovies()
                        .contains(targetMovie));
    }

    /**
     * Hint: What method on streams eliminates elements, based on some condition?
     */
    public static long findHowManyPeopleLikeMove(List<Customer> customers, Movie targetMovie) {


        //here is another working decision
        // return customers.stream().map(customer -> customer.getLikedMovies()
        //         .stream()).filter(movieStream -> movieStream.equals(targetMovie)).count();
        //but the test does not run successfully because map means zip in zip, because of that we should use flatMap
        return customers
                .stream()
                .filter(customer -> customer
                        .getLikedMovies()
                        .contains(targetMovie)).count();
    }

    /**
     * Hint: Is there a method on streams that can eliminate elements from a collection, based on some condition? Also,
     * is there a method that transforms one thing into another thing?
     */
    public static double findTheAverageAgeOfPeopleWhoDislikeMovies(List<Customer> customers, Movie targetMovie) {

        //Another decision: return customers.stream.filter(customer -> customer.getDislikedMovies
        //.contains(targetMovie)).mapToInt(Customer::getAge).average().getAsDouble;
        return customers.stream()
                .filter(customer -> customer.getDislikedMovies()
                        .contains(targetMovie))
                .collect(Collectors.averagingDouble(Customer::getAge));
    }

    /**
     * Hint: There is a method average() but we can use it on numeric types only. What method transforms
     * streams from one type to another?
     */
    public static double findAverageAgeOfAllCustomers(List<Customer> customers) {

        //Another decision: customers.stream.mapToInt(Customer::getAge).average.getAsDouble;

        return customers.stream()
                .collect(Collectors.averagingDouble(Customer::getAge));
    }

    /**
     * Hint: First, we need to eliminate all customers whose age is below the targetAge. Then, we need to eliminate
     * all customers who do not have any movies with the targetGenre in their list of likedMovies.
     */
    public static List<Customer> findAllCustomersAboveTargetAgeThatLikeGenre(List<Customer> customers, int targetAge, Genre targetGenre) {


        return customers.stream()

                .filter(customer -> customer.getAge() > targetAge)
                .filter(customer -> customer.getLikedMovies().contains(targetGenre)).collect(Collectors.toList());

    }

    /**
     * Hint: Eliminate all customers with age below the targetAge.
     */
    public static List<Customer> findAllCustomersUnderTargetAge(List<Customer> customers, int targetAge) {

        return customers.stream().filter(customer -> customer.getAge() < targetAge).collect(Collectors.toList());
    }

    /**
     * Hint: https://www.baeldung.com/java-stream-reduce
     */
    public static Customer findTheCustomerWithTheLongestName(List<Customer> customers) {

        return customers
                .stream()
                .filter(customer -> customer.getName().length() == (customers
                        .stream()
                        .map(Customer::getName)
                        .map(String::length)
                        .reduce(0, (first, second) -> first >= second ? first : second)))
                .collect(Collectors.toList()).get(0);
    }

    /**
     * Hint: From the list of customers, eliminate the ones whose list of movies
     * has a movie with a genre, different than the targetGenre.
     */
    public static List<Customer> findAllCustomersWhoLikeOnlyMoviesWithGenre(List<Customer> customers, Genre targetGenre) {


        return customers.stream()
                .filter(customer -> customer.getLikedMovies()
                        .stream()
                        .allMatch(movie -> movie.getGenre().equals(targetGenre)))
                .collect(Collectors.toList());
    }
}
