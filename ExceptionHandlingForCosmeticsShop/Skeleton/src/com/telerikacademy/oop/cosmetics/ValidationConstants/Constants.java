package com.telerikacademy.oop.cosmetics.ValidationConstants;

public class Constants {

    public static final int MIN_NAME_LENGTH = 3;
    public static final int MAX_NAME_LENGTH = 10;
    public static final int MIN_BRAND_LENGTH = 2;
    public static final int MAX_BRAND_LENGTH = 10;


}
