package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.ValidationConstants.Constants;
import com.telerikacademy.oop.cosmetics.commands.*;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CommandFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;


import static com.telerikacademy.oop.cosmetics.utils.ParsingHelpers.*;

public class CommandFactoryImpl implements CommandFactory {

    @Override
    public Command createCommandFromCommandName(String commandTypeValue, ProductRepository productRepository) {

        CommandType commandType;
        try {
            commandType = CommandType.valueOf(commandTypeValue.toUpperCase());
        } catch (IllegalArgumentException input) {
            throw new IllegalArgumentException(String.format("Command %s is not supported.", commandTypeValue));
        }

        switch (commandType) {
            case CREATECATEGORY:
                return new CreateCategoryCommand(productRepository);
            case CREATEPRODUCT:
                return new CreateProductCommand(productRepository);
            case ADDPRODUCTTOCATEGORY:
                return new AddProductToCategoryCommand(productRepository);
            case SHOWCATEGORY:
                return new ShowCategoryCommand(productRepository);
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandType));
        }
    }

}
