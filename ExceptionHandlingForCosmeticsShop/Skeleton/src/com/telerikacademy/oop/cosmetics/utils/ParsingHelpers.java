package com.telerikacademy.oop.cosmetics.utils;

import com.telerikacademy.oop.cosmetics.commands.CommandType;
import com.telerikacademy.oop.cosmetics.models.GenderType;

public class ParsingHelpers {


    public static final String INVALID_COMMAND = "Invalid command %s";
    public static final String PARAMETERS_COUNT = "%s command expects %d parameters";
    public static final String NO_SUCH_GENDER_TYPE_ENUM = "Forth parameter should be one of Men, Women or Unisex.";
    private static final String NO_SUCH_COMMAND_TYPE_ENUM = "Command %s is not supported.";


    public static double inputDoubleValidation(String valueToParse, String errorMessage) {
        try {
            return Double.parseDouble(valueToParse);
        } catch (NumberFormatException exception) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static GenderType tryParseGender(String valueToParse) {
        try {
            return GenderType.valueOf(valueToParse);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(NO_SUCH_GENDER_TYPE_ENUM, valueToParse));
        }
    }

    public static CommandType tryParseCommandType(String valueToParse) {
        try {
            return CommandType.valueOf(valueToParse);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(NO_SUCH_COMMAND_TYPE_ENUM, valueToParse));
        }
    }
}
