package com.telerikacademy.oop.cosmetics.utils;

import com.telerikacademy.oop.cosmetics.ValidationConstants.Constants;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.ValidationConstants.Constants.*;
import static com.telerikacademy.oop.cosmetics.utils.ParsingHelpers.*;


public class ValidationHelpers {

    public static final String STRING_LENGTH_ERROR = "%s name should be between 3 and 10 symbols.";


    public static void validateIntRange(int minLength, int maxLength, int actualLength, String type) {
        if (actualLength < minLength || actualLength > maxLength) {
            throw new IllegalArgumentException(String.format(STRING_LENGTH_ERROR, type));
        }
    }

    public static void validateLength(String stringToValidate, int minLength, int maxLength, String type) {

        validateIntRange(minLength, maxLength, stringToValidate.length(), type);

    }


    public static void validatePrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price can't be negative.");
        }
    }


    public static void validateParametersCount(List<String> parameters, int validParameters) {

        if (parameters.size() > validParameters || parameters.size() < validParameters) {
            throw new IllegalArgumentException(String.format(PARAMETERS_COUNT, validParameters, parameters.size()));
        }
    }
}
