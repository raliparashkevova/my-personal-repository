package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.utils.ValidationHelpers.validateParametersCount;

public class CreateCategoryCommand implements Command {

    private static final String CATEGORY_CREATED = "Category with name %s was created!";
    private static final int VALID_PARAMETERS = 1;
    private final ProductRepository productRepository;

    public CreateCategoryCommand(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateParametersCount(parameters, VALID_PARAMETERS);

        String categoryName = parameters.get(0);

        return createCategory(categoryName);
    }

    private String createCategory(String categoryName) {

        if (productRepository.categoryExist(categoryName)) {
            throw new IllegalArgumentException(String.format("Category %s already exist.", categoryName));
        }
        productRepository.createCategory(categoryName);

        return String.format(CATEGORY_CREATED, categoryName);
    }

}
