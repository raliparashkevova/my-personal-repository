package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.utils.ParsingHelpers.*;

public class CreateProductCommand implements Command {

    private static final String PRODUCT_CREATED = "Product with name %s was created!";
    private static final int VALID_PARAMETERS = 4;
    private static final String INVALID_PARAMETERS = "Third parameter should be price (real number).";
    private final ProductRepository productRepository;

    public CreateProductCommand(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() > VALID_PARAMETERS || parameters.size() < VALID_PARAMETERS) {
            throw new IllegalArgumentException(String.format("CreateProduct command expects %d parameters", VALID_PARAMETERS));
        }
        String name = parameters.get(0);
        String brand = parameters.get(1);

        double price = inputDoubleValidation(parameters.get(2), INVALID_PARAMETERS);


        GenderType gender = tryParseGender(parameters.get(3).toUpperCase());

        return createProduct(name, brand, price, gender);
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {

        if (productRepository.productExist(name)) {
            throw new IllegalArgumentException(String.format("Product %s already exist.", name));
        }
        productRepository.createProduct(name, brand, price, gender);

        return String.format(PRODUCT_CREATED, name);
    }

}
