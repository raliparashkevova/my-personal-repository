package com.telerikacademy.oop.cosmetics.commands;

public enum CommandType {
    CREATECATEGORY,
    CREATEPRODUCT,
    ADDPRODUCTTOCATEGORY,
    SHOWCATEGORY;

    @Override
    public String toString() {
        switch (this) {
            case CREATECATEGORY:
                return "CreateCategory";
            case ADDPRODUCTTOCATEGORY:
                return "AddProductToCategory";
            case CREATEPRODUCT:
                return "CreateProduct";
            case SHOWCATEGORY:
                return "ShowCategory";
            default:
                return "Unknown";
        }
    }
}
