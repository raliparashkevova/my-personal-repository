package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.ValidationConstants.Constants;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import static com.telerikacademy.oop.cosmetics.ValidationConstants.Constants.*;
import static com.telerikacademy.oop.cosmetics.utils.ValidationHelpers.*;
import static com.telerikacademy.oop.cosmetics.utils.ValidationHelpers.*;

public class ProductImpl implements Product {

    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        validateLength(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH, "Product");
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if (brand.length() < MIN_BRAND_LENGTH || brand.length() > MAX_BRAND_LENGTH) {
            throw new IllegalArgumentException(String.format("Product brand should be between %d and %d symbols.", MIN_BRAND_LENGTH, MAX_BRAND_LENGTH));
        }
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        validatePrice(price);
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }

}
