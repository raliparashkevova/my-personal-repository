import java.util.*;
import java.util.stream.Collectors;

public class ArmyLunch {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int n = Integer.parseInt(scanner.nextLine());
        String[] input = scanner.nextLine().split(" ");

        List<String> sergeants = new ArrayList<>();
        List<String> corporals = new ArrayList<>();
        List<String> privates = new ArrayList<>();

        List<String> result = new ArrayList<>();


        for (String soldier : input) {
            char symbol = soldier.charAt(0);
            String number = String.valueOf(soldier.charAt(1));
            if (symbol == 'S') {

                sergeants.add(soldier);
            } else if (symbol == 'C') {
                corporals.add(soldier);

            } else if (symbol == 'P') {

                privates.add(soldier);
            }
        }

        result.addAll(sergeants);
        result.addAll(corporals);
        result.addAll(privates);
        System.out.println(
                result.stream()
                        .map(String::valueOf)
                        .collect(Collectors.joining(" ")));

    }

}

