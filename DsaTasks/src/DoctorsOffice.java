import java.util.*;

public class DoctorsOffice {

    private static Map<String, Count> namesInQueue = new LinkedHashMap<>();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        String input = scanner.nextLine();
        Queue<String> patients = new LinkedList<>();
        List<String> patientList = new ArrayList<>();
        while (!"End".equalsIgnoreCase(input)) {
            String[] lines = input.split(" ");
            String command = lines[0];
            switch (command) {
                case "Append":
                    String name = lines[1];
                    patientList.add(name);
                    System.out.println("OK");
                    Count counter = namesInQueue.get(name);
                    if (counter == null) {
                        namesInQueue.put(name, new Count());
                    } else {
                        counter.increment();
                    }
                    break;
                case "Examine":
                    int count = Integer.parseInt(lines[1]);
                    if (count < 0 || count > patientList.size()) {
                        System.out.println("Error");
                        break;
                    }
                    for (int i = 0; i < count; i++) {
                        String removed = patientList.remove(0);
                        System.out.print(removed + " ");
                        namesInQueue.get(removed).decrement();
                    }
                    System.out.println();
                    break;
                case "Find":
                    try {
                        System.out.println(namesInQueue.get(lines[1]).getCount());
                    } catch (Exception e) {
                        System.out.println(0);
                    }
                    break;
                case "Insert":
                    int position = Integer.parseInt(lines[1]);
                    String patientName = lines[2];
                    if (position < 0 || position > patientList.size()) {
                        System.out.println("Error");
                        break;
                    }
                    patientList.add(position, patientName);
                    System.out.println("OK");
                    Count counter1 = namesInQueue.get(patientName);
                    if (counter1 == null) {
                        namesInQueue.put(patientName, new Count());
                    } else {
                        counter1.increment();
                    }
                    break;
            }
            input = scanner.nextLine();
        }
    }

    private static class Count {
        int count = 1;

        public void increment() {
            count++;
        }

        public void decrement() {
            count--;
            if (count < 0) count = 0;
        }

        public int getCount() {
            return count;
        }
    }
}