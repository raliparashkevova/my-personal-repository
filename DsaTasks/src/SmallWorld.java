import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class SmallWorld {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String[] size = scanner.nextLine().split(" ");
        int n = Integer.parseInt(size[0]);
        int m = Integer.parseInt(size[1]);


        int[][] board = new int[n][m];


        for (int row = 0; row < n; row++) {
            String rows = scanner.nextLine();
            for (int col = 0; col < m; col++) {
                String[] numbers = rows.split("");
                board[row][col] = Integer.parseInt(numbers[col]);


            }
        }


        boolean[][] visited = new boolean[n][m];


        List<Integer> conquests = new ArrayList<>();


        for (int row = 0; row < n; row++) {
            for (int col = 0; col < m; col++) {
                if (visited[row][col] || board[row][col] == 0) {
                    continue;
                }
                int count = explore(board, visited, row, col);
                conquests.add(count);
            }
        }
        conquests.sort(Collections.reverseOrder());
        for (Integer conquestSize :
                conquests) {
            System.out.println(conquestSize);
        }
    }


    private static int explore(int[][] board, boolean[][] visited, int row, int col) {
        if (outOfboard(board, row, col)) {
            return 0;
        }


        if (visited[row][col]) {
            return 0;
        }


        if (board[row][col] == 0) {
            return 0;
        }


        visited[row][col] = true;
        board[row][col] = 0;


        return explore(board, visited, row - 1, col) +
                explore(board, visited, row + 1, col) +
                explore(board, visited, row, col - 1) +
                explore(board, visited, row, col + 1) + 1;
    }


    private static boolean outOfboard(int[][] maze, int row, int col) {
        return row >= maze.length || row < 0 || col >= maze[row].length || col < 0;
    }
}