import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Sequence {
    private static List<Integer> result = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int k = scanner.nextInt();
        int n = scanner.nextInt();
        List<Integer> result;
        if (n == 1) {
            System.out.println(k);
        } else if (n == 2) {
            System.out.println(k + 1);
        } else {
            result = createSequence(k, n);
            System.out.println(result.get(n - 2));
        }
    }

    public static List<Integer> createSequence(int e1, int n) {
        if (result.size() > n + 2) {
            return result;
        }
        result.add(e1 + 1);
        result.add(2 * e1 + 1);
        result.add(e1 + 2);

        return createSequence(e1 + 1, n - 1);
    }
}