public class Recursion {

    public static void main(String[] args) {


        int n = 6;

        int factorial = getFactorial(n);
    }

    private static int getFactorial(int n) {

        if (n <= 1) {
            return 1;
        }

        return n * getFactorial(n - 1);
    }
}
