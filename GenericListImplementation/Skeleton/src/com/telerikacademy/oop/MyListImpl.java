package com.telerikacademy.oop;

import java.util.*;

public class MyListImpl<T> implements MyList<T> {

    private final int DEFAULT_CAPACITY = 4;
    private T[] customArray;
    private int capacity = 0;
    private int size;

    public MyListImpl() {
        this.capacity = DEFAULT_CAPACITY;
        this.customArray = (T[]) new Object[capacity];

    }

    public MyListImpl(int initialCapacity) {
        this.capacity = initialCapacity;
        this.customArray = (T[]) new Object[capacity];
        this.size = 0;

    }

    public MyListImpl(Collection<? extends T> myCollection) {

    }

    @Override
    public int size() {


        return this.size;
    }

    @Override
    public int capacity() {
        return this.capacity;
    }

    @Override
    public void add(T element) {

        if (size < capacity) {
            customArray[size] = element;
            size++;
        } else {
            resize();
            customArray[size] = element;

        }
    }

    @Override
    public T get(int index) {

        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException(index);
        }


        return customArray[index];
    }

    @Override
    public int indexOf(T element) {

        if (element != null) {
            for (int i = 0; i < size; i++) {
                if (customArray[i] == element) {
                    return i;
                }

            }
        } else {
            return -1;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {

        if (element != null) {
            for (int i = customArray.length - 1; i >= 0; i--) {
                if (customArray[i] == element) {
                    return i;
                }

            }
        } else {
            return -1;
        }
        return -1;
    }

    @Override
    public boolean contains(T element) {

        if (element != null) {
            for (int i = 0; i < size; i++) {
                if (customArray[i] == element) {
                    return true;
                }

            }
        } else {
            return false;
        }
        return false;
    }

    @Override
    public void removeAt(int index) {

        if (index < 0 || index >= customArray.length - 1) {
            throw new ArrayIndexOutOfBoundsException("The index id out of bounds!");
        }

        T removedElement = customArray[index];

        for (int i = index + 1; i < size; i++) {
            customArray[i - 1] = customArray[i];
        }
        size--;
    }

    @Override
    public boolean remove(T element) {


        if (contains(element)) {
            int index = indexOf(element);

            for (int i = index + 1; i < size; i++) {
                int removedIndex = i - 1;
                removedIndex = i;
                return true;
            }
            size--;
        }


        return false;
    }

    @Override
    public void clear() {
        for (T t : customArray) {
            remove(t);
        }
    }

    @Override
    public void swap(int from, int to) {


        T fromElement = customArray[from];
        T toElement = customArray[to];

        customArray[from] = toElement;
        customArray[to] = fromElement;

    }

    @Override
    public void print() {

        for (int i = 0; i < size; i++) {
            System.out.print(customArray[i] + " ");
        }


    }


    public void resize() {

        capacity *= 2;

        customArray = Arrays.copyOf(customArray, capacity);

    }

    @Override
    public Iterator<T> iterator() {
        return new MyListIterator();
    }

    private class MyListIterator implements Iterator<T> {

        private int currentIndex;

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public T next() {
            T currentElement = customArray[currentIndex];
            currentIndex++;

            return currentElement;
        }
    }
}
