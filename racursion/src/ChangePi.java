import java.util.Scanner;

public class ChangePi {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        String filter = "pi";
        String value = "3.14";
        String output = replaceWithPiValue(input, filter, value);

        System.out.println(output);


    }


    private static String replaceWithPiValue(String input, String filter, String value) {

        if (input.equals("") || input.length() < filter.length()) {
            return input;
        }

        if (input.charAt(0) == filter.charAt(0) && input.charAt(1) == filter.charAt(1)) {
            return value + replaceWithPiValue(input.substring(filter.length()), filter, value);
        }

        return input.charAt(0) + replaceWithPiValue(input.substring(1), filter, value);

    }
}
