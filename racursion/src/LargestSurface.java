import java.util.Scanner;

public class LargestSurface {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        //readInput

        int rows = scanner.nextInt();
        int cols = scanner.nextInt();


        int[][] matrix = new int[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = scanner.nextInt();
            }
        }

        boolean[][] visitedMatrix = new boolean[rows][cols];
        int maxCount = 0;

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (visitedMatrix[row][col]) {
                    continue;
                }

                int count = findLargestSurface(matrix, row, col, visitedMatrix, matrix[row][col]);
                maxCount = Math.max(maxCount, count);

            }

        }

        ;
        System.out.println(maxCount);
    }

    private static int findLargestSurface(int[][] matrix, int row, int col, boolean[][] visitedMatrix, int currentValue) {


        if (isOutOfMatrix(matrix, row, col)) {
            return 0;
        }

        if (isVisited(visitedMatrix, row, col)) {
            return 0;
        }

        if (currentValue != matrix[row][col]) {
            return 0;
        }


        visitedMatrix[row][col] = true;
        return findLargestSurface(matrix, row + 1, col, visitedMatrix, matrix[row][col]) +

                findLargestSurface(matrix, row - 1, col, visitedMatrix, matrix[row][col]) +
                findLargestSurface(matrix, row, col + 1, visitedMatrix, matrix[row][col]) +
                findLargestSurface(matrix, row, col - 1, visitedMatrix, matrix[row][col]) + 1;


    }

    private static boolean isVisited(boolean[][] visitedMatrix, int row, int col) {

        return visitedMatrix[row][col];
    }


    // private static int[] getNextCell(int[][] matrix, int row, int col,boolean[][] visitedMatrix) {
    //     int downCell = getCell(matrix,row +1,col);
    //     int upCell = getCell(matrix,row -1,col);
    //     int rightCell = getCell(matrix,row,col+1);
    //     int leftCell = getCell(matrix,row,col -1);


    //     if (matrix[row][col] == downCell){
    //         visitedMatrix[row][col] = true;
    //         return new int[]{row+1,col};
    //     }
    //     if (matrix [row][col] == upCell){
    //         visitedMatrix[row][col] = true;
    //         return new int[]{row-1,col};
    //     }
    //     if (matrix [row][col] == rightCell){
    //         visitedMatrix[row][col] = true;
    //         return new int[]{row,col+1};
    //     }
    //     if (matrix [row][col] == leftCell){
    //         visitedMatrix[row][col] = true;
    //         return new int[]{row,col -1};
    //     }


    //     return new int[]{row,col};
    // }

    private static int getCell(int[][] matrix, int row, int col) {

        return isOutOfMatrix(matrix, row, col) ? 0 : matrix[row][col];
    }

    private static boolean isOutOfMatrix(int[][] matrix, int row, int col) {

        return row < 0 || row >= matrix.length || col < 0 || col >= matrix[row].length;
    }
}
