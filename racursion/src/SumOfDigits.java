import java.util.Scanner;

public class SumOfDigits {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int number = Integer.parseInt(scanner.nextLine());

        System.out.println(sumOfDigits(number));
    }

    private static long sumOfDigits(int number) {
        if (number == 0)
            return 0;
        return (number % 10 + sumOfDigits(number / 10));
    }
}
