import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        int[] result = new int[1];
        System.out.println(fibonacciFromN(n, result));
    }

    private static int fibonacciFromN(int n, int[] result) {

        if (n < 2)
            return n;

        result[0] = fibonacciFromN(n - 1, result)
                + fibonacciFromN(n - 2, result);

        return result[0];
    }


}