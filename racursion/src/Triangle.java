import java.util.Scanner;

public class Triangle {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int numberOfBlocks = Integer.parseInt(scanner.nextLine());


        System.out.println(computeNumberOfBlocks(numberOfBlocks));
    }

    private static long computeNumberOfBlocks(int numberOfBlocks) {

        if (numberOfBlocks == 0) {
            return 0;
        } else if (numberOfBlocks == 1) {
            return 1;
        }

        return numberOfBlocks + (computeNumberOfBlocks(numberOfBlocks - 1));
    }
}
