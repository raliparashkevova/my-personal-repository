import java.util.Scanner;

public class countOccurrence {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int number = Integer.parseInt(scanner.nextLine());

        int count = 0;
        countSeventh(number, count);
    }

    private static void countSeventh(int number, int count) {

        if (number < 1) {
            System.out.println(count);
            return;
        }


        int digit = number % 10;

        if (digit == 7) {
            count++;
        }

        countSeventh(number / 10, count);
    }
}
