import java.util.Scanner;

public class CountX {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        int count = getCountOfXCharacters(input);

        System.out.println(count);


    }

    private static int getCountOfXCharacters(String input) {
        if (input.length() == 0)   // base case
        {
            return 0;
        }

        if (input.charAt(0) == 'x') {
            return 1 + getCountOfXCharacters(input.substring(1));
        } else {
            return getCountOfXCharacters(input.substring(1));
        }
    }
}
