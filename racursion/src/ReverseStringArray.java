import java.util.Arrays;
import java.util.Scanner;

public class ReverseStringArray {

    public static void main(String[] args) {

        String [] input = {"John","Aria","Rali","Boris","Martin","Dani"};



        for (int i = 0; i < input.length; i++) {
            for (int j = i+1; j < input.length; j++) {
                if (input[i].compareTo(input[j]) > 0){
                    String temp = input[i];

                    input[i] = input[j];
                    input[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(input));
    }
}
