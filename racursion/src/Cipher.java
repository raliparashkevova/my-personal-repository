import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Cipher {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String secretCode = scanner.nextLine();
        String cipher = scanner.nextLine();

        CipherDecoder decoder = new CipherDecoder(secretCode, cipher);

        List<String> originalMessages = decoder.Decode();

        Collections.sort(originalMessages);

        System.out.println(originalMessages.size());

        printMessages(originalMessages);
    }

    private static void printMessages(List<String> originalMessages) {
        for (String message : originalMessages) {
            System.out.println(message);
        }
    }

    static class CipherDecoder {

        private final List<CipherItem> cipherItems = new LinkedList<>();
        private final String secretCode;
        private final List<String> originalMessages = new LinkedList<>();
        char[] currentOriginalMessage = new char[100];

        public CipherDecoder(String secretCode, String cipher) {

            StringBuilder currentCipher = new StringBuilder();
            char lastChar = '\0';

            for (char c : cipher.toCharArray()) {

                if (c >= 'A' && c <= 'Z') {
                    if (currentCipher.length() > 0) {
                        cipherItems.add(new CipherItem(lastChar, currentCipher.toString()));
                        currentCipher.delete(0, currentCipher.length());
                    }
                    lastChar = c;
                } else {
                    currentCipher.append(c);
                }
            }
            if (currentCipher.length() > 0) {
                cipherItems.add(new CipherItem(lastChar, currentCipher.toString()));
            }

            this.secretCode = secretCode;
        }

        private void AddSolution(char[] currentOriginalMessage) {
            StringBuilder originalMessage = new StringBuilder();
            for (char c : currentOriginalMessage) {

                if (c < 'A' || c > 'Z') break;
                originalMessage.append(c);
            }
            originalMessages.add(originalMessage.toString());
        }

        private void DecodeWithRecursion(int index, int wordIndex) {
            if (index == secretCode.length()) {
                AddSolution(currentOriginalMessage);
                return;
            }
            if (index > secretCode.length()) {
                return;
            }
            for (CipherItem item : cipherItems) {

                if (isDecode(index, item)) {
                    currentOriginalMessage[wordIndex] = item.getLetter();
                    DecodeWithRecursion((index + item.getCode().length()), (wordIndex + 1));
                    currentOriginalMessage[wordIndex] = '\0';
                }
            }
        }

        private boolean isDecode(int index, CipherItem item) {
            boolean outOfBounds = secretCode.length() >= index + item.getCode().length();
            boolean isEqual = secretCode.startsWith(item.getCode(), index);

            return outOfBounds && isEqual;
        }

        public List<String> Decode() {
            DecodeWithRecursion(0, 0);
            return originalMessages;
        }
    }

    static class CipherItem {

        public char letter;
        public String code;

        public CipherItem(char letter, String code) {
            this.letter = letter;
            this.code = code;
        }

        public char getLetter() {
            return letter;
        }

        public String getCode() {
            return code;
        }
    }

}