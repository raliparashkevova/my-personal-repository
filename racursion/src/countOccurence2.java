import java.util.Scanner;

public class countOccurence2 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());


        int count = 0;

        countGivenDigits(n, count);
    }

    private static void countGivenDigits(int n, int count) {


        if (n < 1) {
            System.out.println(count);
            return;
        }

        if (n % 10 == 8 && (n / 10) % 10 == 8) {
            count += 2;

        } else if (n % 10 == 8) {
            count++;
        }
        System.out.println(n);
        countGivenDigits(n / 10, count);
    }
}
