import java.util.Arrays;
import java.util.Scanner;

public class ArrayValuesTimes10 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] numbers = Arrays.stream(scanner.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        int index = Integer.parseInt(scanner.nextLine());

        System.out.println(isNextValuesTime10(numbers, index));
    }

    private static boolean isNextValuesTime10(int[] numbers, int index) {

        if (index >= numbers.length - 1)
            return false;

        return numbers[index + 1] == 10 * numbers[index] || isNextValuesTime10(numbers, index + 1);
    }
}
