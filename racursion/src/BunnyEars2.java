import java.util.Scanner;

public class BunnyEars2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int bunny = Integer.parseInt(scanner.nextLine());


        System.out.println(computeBunnyEars(bunny));
    }

    private static int computeBunnyEars(int bunny) {
        int evenEars = 2;
        int oddEars = 3;
        if (bunny == 0) {
            return 0;
        } else if (bunny == 1) {
            return evenEars;
        }


        if (bunny % 2 == 0) {
            return oddEars + computeBunnyEars(bunny - 1);
        } else {
            return evenEars + computeBunnyEars(bunny - 1);
        }

    }
}
