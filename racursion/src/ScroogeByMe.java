import java.util.Arrays;
import java.util.Scanner;

public class ScroogeByMe {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        int row = scanner.nextInt();
        int col = scanner.nextInt();

        int[][] labyrinth = new int[row][col];

        int[] scroogeStartingCoordinates = new int[2];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                int inputValue = scanner.nextInt();
                labyrinth[i][j] = inputValue;
                if (inputValue == 0) {
                    scroogeStartingCoordinates[0] = i;
                    scroogeStartingCoordinates[1] = j;
                }
            }
        }

        int[] coins = {0};

        solveScroogeLabyrinth(labyrinth, coins, scroogeStartingCoordinates);

        System.out.println(coins[0]);
    }

    private static void solveScroogeLabyrinth(int[][] labyrinth, int[] coins, int[] scroogeStartingCoordinates) {
        int[] nextCell = getNextCell(labyrinth, scroogeStartingCoordinates[0], scroogeStartingCoordinates[1]);
        if (nextCell[0] == -1 || nextCell[1] == -1) {
            return;
        }

        coins[0]++;

        labyrinth[nextCell[0]][nextCell[1]]--; // намаляваме клетката където е стъпил Скрудж с 1, тъй като може да вземе само 1 монета
        solveScroogeLabyrinth(labyrinth, coins, nextCell);
    }

    private static int[] getNextCell(int[][] labyrinth, int scroogeStartingRow, int scroogeStartingCol) {

        int leftCoins = getCoins(labyrinth, scroogeStartingRow, scroogeStartingCol - 1);
        int rightCoins = getCoins(labyrinth, scroogeStartingRow, scroogeStartingCol + 1);
        int upCoins = getCoins(labyrinth, scroogeStartingRow - 1, scroogeStartingCol);
        int downCoins = getCoins(labyrinth, scroogeStartingRow + 1, scroogeStartingCol);
        int maxCoins = getMaxCoins(leftCoins, rightCoins, upCoins, downCoins);

        if (maxCoins == 0) {
            return new int[]{-1, -1};
        }
        if (maxCoins == leftCoins) {
            return new int[]{scroogeStartingRow, scroogeStartingCol - 1};
        }
        if (maxCoins == rightCoins) {
            return new int[]{scroogeStartingRow, scroogeStartingCol + 1};
        }
        // if (maxCoins == downCoins) {
        //     return new int[]{scroogeStartingRow + 1, scroogeStartingCol};
        // }
        if (maxCoins == upCoins) {
            return new int[]{scroogeStartingRow - 1, scroogeStartingCol}; // монетите нагоре
        }


        return new int[]{scroogeStartingRow + 1, scroogeStartingCol};
    }

    private static int getMaxCoins(int... coins) {
        int maxCoins = 0;
        for (int coin : coins) {
            if (coin > maxCoins) {
                maxCoins = coin;
            }
        }
        return maxCoins;
    }

    private static int getCoins(int[][] labyrinth, int scroogeStartingRow
            , int scroogeStartingCol) {

        return outOfLabyrinth(labyrinth, scroogeStartingRow, scroogeStartingCol) ? 0 : labyrinth[scroogeStartingRow][scroogeStartingCol];
    }

    private static boolean outOfLabyrinth(int[][] labyrinth, int scroogeStartingRow
            , int scroogeStartingCol) {

        return scroogeStartingRow >= labyrinth.length || scroogeStartingRow < 0
                || scroogeStartingCol >= labyrinth[scroogeStartingRow].length || scroogeStartingCol < 0;
    }


}
