import java.util.Scanner;

public class BunnyEars {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int bunny = Integer.parseInt(scanner.nextLine());


        System.out.println(computeBunnyEars(bunny));
    }

    private static int computeBunnyEars(int bunny) {
        int ears = 2;
        if (bunny == 0) {
            return 0;
        } else if (bunny == 1) {
            return 2;
        }

        return ears + computeBunnyEars(bunny - 1);
    }
}
