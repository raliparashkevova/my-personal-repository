import java.util.*;

public class Variations {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int lengthOfStrings = Integer.parseInt(scanner.nextLine());

        String symbols = scanner.nextLine().replaceAll(" ", "");
        char[] chars = symbols.toCharArray();


        Set<String> output = new TreeSet<>();
        findVariation(chars, "", symbols.length(), lengthOfStrings, output);

        printOutput(output);
    }

    private static void printOutput(Set<String> output) {

        for (String s : output) {
            System.out.println(s);
        }
    }

    private static void findVariation(char[] symbols, String prefix,
                                      int size, int lengthOfStrings,
                                      Set<String> result) {

        if (lengthOfStrings == 0) {
            result.add(prefix);
            return;

        }

        for (int i = 0; i < size; i++) {
            String newest = prefix + symbols[i];

            findVariation(symbols, newest, size, lengthOfStrings - 1, result);
        }


    }


}
