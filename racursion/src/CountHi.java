import java.util.Scanner;

public class CountHi {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String input = scanner.nextLine();
        String filter = "hi";

        int count = getCountHi(input, filter);

        System.out.println(count);

    }

    private static int getCountHi(String input, String filter) {

        if (input.length() == 0 || input.length() < filter.length()) {
            return 0;
        }

        if (input.substring(0, filter.length()).equals(filter)) {
            return 1 + getCountHi(input.substring(filter.length() - 1), filter);
        }

        return getCountHi(input.substring(filter.length() - 1), filter);
    }
}
