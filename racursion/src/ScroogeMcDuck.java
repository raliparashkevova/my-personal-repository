import java.util.Arrays;
import java.util.Scanner;

public class ScroogeMcDuck {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int rows = scanner.nextInt();
        int cols = scanner.nextInt();

        int[][] maze = new int[rows][cols];

        int[] startingPoints = new int[2]; // по този начин метода може да ни върне две стойности

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                int temp = scanner.nextInt(); // Пълним матрицата //nextInt слиза сам на нов ред и си върви по реда.  И ако има празно пространство то сам го пропуска тъй като не е инт
                maze[row][col] = temp;
                if (temp == 0) {
                    startingPoints[0] = row;
                    startingPoints[1] = col;
                }
            }
        }

        int[] coins = {0}; // замества статична променлива, използва се int а не
        // wrapper класа, тъй като той е Immutable kato Стринг, a pprimitive инт работи по стойност

        solveMaze(maze, coins, startingPoints);


        System.out.println(coins[0]);
    }

    private static void solveMaze(int[][] maze, int[] coins, int[] startingPoints) {

        int[] nextCell = calculateNextCell(maze, startingPoints[0], startingPoints[1]);

        if (nextCell[0] == -1 && nextCell[1] == -1) { // Това е дъното на метода/ракурсията тоест те са невалидни
            return;
        }
        coins[0]++;
        maze[nextCell[0]][nextCell[1]]--;
        solveMaze(maze, coins, nextCell);
    }

    private static int[] calculateNextCell(int[][] maze, int row, int col) {
        int leftCoins = getCoins(maze, row, col - 1);
        int rightCoins = getCoins(maze, row, col + 1);
        int downCoins = getCoins(maze, row + 1, col);
        int upCoins = getCoins(maze, row - 1, col);
        int maxCoins = getMaxCoins(leftCoins, rightCoins, downCoins, upCoins);

        if (maxCoins == 0) {
            return new int[]{-1, -1};
        }
        if (maxCoins == leftCoins) {
            return new int[]{row, col - 1};
        }
        if (maxCoins == rightCoins) {
            return new int[]{row, col + 1};
        }
        if (maxCoins == downCoins) {
            return new int[]{row + 1, col};
        }
        if (maxCoins == upCoins) {
            return new int[]{row - 1, col};
        }

        return new int[]{row + 1, col};
    }

    private static int getMaxCoins(int... coins) {

        int maxCoins = 0;

        for (int coin : coins) {
            if (coin > maxCoins) {
                maxCoins = coin;
            }
        }
        return maxCoins;
    }

    private static int getCoins(int[][] maze, int row, int col) {

        return outOfMaze(maze, row, col) ? 0 : maze[row][col];
    }

    private static boolean outOfMaze(int[][] maze, int row, int col) {
        return row >= maze.length || row < 0 || col >= maze[row].length || col < 0;
    }
}
