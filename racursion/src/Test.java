import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int rows = scanner.nextInt();
        int cols = scanner.nextInt();

        int[][] maze = new int[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                maze[row][col] = scanner.nextInt();
            }
        }

        boolean[][] visited = new boolean[rows][cols];
        int maxCells = 0;

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (visited[row][col]) {
                    continue;
                }
                int count = explore(maze, visited, row, col, maze[row][col]);
                maxCells = Math.max(maxCells, count);
            }
        }

        System.out.println(maxCells);
    }

    private static int explore(int[][] maze, boolean[][] visited, int row, int col, int currentValue) {
        if (outOfMaze(maze, row, col)) {
            return 0;
        }

        if (visited[row][col]) {
            return 0;
        }

        if (maze[row][col] != currentValue) {
            return 0;
        }

        visited[row][col] = true;

        return explore(maze, visited, row - 1, col, currentValue) +
                explore(maze, visited, row + 1, col, currentValue) +
                explore(maze, visited, row, col - 1, currentValue) +
                explore(maze, visited, row, col + 1, currentValue) + 1;
    }

    private static boolean outOfMaze(int[][] maze, int row, int col) {
        return row >= maze.length || row < 0 || col >= maze[row].length || col < 0;
    }
}
