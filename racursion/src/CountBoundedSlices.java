import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

public class CountBoundedSlices {

    public static void main(String[] args) {


        int k = 2;
        int[] numbers = {3, 5, 7, 6, 3};
        int result = solution(k, numbers);
        System.out.println(result);

    }

    private static int solution(int K, int[] A) {

        int countSlice = 0;

      int size = A.length;

        for (int i = 0; i < size; i++) {
            int max = A[i];
            int min = A[i];
            for (int j = i; j < size; j++) {

                max = Math.max(max,A[j]);
               min = Math.min(min,A[j]);
                int diff = max - min;
                if (diff <= K ){
                    countSlice++;

                }
            }
        }

        if (countSlice > 1000000000){
            return 1000000000;
        }


        return countSlice;
    }
}
