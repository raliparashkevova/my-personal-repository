import java.util.Scanner;

public class PowerN {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        int baseNumber = Integer.parseInt(scanner.nextLine());

        int power = Integer.parseInt(scanner.nextLine());

        int result = getBasePower(baseNumber, power);
        System.out.println(result);


    }

    private static int getBasePower(int baseNumber, int power) {

        if (power == 0)
            return 1;
        else
            return baseNumber * getBasePower(baseNumber, power - 1);
    }


}
