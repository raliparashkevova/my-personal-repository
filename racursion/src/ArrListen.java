
import java.util.Arrays;
import java.util.LinkedList;

import java.util.List;
import java.util.Scanner;

public class ArrListen {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] A = {1, 4, -1, 3, 2};
//        int[] A = {-1,2};

        int result = solution(A);
        System.out.println(result);
    }

    private static int solution(int[] A) {


        int count = 1;
        int value = A[0];

        while (value!=-1){
            value = A[value];
            count++;
        }



        return count;
    }


}