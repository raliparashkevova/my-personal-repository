import java.util.Arrays;
import java.util.Scanner;

public class ArraysContains11 {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] numbers = Arrays.stream(scanner.nextLine().split(","))
                .mapToInt(Integer::parseInt).toArray();

        int index = Integer.parseInt(scanner.nextLine());

        int count = 0;

        getCountIfArraysContains11(numbers, index, count);
    }

    private static void getCountIfArraysContains11(int[] numbers, int index, int count) {

        if (index >= numbers.length) {
            System.out.println(count);
            return;
        }

        if (numbers[index] == 11) {
            count++;
        }

        getCountIfArraysContains11(numbers, ++index, count);
    }
}
