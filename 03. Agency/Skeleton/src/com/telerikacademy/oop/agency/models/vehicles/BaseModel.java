package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.contracts.Identifiable;
import com.telerikacademy.oop.agency.models.contracts.Printable;

public class BaseModel implements Identifiable {

    private int id;

    public BaseModel(int id) {
        this.id = id;
    }


    @Override
    public int getId() {
        return this.id;
    }


}
