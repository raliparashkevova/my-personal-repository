package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class TrainImpl extends BaseVehicle implements Train {
    public static final int PASSENGER_MIN_VALUE = 30;
    public static final int PASSENGER_MAX_VALUE = 150;
    public static final int CARTS_MIN_VALUE = 1;
    public static final int CARTS_MAX_VALUE = 15;
    public static final double PRICE_MIN_VALUE = 0.1;
    public static final double PRICE_MAX_VALUE = 2.5;

    private int carts;

    public TrainImpl(int id, int passengerCapacity, double pricePerKilometer, int carts) {
        super(id, passengerCapacity, pricePerKilometer);
        setCarts(carts);
    }

    @Override
    protected void validatePassengerCapacity(int passengerCapacity) {

        if (passengerCapacity < PASSENGER_MIN_VALUE || passengerCapacity > PASSENGER_MAX_VALUE) {
            throw new IllegalArgumentException
                    (String.format("A train cannot have less than %d passengers or more than %d passengers."
                            , PASSENGER_MIN_VALUE, PASSENGER_MAX_VALUE));
        }


    }

    @Override
    protected void validatePricePerKilometer(double pricePerKilometer) {

        ValidationHelper.validateValueInRange(pricePerKilometer, PRICE_MIN_VALUE, PRICE_MAX_VALUE
                , String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $.2f cannot exist!"
                        , PRICE_MIN_VALUE, PRICE_MAX_VALUE));

    }


    @Override
    public int getCarts() {
        return this.carts;
    }

    public void setCarts(int carts) {
        if (carts < CARTS_MIN_VALUE || carts > CARTS_MAX_VALUE) {
            throw new IllegalArgumentException(String.format
                    ("A train cannot have less than %d cart or more than %d carts.", CARTS_MIN_VALUE, CARTS_MAX_VALUE));
        }
        this.carts = carts;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.LAND;
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Train ----");
        sb.append(System.lineSeparator());
        sb.append(super.getAsString());
        sb.append(String.format("Carts amount: %d", getCarts()));
        sb.append(System.lineSeparator());


        return sb.toString();
    }
}