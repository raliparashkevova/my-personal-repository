package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;
import com.telerikacademy.oop.agency.models.vehicles.BaseModel;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class TicketImpl extends BaseModel implements Ticket {

    private Journey journey;
    private double administrativeCosts;


    public TicketImpl(int id, Journey journey, double costs) {
        super(id);
        this.journey = journey;
        setAdministrativeCosts(costs);

    }


    @Override
    public Journey getJourney() {
        return this.journey;
    }

    public void setAdministrativeCosts(double administrativeCosts) {
        if (administrativeCosts < 0.0) {
            throw new InvalidUserInputException
                    (String.format("Value of 'costs' must be a positive number. Actual value: %.2f."
                            , administrativeCosts));
        }
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return this.administrativeCosts;
    }

    @Override
    public double calculatePrice() {

        double travelCosts = journey.calculateTravelCosts();


        return getAdministrativeCosts() * travelCosts;
    }

    @Override
    public String getAsString() {

        StringBuilder sb = new StringBuilder();

        double calculatePrice = calculatePrice();
        sb.append("Ticket ----");
        sb.append(System.lineSeparator());
        sb.append(String.format("Destination: %s", journey.getDestination()));
        sb.append(System.lineSeparator());
        sb.append(String.format("Price: %.2f", calculatePrice));


        return sb.toString();
    }
}
