package com.telerikacademy.oop.agency.models.vehicles;


import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.JOIN_DELIMITER;

public abstract class BaseVehicle implements Vehicle {

    private int id;
    private int passengerCapacity;
    private double pricePerKilometer;


    public BaseVehicle(int id, int passengerCapacity, double pricePerKilometer) {
        setId(id);
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }


    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getPassengerCapacity() {
        return this.passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return this.pricePerKilometer;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        validatePassengerCapacity(passengerCapacity);
        this.passengerCapacity = passengerCapacity;
    }

    public void setPricePerKilometer(double pricePerKilometer) {
        validatePricePerKilometer(pricePerKilometer);
        this.pricePerKilometer = pricePerKilometer;
    }

    protected abstract void validatePassengerCapacity(int passengerCapacity);

    protected abstract void validatePricePerKilometer(double pricePerKilometer);

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String getAsString() {

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Passenger capacity: %d", getPassengerCapacity()));
        sb.append(System.lineSeparator());
        sb.append(String.format("Price per kilometer: %.2f", getPricePerKilometer()));
        sb.append(System.lineSeparator());


        return sb.toString();
    }
}
