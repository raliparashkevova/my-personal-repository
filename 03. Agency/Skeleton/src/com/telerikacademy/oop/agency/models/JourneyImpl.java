package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.contracts.Identifiable;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.BaseModel;
import com.telerikacademy.oop.agency.models.vehicles.BaseVehicle;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class JourneyImpl extends BaseModel implements Journey {

    public static final int START_LOCATION_MIN_LENGTH = 5;
    public static final int START_LOCATION_MAX_LENGTH = 25;
    public static final int DESTINATION_MIN_LENGTH = 5;
    public static final int DESTINATION_MAX_LENGTH = 25;
    public static final int DISTANCE_MIN_VALUE = 5;
    public static final int DISTANCE_MAX_VALUE = 5000;


    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;
    private Journey journey;


    public JourneyImpl(int id, String startLocation, String destination, int distance, Vehicle vehicle) {
        super(id);
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        this.vehicle = vehicle;

    }


    @Override
    public int getDistance() {
        return this.distance;
    }

    @Override
    public Vehicle getVehicle() {
        return this.vehicle;
    }

    @Override
    public String getDestination() {
        return this.destination;
    }

    @Override
    public double calculateTravelCosts() {


        return getDistance() * vehicle.getPricePerKilometer();
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {

        ValidationHelper.validateStringLength(startLocation, START_LOCATION_MIN_LENGTH,
                START_LOCATION_MAX_LENGTH,
                String.format("The StartingLocation's length cannot be less than %d or more than %d symbols long.",
                        START_LOCATION_MIN_LENGTH, START_LOCATION_MAX_LENGTH));
        this.startLocation = startLocation;
    }

    public void setDestination(String destination) {

        ValidationHelper.validateStringLength(destination, DESTINATION_MIN_LENGTH,
                DESTINATION_MAX_LENGTH,
                String.format("The Destination's length cannot be less than %d or more than %d symbols long.",
                        DESTINATION_MIN_LENGTH, DESTINATION_MAX_LENGTH));
        this.destination = destination;
    }

    public void setDistance(int distance) {
        if (distance < DISTANCE_MIN_VALUE || distance > DISTANCE_MAX_VALUE) {
            throw new IllegalArgumentException
                    (String.format("The Distance cannot be less than %d or more than %d kilometers."
                            , DISTANCE_MIN_VALUE, DISTANCE_MAX_VALUE));
        }
        this.distance = distance;
    }


    @Override
    public String getAsString() {

        StringBuilder sb = new StringBuilder();


        double travelCosts = calculateTravelCosts();
        sb.append("Journey ----");
        sb.append(System.lineSeparator());
        sb.append(String.format("Start location: %s", getStartLocation()));
        sb.append(System.lineSeparator());
        sb.append(String.format("Destination: %s", getDestination()));
        sb.append(System.lineSeparator());
        sb.append(String.format("Vehicle type: %s", getVehicle().getType()));
        sb.append(System.lineSeparator());
        sb.append(String.format("Travel costs: %.2f", travelCosts));
        sb.append(System.lineSeparator());
        sb.append(JOIN_DELIMITER);


        return sb.toString();
    }

}