package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class AirplaneImpl extends BaseVehicle implements Airplane {


    private boolean hasFreeFood;

    public static final int PASSENGER_MIN_VALUE = 1;
    public static final int PASSENGER_MAX_VALUE = 800;
    public static final double PRICE_MIN_VALUE = 0.1;
    public static final double PRICE_MAX_VALUE = 2.5;

    public AirplaneImpl(int id, int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(id,
                passengerCapacity, pricePerKilometer);
        this.hasFreeFood = hasFreeFood;
    }

    public boolean isHasFreeFood() {
        return hasFreeFood;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.AIR;
    }

    @Override
    protected void validatePassengerCapacity(int passengerCapacity) {

        if (passengerCapacity < PASSENGER_MIN_VALUE || passengerCapacity > PASSENGER_MAX_VALUE) {
            throw new IllegalArgumentException
                    (String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!"
                            , PASSENGER_MIN_VALUE, PASSENGER_MAX_VALUE));

        }
    }

    @Override
    protected void validatePricePerKilometer(double pricePerKilometer) {
        ValidationHelper.validateValueInRange
                (pricePerKilometer, PRICE_MIN_VALUE, PRICE_MAX_VALUE
                        , String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $.2f cannot exist!"
                                , PRICE_MIN_VALUE, PRICE_MAX_VALUE));

    }

    @Override
    public boolean hasFreeFood() {
        return false;
    }

    @Override
    public String getAsString() {

        StringBuilder sb = new StringBuilder();

        sb.append("Airplane ----");
        sb.append(System.lineSeparator());
        sb.append(super.getAsString());
        sb.append(String.format("Has free food: %b", isHasFreeFood()));
        sb.append(System.lineSeparator());


        return sb.toString();
    }
}
