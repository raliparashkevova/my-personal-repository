package com.telerikacademy.oop.agency.commands.creation;

import com.telerikacademy.oop.agency.commands.contracts.Command;
import com.telerikacademy.oop.agency.core.contracts.AgencyRepository;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.TICKET_CREATED_MESSAGE;

import static com.telerikacademy.oop.agency.utils.ParsingHelpers.*;

public class CreateTicketCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final AgencyRepository agencyRepository;

    private Journey journeyID;
    private double administrativeCosts;

    public CreateTicketCommand(AgencyRepository agencyRepository) {
        this.agencyRepository = agencyRepository;
    }

    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Ticket createTicket = agencyRepository.createTicket(journeyID, administrativeCosts);

        return String.format(TICKET_CREATED_MESSAGE, createTicket.getId());
    }

    private void parseParameters(List<String> parameters) {

        administrativeCosts = tryParseDouble(parameters.get(1), "administrativeCosts");
        journeyID = agencyRepository.findElementById(agencyRepository.getJourneys(), tryParseInteger(parameters.get(0), "journeyId"));
    }

}