package com.telerikacademy.dsa.queue;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {
    private final int DEFAULT_CAPACITY = 5;
    private E[] items;
    private int head, tail, size;
    private int queueSize = DEFAULT_CAPACITY;


    public ArrayQueue() {
        items = (E[]) new Object[queueSize];
        head = 0;
        tail = 0;
        size = 0;
    }

    @Override
    public void enqueue(E element) {
        if (size() >= items.length) {
            queueSize += queueSize;
            items = Arrays.copyOf(items, queueSize);
        }

        items[tail++] = element;
        size++;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException("There are no elements in the arrayQueue");
        }

        E removedElement = items[head];

        for (int i = head; i < tail - 1; i++) {
            items[i] = items[i + 1];
        }
        tail--;
        size--;


        return removedElement;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("There is no elements in the arrayQueue");
        }

        return items[head];
    }

    @Override
    public int size() {
        return tail;
    }

    @Override
    public boolean isEmpty() {
        if (tail == 0) {
            return true;
        }

        return false;
    }

}
