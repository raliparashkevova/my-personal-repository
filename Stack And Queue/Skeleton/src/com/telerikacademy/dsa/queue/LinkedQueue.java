package com.telerikacademy.dsa.queue;

import com.telerikacademy.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedQueue<E> implements Queue<E> {
    private Node<E> head, tail;
    private int size;


    public LinkedQueue() {
        this.head = null;
        tail = null;
        size = 0;
    }

    @Override
    public void enqueue(E element) {
        Node node = new Node(element);
        if (head == null) {
            head = node;
            tail = node;
            size++;
        } else {
            tail.next = node;
            tail = node;
            size++;
        }

    }

    // вземаме от хеда тъй като имаме референция към тейл
    @Override
    public E dequeue() {
        throwIfEmpty();
        E result = head.data;
        if (head.next == null) {
            tail = null;
        }
        head = head.next;
        size--;

        return result;
    }

    private void throwIfEmpty() {
        if (isEmpty()) {
            throw new NoSuchElementException("There are no elements in the linkedStack");
        }
    }

    @Override
    public E peek() {
        throwIfEmpty();
        return head.data;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}
