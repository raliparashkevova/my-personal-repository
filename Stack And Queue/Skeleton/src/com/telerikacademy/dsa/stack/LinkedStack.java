package com.telerikacademy.dsa.stack;

import com.telerikacademy.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedStack<E> implements Stack<E> {
    private Node<E> top;
    private int size;


    public LinkedStack() {
        top = null;
        size = 0;
    }

    @Override
    public void push(E element) {
        Node node = new Node(element); // тук го създаваме за да създадем първият адрес
        node.next = top; // казваме новият нод сочи към топ,
        top = node;
        size++;

    }

    @Override
    public E pop() {
        throwIfEmpty();
        E result = top.data;
        top = top.next;
        size--;

        return result;
    }

    private void throwIfEmpty() {
        if (isEmpty()) {
            throw new NoSuchElementException("There is no elements in the linkedStack");
        }
    }

    @Override
    public E peek() {
        throwIfEmpty();
        return top.data;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {


        return size() == 0;
    }
}
