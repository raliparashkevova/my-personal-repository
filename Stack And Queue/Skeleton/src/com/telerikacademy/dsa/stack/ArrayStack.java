package com.telerikacademy.dsa.stack;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;

//Stack => Last-In-First-Oot
public class ArrayStack<E> implements Stack<E> {
    private final int DEFAULT_CAPACITY = 10;

    private E[] items;
    private int top;
    private int size = DEFAULT_CAPACITY;

    public ArrayStack() { // конструктора за инициализиране на полетата
        this.items = (E[]) new Object[size]; // паметта е заета и инициализиране още при инициализацията на масива

        this.top = 0;
    }

    @Override
    public void push(E element) {

        if (top >= items.length) {
            size += DEFAULT_CAPACITY;
            items = Arrays.copyOf(items, size);
        }

        // друг вариант на рисайзване е да използваме copy of
        // items = Array.copyOf(items, items.length *2)
        items[top] = element;
        top++;
    }

    @Override
    public E pop() {
        throwIfEmpty();
        E removeElement = items[top - 1]; // щом е минало проверка значи съществува помне един елемент
        top--;
//и затова няма освобождаване на паметта, тоест не трябва да презаписване в нов масив
        //items = Arrays.copyOfRange(items,0,top); => не е необходимо

        return removeElement;
    }

    @Override
    public E peek() { // да видим какво е най-отгоре в стака или в купа в чинии без да я вземаме
        throwIfEmpty();
        return items[top - 1];
    }

    private void throwIfEmpty() {
        if (isEmpty()) {
            throw new NoSuchElementException("There are no elements in the arrayStack");
        }
    }

    @Override
    public int size() {

        return top;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

}
