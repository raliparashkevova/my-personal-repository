package com.telerikacademy.dsa;

import com.telerikacademy.dsa.queue.ArrayQueue;
import com.telerikacademy.dsa.stack.LinkedStack;

import java.util.ArrayDeque;

public class Main {
    public static void main(String[] args) {

        //First-In-First-Out
        ArrayQueue<Character> queue = new ArrayQueue<>();
        queue.enqueue('h');
        queue.enqueue('e');
        queue.enqueue('l');
        queue.enqueue('l');
        queue.enqueue('0');

        // queue.enqueue('g');


        while (!queue.isEmpty()) {
            System.out.println(queue.dequeue());
        }


    }
}
