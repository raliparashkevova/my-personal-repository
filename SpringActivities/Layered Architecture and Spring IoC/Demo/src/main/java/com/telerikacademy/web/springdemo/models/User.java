package com.telerikacademy.web.springdemo.models;

public class User {

    @Positive(message = "Should be positive number")
    @NotNull
    private int id;

    @NotNull(message = "Name can't be empty")
    @NotBlank(message = "Name can't be blanc")
    @Size(min = 3, max = 30, message = "The name should be between 3 and 30 symbols")
    private String name;

    @NotNull(message = "Email can't be empty")
    @Email
    private String email;


    public User() {
    }

    public User(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
