package com.telerikacademy.web.springdemo.controllers;

public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping //тук няма Url, но тези които нямат вземат ос
    public List<User> getAll() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@Valid @PathVariable int id) {

        try {  // тук използваме try catch Консътрукция за да прихванем грешката,
            // която може да бъде хвърлена по-надолу , а именно в Persistence layer-a, в койти е репоситорито, където може да възникне грешката
            return userService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage()
            );
        }
    }


    @PostMapping // raboti през основният url зададен в requestMaping-a
    public User createUser(@Valid @RequestBody User user) { // Тук е RequestBody тъй като приемаме и ще създадваме нов обект
        try {
            userService.createUser(user);
        } catch (DuplicateEntityException duplicateEntityException) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, duplicateEntityException.getMessage());
        } catch (EntityNotFoundException entityNotFoundException) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, entityNotFoundException.getMessage());
        }


        return user;
    }

    @PutMapping("/{id}") //("/update")  или //("/update{id}")
    public User updateUser(@RequestParam int id, @Valid @RequestBody User user) {

        // тук може да бъде и
        // User updateUser = getByIdHelper(user.getId()); Тук на метода подаваме ай дито на юзера отвън
        // и нямаме екстрактното ай ги като path variable
        try {
            userService.updateUser(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException d) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, d.getMessage());
        }

        return user;
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {

        try {
            userService.deleteUser(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }
}

