import com.taskmanagеment.core.TaskManagementEngineImpl;
import com.taskmanagеment.models.ActivityHistoryImpl;
import com.taskmanagеment.models.BugImpl;
import com.taskmanagеment.models.MemberImpl;
import com.taskmanagеment.models.contracts.ActivityHistory;
import com.taskmanagеment.models.contracts.Bug;
import com.taskmanagеment.models.contracts.Member;
import com.taskmanagеment.models.enums.BugStatus;
import com.taskmanagеment.models.enums.Priority;
import com.taskmanagеment.models.enums.Severity;

import java.time.LocalDate;

public class StartUp {
    public static void main(String[] args) {

        TaskManagementEngineImpl engine = new TaskManagementEngineImpl();
        engine.start();

    }
}
