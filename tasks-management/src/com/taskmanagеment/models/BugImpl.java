package com.taskmanagеment.models;

import com.taskmanagеment.models.contracts.Bug;
import com.taskmanagеment.models.contracts.BugStory;
import com.taskmanagеment.models.enums.BugStatus;
import com.taskmanagеment.models.enums.Priority;
import com.taskmanagеment.models.enums.Severity;
import com.taskmanagеment.models.enums.TaskType;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BugImpl extends BaseBugStory implements Bug, BugStory {

    private static final BugStatus initialStatus = BugStatus.ACTIVE;
    private static final BugStatus finalStatus = BugStatus.FIXED;
    private static final Severity initialSeverity = Severity.CRITICAL;
    private static final Severity finalSeverity = Severity.MINOR;
    private static final Priority initialPriority = Priority.HIGH;
    private static final Priority finalPriority = Priority.LOW;


    private Severity severity;
    private BugStatus bugStatus;

    private final List<String> stepsToReproduce;

    public BugImpl(int id, String title, String description, Priority priority, Severity severity, BugStatus bugStatus, String assignee) {
        super(id, title, description, priority, assignee);
        this.severity = severity;
        this.bugStatus = bugStatus;
        this.stepsToReproduce = new ArrayList<>();

    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public BugStatus getBugStatus() {
        return bugStatus;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public String getAssignee() {
        return super.getAssignee();
    }

    @Override
    public void addStepToReproduce(String step) {

        stepsToReproduce.add(step);
    }

    private void setSeverity(Severity severity) {
        this.severity = severity;
    }

    private void setBugStatus(BugStatus bugStatus) {
        this.bugStatus = bugStatus;
    }

    @Override
    public void removeStepToReproduce(String step) {
        stepsToReproduce.remove(step);
    }

    @Override
    public void advanceSeverity() {
        Severity current = getSeverity();
        Severity nextSeverity = getSeverity().next();
        if (current == initialSeverity) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Severity changed from %s to %s", current, nextSeverity), LocalDateTime.now()));

            setSeverity(nextSeverity);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't advance, already Severity is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void revertSeverity() {
        Severity current = getSeverity();
        Severity previous = getSeverity().previous();

        if (current != initialSeverity) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Severity changed from %s to %s", current, previous), LocalDateTime.now()));
            setSeverity(previous);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't revert, already Severity is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void advanceStatus() {
        BugStatus current = getBugStatus();
        BugStatus nextStatus = getBugStatus().next();
        if (current == initialStatus) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Bug status changed from %s to %s", current, nextStatus), LocalDateTime.now()));

            setBugStatus(nextStatus);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't advance, already Severity is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void revertStatus() {
        BugStatus current = getBugStatus();
        BugStatus previous = getBugStatus().previous();

        if (current != initialStatus) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Bug status changed from %s to %s", current, previous), LocalDateTime.now()));
            setBugStatus(previous);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't revert, already Bug status is %s ", current), LocalDateTime.now()));
        }

    }

    @Override
    public TaskType getType() {
        return TaskType.BUG;
    }

    @Override
    public void advancePriority() {
        Priority current = getPriority();
        Priority nextPriority = getPriority().next();
        if (current == initialPriority) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Priority changed from %s to %s", current, nextPriority), LocalDateTime.now()));

            setPriority(nextPriority);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't advance, already Priority is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void revertPriority() {
        Priority current = getPriority();
        Priority previous = getPriority().previous();

        if (current != initialPriority) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Priority changed from %s to %s", current, previous), LocalDateTime.now()));
            setPriority(previous);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't revert, already Priority is %s ", current), LocalDateTime.now()));
        }

    }

    @Override
    public void changeBugPriority(Priority priority) {
        getActiveHistory().add(new ActivityHistoryImpl(String.format("Priority was changed from %s to %s", getPriority(), priority), LocalDateTime.now()));
        setPriority(priority);
    }

    @Override
    public void changeBugSeverity(Severity severity) {
        getActiveHistory().add(new ActivityHistoryImpl(String.format("Severity was changed from %s to %s", getSeverity(), severity), LocalDateTime.now()));
        setSeverity(severity);
    }

    @Override
    public void changeBugStatus(BugStatus bugStatus) {
        getActiveHistory().add(new ActivityHistoryImpl(String.format("Bug status was changed from %s to %s", getBugStatus(), bugStatus), LocalDateTime.now()));

        setBugStatus(bugStatus);
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();

        sb.append(super.getAsString());
        sb.append(String.format("Severity: %s\n", getSeverity()));
        sb.append(String.format("Bug status: %s\n", getBugStatus()));

        return sb.toString().trim();
    }
}
