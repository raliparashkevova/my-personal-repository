package com.taskmanagеment.models;

import com.taskmanagеment.models.contracts.ActivityHistory;
import com.taskmanagеment.models.contracts.BaseModel;
import com.taskmanagеment.models.contracts.Comment;
import com.taskmanagеment.models.contracts.Task;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.taskmanagеment.Constants.ModelConstants.*;

public abstract class BaseModelImpl implements BaseModel {

    private int id;
    private String name;
    private List<Task> tasks;
    private List<ActivityHistory> activityHistories;


    public BaseModelImpl(int id, String name) {
        this.id = id;
        setName(name);
        this.tasks = new ArrayList<>();
        this.activityHistories = new ArrayList<>();

    }

    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        validateName(name);
        this.name = name;
    }

    @Override
    public int getId() {
        return id;
    }

    protected abstract void validateName(String name);

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public List<ActivityHistory> getActiveHistory() {
        return new ArrayList<>(activityHistories);
    }


    @Override
    public void addTask(Task task) {
        this.tasks.add(task);
        activityHistories.add(new ActivityHistoryImpl("Task was added", LocalDateTime.now()));
    }

    @Override
    public void removeTask(Task task) {
        this.tasks.remove(task);
        activityHistories.add(new ActivityHistoryImpl("Task was removed", LocalDateTime.now()));
    }

    @Override
    public void addActivityHistory(ActivityHistory activityHistory) {
        this.activityHistories.add(activityHistory);

    }

    @Override
    public void removeActivityHistory(ActivityHistory activityHistory) {
        this.activityHistories.remove(activityHistory);

    }

    private String printTasks() {

        StringBuilder sb = new StringBuilder();

        if (tasks.isEmpty()) {
            sb.append(NO_TASK);
            sb.append(String.format("%s\n", TASK_HEADER));
        } else {
            for (Task task : tasks) {
                sb.append("----------");
                sb.append(task.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    private String printActivityHistory() {

        StringBuilder sb = new StringBuilder();


        if (activityHistories.isEmpty()) {
            sb.append(NO_ACTIVITY_HISTORY_HEADER);
            sb.append(String.format("%s\n", ACTIVITY_HISTORY_HEADER));
        } else {
            for (ActivityHistory activityHistory : activityHistories) {
                sb.append("----------");
                sb.append(activityHistory.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Name: %s\n", getName()));
        sb.append(printTasks());
        sb.append(printActivityHistory());


        return sb.toString().trim();
    }


}
