package com.taskmanagеment.models;

import com.taskmanagеment.models.contracts.Member;
import com.taskmanagеment.models.contracts.Story;
import com.taskmanagеment.models.enums.Priority;
import com.taskmanagеment.models.enums.Size;
import com.taskmanagеment.models.enums.StoryStatus;
import com.taskmanagеment.models.enums.TaskType;

import java.time.LocalDateTime;


public class StoryImpl extends BaseBugStory implements Story {
    private static final Priority initialPriority = Priority.HIGH;
    private static final Priority finalPriority = Priority.LOW;
    private static final StoryStatus initialStoryStatus = StoryStatus.NOTDONE;
    private static final StoryStatus finalStoryStatus = StoryStatus.DONE;
    private static final Size initialSize = Size.LARGE;
    private static final Size finalSize = Size.SMALL;

    private Size size;
    private StoryStatus storyStatus;


    public StoryImpl(int id, String title, String description, Priority priority, Size size, StoryStatus storyStatus, String assignee) {
        super(id, title, description, priority, assignee);
        this.size = size;
        this.storyStatus = storyStatus;

    }


    @Override
    public Size getSize() {
        return size;
    }

    private void setSize(Size size) {
        this.size = size;
    }

    private void setStoryStatus(StoryStatus storyStatus) {
        this.storyStatus = storyStatus;
    }


    @Override
    public StoryStatus getStoryStatus() {
        return storyStatus;
    }

    @Override
    public String getAssignee() {
        return super.getAssignee();
    }

    @Override
    public void advanceSize() {
        Size current = getSize();
        Size nextSize = getSize().next();
        if (current == initialSize) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Size changed from %s to %s", current, nextSize), LocalDateTime.now()));

            setSize(nextSize);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't advance, already Size is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void revertSize() {
        Size current = getSize();
        Size previousSize = getSize().previous();
        if (current != initialSize) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Size changed from %s to %s", current, previousSize), LocalDateTime.now()));
            setSize(previousSize);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't revert, already Size is %s ", current), LocalDateTime.now()));
        }

    }

    @Override
    public void changeStoryPriority(Priority priority) {
        getActiveHistory().add(new ActivityHistoryImpl(String.format("Priority was changed from %s to %s", getPriority(), priority), LocalDateTime.now()));

        setPriority(priority);
    }


    @Override
    public void advanceStatus() {
        StoryStatus current = getStoryStatus();
        StoryStatus nextStoryStatus = getStoryStatus().next();
        if (current == initialStoryStatus) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Story status changed from %s to %s", current, nextStoryStatus), LocalDateTime.now()));

            setStoryStatus(nextStoryStatus);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't advance, already Story status is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void revertStatus() {
        StoryStatus current = getStoryStatus();
        StoryStatus previous = getStoryStatus().previous();

        if (current != initialStoryStatus) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Story status changed from %s to %s", current, previous), LocalDateTime.now()));
            setStoryStatus(previous);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't revert, already Story status is %s ", current), LocalDateTime.now()));
        }

    }

    @Override
    public TaskType getType() {
        return TaskType.STORY;
    }

    @Override
    public void advancePriority() {
        Priority current = getPriority();
        Priority nextPriority = getPriority().next();
        if (current == initialPriority) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Priority changed from %s to %s", current, nextPriority), LocalDateTime.now()));

            setPriority(nextPriority);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't advance, already Priority is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void revertPriority() {
        Priority current = getPriority();
        Priority previous = getPriority().previous();

        if (current != initialPriority) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Priority changed from %s to %s", current, previous), LocalDateTime.now()));
            setPriority(previous);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't revert, already Priority is %s ", current), LocalDateTime.now()));
        }

    }

    @Override
    public void changeSize(Size size) {

        getActiveHistory().add(new ActivityHistoryImpl(String.format("Size was changed from %s to %s", getSize(), size), LocalDateTime.now()));

        setSize(size);
    }

    @Override
    public void changeStoryStatus(StoryStatus storyStatus) {
        getActiveHistory().add(new ActivityHistoryImpl(String.format("Story status was changed from %s to %s", getStoryStatus(), storyStatus), LocalDateTime.now()));

        setStoryStatus(storyStatus);
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.getAsString());
        sb.append(String.format("Size: %s\n", getSize()));
        sb.append(String.format("Story status: %s\n", getStoryStatus()));


        return sb.toString().trim();
    }
}
