package com.taskmanagеment.models;

import com.taskmanagеment.models.contracts.Comment;

public class CommentImpl implements Comment {
    private String content;
    private String author;


    public CommentImpl(String content, String author) {

        this.content = content;
        this.author = author;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Author: %s\n", getAuthor()));
        sb.append(String.format("Content: %s\n", getContent()));
        return sb.toString();
    }
}
