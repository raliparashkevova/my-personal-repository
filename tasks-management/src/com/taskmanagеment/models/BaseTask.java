package com.taskmanagеment.models;

import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.Constants.OutputMessages;
import com.taskmanagеment.models.contracts.ActivityHistory;
import com.taskmanagеment.models.contracts.Comment;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.utils.ValidationHelpers;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.taskmanagеment.Constants.ModelConstants.*;
import static com.taskmanagеment.Constants.OutputMessages.*;

public abstract class BaseTask implements Task {

    private int id;
    private String title;
    private String description;
    private List<Comment> comments;
    private List<ActivityHistory> activityHistories;

    public BaseTask(int id, String title, String description) {
        this.id = id;
        setTitle(title);
        setDescription(description);
        this.comments = new ArrayList<>();
        this.activityHistories = new ArrayList<>();
    }


    private void setTitle(String title) {
        ValidationHelpers.validateInRange(title.length(), TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, TITLE_ERR);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelpers.validateInRange(description.length(), DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH, DESCRIPTION_ERR);
        this.description = description;
    }


    @Override
    public int getId() {
        return id;
    }


    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<ActivityHistory> getActiveHistory() {
        return new ArrayList<>(activityHistories);
    }


    @Override
    public void addCommend(Comment comment) {

        comments.add(comment);

        activityHistories.add(new ActivityHistoryImpl(String.format("Comment from %s with content %s was added", comment.getAuthor(), comment.getContent()), LocalDateTime.now()));
    }

    @Override
    public void removeComment(Comment comment) {


        if (comments.isEmpty()) {
            throw new IllegalArgumentException("There is no comments");
        }
        comments.remove(comment);
        activityHistories.add(new ActivityHistoryImpl(String.format("Comment from %s with content %s was removed", comment.getAuthor(), comment.getContent()), LocalDateTime.now()));
    }


    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    private String printComments() {

        StringBuilder sb = new StringBuilder();

        if (comments.isEmpty()) {
            sb.append(NO_COMMENTS_HEADER);
            sb.append(String.format("%s\n", COMMENTS_HEADER));
        } else {
            for (Comment comment : comments) {
                sb.append("----------");
                sb.append(comment.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    private String printActivityHistory() {

        StringBuilder sb = new StringBuilder();


        if (activityHistories.isEmpty()) {
            sb.append(NO_ACTIVITY_HISTORY_HEADER);
            sb.append(String.format("%s\n", ACTIVITY_HISTORY_HEADER));
        } else {
            for (ActivityHistory activityHistory : activityHistories) {
                sb.append("----------");
                sb.append(activityHistory.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Title: %s", getTitle()));
        sb.append(System.lineSeparator());
        sb.append(String.format("Description: %s", getDescription()));
        sb.append(System.lineSeparator());
        sb.append(printComments());
        sb.append(System.lineSeparator());
        sb.append(printActivityHistory());

        return sb.toString().trim();
    }
}
