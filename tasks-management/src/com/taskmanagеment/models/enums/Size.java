package com.taskmanagеment.models.enums;

public enum Size {

    LARGE,
    MEDIUM,
    SMALL;

    private static final Size[] sizes = values();

    @Override
    public String toString() {
        switch (this) {
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            default:
                throw new UnsupportedOperationException("This Size does not exists");
        }
    }

    public Size previous() {

        return sizes[Math.abs((ordinal() - 1)) % values().length];
    }

    public Size next() {
        return sizes[Math.abs((ordinal() + 1)) % values().length];
    }
}
