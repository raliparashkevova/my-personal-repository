package com.taskmanagеment.models.enums;

public enum Severity {

    CRITICAL,
    MAJOR,
    MINOR;

    private static final Severity[] severities = values();

    @Override
    public String toString() {
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                throw new UnsupportedOperationException("This severity does not exists");
        }

    }

    public Severity previous() {

        return severities[Math.abs((ordinal() - 1)) % values().length];
    }

    public Severity next() {
        return severities[Math.abs((ordinal() + 1)) % values().length];
    }
}
