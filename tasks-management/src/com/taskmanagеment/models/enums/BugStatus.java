package com.taskmanagеment.models.enums;

public enum BugStatus {

    ACTIVE,
    FIXED;

    private static final BugStatus[] bugStatuses = values();

    @Override
    public String toString() {

        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            default:
                throw new UnsupportedOperationException("This BugStatus does not exists");

        }
    }

    public BugStatus previous() {

        return bugStatuses[Math.abs((ordinal() - 1)) % values().length];
    }

    public BugStatus next() {
        return bugStatuses[Math.abs((ordinal() + 1)) % values().length];
    }
}
