package com.taskmanagеment.models.enums;

public enum StoryStatus {

    NOTDONE,
    INPROGRESS,
    DONE;

    private static final StoryStatus[] storyStatuses = values();

    @Override
    public String toString() {
        switch (this) {
            case NOTDONE:
                return "Not Done";
            case INPROGRESS:
                return "In progress";
            case DONE:
                return "Done";
            default:
                throw new UnsupportedOperationException("This Story status does not exists");
        }
    }

    public StoryStatus previous() {

        return storyStatuses[Math.abs((ordinal() - 1)) % values().length];
    }

    public StoryStatus next() {
        return storyStatuses[Math.abs((ordinal() + 1)) % values().length];
    }
}
