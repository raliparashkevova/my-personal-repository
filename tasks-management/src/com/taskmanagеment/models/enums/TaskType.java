package com.taskmanagеment.models.enums;


import com.taskmanagеment.exceptions.InvalidUserInputException;

import static com.taskmanagеment.Constants.ModelConstants.*;

public enum TaskType {
    BUG,
    STORY,
    FEEDBACK;

    @Override
    public String toString() {
        switch (this) {
            case BUG:
                return "Bug";
            case STORY:
                return "Story";
            case FEEDBACK:
                return "Feedback";
            default:
                throw new InvalidUserInputException(String.format(NOT_SUPPORTED_ENUM, this));
        }
    }
}


