package com.taskmanagеment.models.enums;

public enum FeedBackStatus {


    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE;

    private static final FeedBackStatus[] feedBackStatuses = values();

    @Override
    public String toString() {
        switch (this) {
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            default:
                throw new UnsupportedOperationException("This Feedback Status does not exists");
        }
    }

    public FeedBackStatus previous() {

        return feedBackStatuses[Math.abs((ordinal() - 1)) % values().length];
    }

    public FeedBackStatus next() {
        return feedBackStatuses[Math.abs((ordinal() + 1)) % values().length];
    }
}
