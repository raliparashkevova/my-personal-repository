package com.taskmanagеment.models.enums;

public enum Priority {

    HIGH,
    MEDIUM,
    LOW;
    private static final Priority[] priorities = values();

    @Override
    public String toString() {

        switch (this) {
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
            default:
                throw new UnsupportedOperationException("This priority does not exists");
        }
    }

    public Priority previous() {

        return priorities[Math.abs((ordinal() - 1)) % values().length];
    }

    public Priority next() {
        return priorities[Math.abs((ordinal() + 1)) % values().length];
    }
}
