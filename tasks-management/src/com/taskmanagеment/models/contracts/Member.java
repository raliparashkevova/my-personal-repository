package com.taskmanagеment.models.contracts;

import java.util.List;

public interface Member extends BaseModel, Printable {

    List<Team> getTeams();

    void addTeam(Team team);

}
