package com.taskmanagеment.models.contracts;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ActivityHistory {

    String getDescription();

    LocalDateTime getTimeStamp();

    String getAsString();


}
