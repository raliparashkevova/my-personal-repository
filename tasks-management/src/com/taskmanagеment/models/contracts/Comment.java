package com.taskmanagеment.models.contracts;

public interface Comment {

    String getAuthor();

    String getContent();

    String getAsString();
}
