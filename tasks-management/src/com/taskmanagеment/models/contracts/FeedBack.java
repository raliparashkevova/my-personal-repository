package com.taskmanagеment.models.contracts;

import com.taskmanagеment.models.enums.FeedBackStatus;
import com.taskmanagеment.models.enums.Severity;

public interface FeedBack extends Task {

    int getRating();

    FeedBackStatus getFeedBackStatus();

    void changeFeedbackRating(int rating);

    void changeFeedbackStatus(FeedBackStatus feedBackStatus);
}
