package com.taskmanagеment.models.contracts;

import com.taskmanagеment.models.ActivityHistoryImpl;
import com.taskmanagеment.models.enums.TaskType;

import java.util.List;

public interface Task extends Identifiable, Printable, Commentable {

    String getTitle();

    String getDescription();

    List<ActivityHistory> getActiveHistory();

    void addCommend(Comment comment);

    void removeComment(Comment comment);

    void advanceStatus();

    void revertStatus();

    TaskType getType();


}
