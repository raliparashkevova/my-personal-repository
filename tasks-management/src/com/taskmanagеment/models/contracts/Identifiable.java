package com.taskmanagеment.models.contracts;

public interface Identifiable {

    int getId();
}
