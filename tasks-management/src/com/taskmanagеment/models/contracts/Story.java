package com.taskmanagеment.models.contracts;

import com.taskmanagеment.models.enums.Priority;
import com.taskmanagеment.models.enums.Size;
import com.taskmanagеment.models.enums.StoryStatus;

public interface Story extends Task, BugStory {

    Priority getPriority();

    Size getSize();

    StoryStatus getStoryStatus();
    //  String getAssignee();

    void advanceSize();

    void revertSize();

    void changeStoryPriority(Priority priority);

    void changeSize(Size size);

    void changeStoryStatus(StoryStatus storyStatus);
}
