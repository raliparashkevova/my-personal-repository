package com.taskmanagеment.models.contracts;

import java.util.List;

public interface Commentable {

    List<Comment> getComments();
}
