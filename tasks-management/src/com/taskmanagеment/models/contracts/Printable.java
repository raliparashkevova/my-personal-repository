package com.taskmanagеment.models.contracts;

public interface Printable {

    String getAsString();
}
