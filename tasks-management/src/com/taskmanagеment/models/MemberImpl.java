package com.taskmanagеment.models;

import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.Constants.OutputMessages;
import com.taskmanagеment.models.contracts.ActivityHistory;
import com.taskmanagеment.models.contracts.Member;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.models.contracts.Team;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static com.taskmanagеment.Constants.ModelConstants.*;
import static com.taskmanagеment.Constants.OutputMessages.MEMBER_NAME_ERR;

public class MemberImpl extends BaseModelImpl implements Member {


    private final List<Team> teams;

    public MemberImpl(int id, String name) {
        super(id, name);
        this.teams = new ArrayList<>();
    }


    @Override
    protected void validateName(String name) {
        ValidationHelpers.validateInRange(name.length(), MEMBER_NAME_MIN_LENGTH, MEMBER_NAME_MAX_LENGTH, MEMBER_NAME_ERR);
    }


    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public void addTeam(Team team) {
        teams.add(team);
    }

    private String printTeam() {

        StringBuilder sb = new StringBuilder();


        if (teams.isEmpty()) {
            sb.append(NO_TEAM_HEADER);
            sb.append(String.format("%s\n", TEAM_HEADER));
        } else {
            for (Team team : teams) {
                sb.append("----------");
                sb.append(team.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.getAsString());
        sb.append(printTeam());

        return sb.toString().trim();
    }
}
