package com.taskmanagеment.models;

import com.taskmanagеment.models.BaseTask;
import com.taskmanagеment.models.contracts.BugStory;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.models.enums.Priority;
import com.taskmanagеment.models.enums.Severity;
import com.taskmanagеment.models.enums.TaskType;

import java.time.LocalDateTime;

public abstract class BaseBugStory extends BaseTask implements BugStory {

    private static final Priority initialPriority = Priority.HIGH;
    private static final Priority finalPriority = Priority.LOW;
    private Priority priority;
    private final String assignee;


    protected BaseBugStory(int id, String title, String description, Priority priority, String assignee) {
        super(id, title, description);
        this.priority = priority;
        this.assignee = assignee;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    protected void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }


    public abstract TaskType getType();

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.getAsString());
        sb.append(String.format("Priority: %s\n", getPriority()));
        sb.append(String.format("Assignee: %s\n", getAssignee()));


        return sb.toString().trim();
    }
}
