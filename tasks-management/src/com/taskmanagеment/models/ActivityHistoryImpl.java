package com.taskmanagеment.models;

import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.ActivityHistory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static com.taskmanagеment.Constants.ModelConstants.*;

public class ActivityHistoryImpl implements ActivityHistory {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss", Locale.US);
    private String description;
    private LocalDateTime localDate;

    public ActivityHistoryImpl(String description, LocalDateTime localDate) {
        setDescription(description);
        setLocalDateTime(localDate);
    }


    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public LocalDateTime getTimeStamp() {
        return localDate;
    }


    public void setDescription(String description) {
        if (description.isEmpty()) {
            throw new InvalidUserInputException(DESCRIPTION_IS_EMPTY);
        }
        this.description = description;
    }

    private void setLocalDateTime(LocalDateTime localDate) {
        if (localDate.isBefore(LocalDateTime.now()))
            throw new IllegalArgumentException("The date can not be in the past");
        this.localDate = localDate;
    }

    @Override
    public String getAsString() {
        return String.format("[%s] %s\n", localDate.format(formatter), description);
    }


}
