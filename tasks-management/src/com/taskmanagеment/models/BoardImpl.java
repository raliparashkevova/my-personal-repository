package com.taskmanagеment.models;

import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.Constants.OutputMessages;
import com.taskmanagеment.models.contracts.ActivityHistory;
import com.taskmanagеment.models.contracts.BaseModel;
import com.taskmanagеment.models.contracts.Board;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static com.taskmanagеment.Constants.ModelConstants.*;
import static com.taskmanagеment.Constants.OutputMessages.*;

public class BoardImpl extends BaseModelImpl implements Board {


    public BoardImpl(int id, String name) {
        super(id, name);
    }

    @Override
    protected void validateName(String name) {

        ValidationHelpers.validateInRange(name.length(), BOARD_NAME_MIN_LENGTH, BOARD_NAME_MAX_LENGTH, BOARD_NAME_ERR);
    }


    @Override
    public String getAsString() {
        return super.getAsString();
    }


}
