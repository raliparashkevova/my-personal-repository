package com.taskmanagеment.models;

import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.Constants.OutputMessages;
import com.taskmanagеment.models.contracts.*;
import com.taskmanagеment.utils.ValidationHelpers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.taskmanagеment.Constants.ModelConstants.*;
import static com.taskmanagеment.Constants.OutputMessages.TEAM_NAME_ERR;

public class TeamImpl implements Team {


    private String name;
    private List<Member> members;
    private List<Board> boards;
    private List<ActivityHistory> activityHistories;
    private int id;

    public TeamImpl(String name) {
        this.name = name;
        this.members = new ArrayList<>();
        this.boards = new ArrayList<>();
        this.activityHistories = new ArrayList<>();
        activityHistories.add(new ActivityHistoryImpl("Team with " + name + "was created", LocalDateTime.now()));
    }


    private void setName(String name) {
        ValidationHelpers.validateInRange(name.length(), TEAM_NAME_MIN_LENGTH
                , TEAM_NAME_MAX_LENGTH, TEAM_NAME_ERR);
        this.name = name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addMember(Member member) {

        members.add(member);
    }

    @Override
    public void removeMember(Member member) {

        if (members.isEmpty()) {
            throw new IllegalArgumentException(MISSING_MEMBER);
        }

        members.remove(member);
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public void addBoard(Board board) {

        boards.add(board);
    }

    @Override
    public void removeBoard(Board board) {
        if (boards.isEmpty()) {
            throw new IllegalArgumentException(EMPTY_BOARD_LIST);
        }

        boards.remove(board);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public List<Task> getTasks() {
        List<Task> tasks = new ArrayList<>();

        for (Board board : boards) {
            tasks.addAll(board.getTasks());
        }
        return new ArrayList<>(tasks);
    }

    private String printMembers() {

        StringBuilder sb = new StringBuilder();


        if (members.isEmpty()) {
            sb.append(NO_MEMBER_HEADER);
            sb.append(String.format("%s\n", MEMBER_HEADER));
        } else {
            for (Member member : members) {
                sb.append("----------");
                sb.append(member.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    private String printBoards() {

        StringBuilder sb = new StringBuilder();


        if (boards.isEmpty()) {
            sb.append(NO_BOARD_HEADER);
            sb.append(String.format("%s\n", BOARD_HEADER));
        } else {
            for (Board board : boards) {
                sb.append("----------");
                sb.append(board.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    private String printActivityHistory() {

        StringBuilder sb = new StringBuilder();


        if (activityHistories.isEmpty()) {
            sb.append(NO_ACTIVITY_HISTORY_HEADER);
            sb.append(String.format("%s\n", ACTIVITY_HISTORY_HEADER));
        } else {
            for (ActivityHistory activityHistory : activityHistories) {
                sb.append("----------");
                sb.append(activityHistory.getAsString());
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    @Override
    public String getAsString() {

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Team name: %s\n", getName()));
        sb.append(printMembers());
        sb.append(printBoards());
        sb.append(printActivityHistory());


        return sb.toString().trim();
    }
}
