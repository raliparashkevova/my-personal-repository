package com.taskmanagеment.models;

import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.FeedBack;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.models.enums.*;

import java.time.LocalDateTime;

public class FeedBackImpl extends BaseTask implements FeedBack {
    private static final FeedBackStatus initialFeedbackStatus = FeedBackStatus.NEW;


    private int rating;
    private FeedBackStatus feedBackStatus;

    public FeedBackImpl(int id, String title, String description, int rating, FeedBackStatus feedBackStatus) {
        super(id, title, description);
        setRating(rating);
        this.feedBackStatus = feedBackStatus;
    }


    @Override
    public int getRating() {
        return rating;
    }

    private void setRating(int rating) {
        if (rating < 1) {
            throw new InvalidUserInputException(ModelConstants.NO_SUCH_RATING);
        }
        this.rating = rating;
    }

    private void setFeedBackStatus(FeedBackStatus feedBackStatus) {
        this.feedBackStatus = feedBackStatus;
    }

    @Override
    public FeedBackStatus getFeedBackStatus() {
        return feedBackStatus;
    }

    @Override
    public void changeFeedbackRating(int rating) {
        getActiveHistory().add(new ActivityHistoryImpl(String.format("Feedback rating was changed from %s to %s", getRating(), rating), LocalDateTime.now()));

        setRating(rating);
    }


    @Override
    public void advanceStatus() {
        FeedBackStatus current = getFeedBackStatus();
        FeedBackStatus next = getFeedBackStatus().next();
        if (current == initialFeedbackStatus) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Feedback status changed from %s to %s", current, next), LocalDateTime.now()));

            setFeedBackStatus(next);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't advance, already Feedback status is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void revertStatus() {
        FeedBackStatus current = getFeedBackStatus();
        FeedBackStatus previous = getFeedBackStatus().previous();

        if (current != initialFeedbackStatus) {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Feedback status changed from %s to %s", current, previous), LocalDateTime.now()));
            setFeedBackStatus(previous);
        } else {
            getActiveHistory().add(new ActivityHistoryImpl(String.format("Can't revert, already Feedback status is %s ", current), LocalDateTime.now()));
        }
    }

    @Override
    public void changeFeedbackStatus(FeedBackStatus feedBackStatus) {
        getActiveHistory().add(new ActivityHistoryImpl(String.format("Feedback status was changed from %s to %s", getFeedBackStatus(), feedBackStatus), LocalDateTime.now()));
        setFeedBackStatus(feedBackStatus);
    }

    @Override
    public TaskType getType() {
        return TaskType.FEEDBACK;
    }

    @Override
    public String getAsString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.getAsString());
        sb.append(String.format("Rating: %d\n", getRating()));
        sb.append(String.format("Feedback status: %s\n", getFeedBackStatus()));


        return sb.toString().trim();
    }
}
