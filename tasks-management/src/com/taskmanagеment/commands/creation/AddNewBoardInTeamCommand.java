package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.Constants.CoreConstants;
import com.taskmanagеment.commands.contracts.Command;

import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.Board;
import com.taskmanagеment.models.contracts.Team;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.List;

public class AddNewBoardInTeamCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final TaskManagementRepository taskManagementRepository;

    public AddNewBoardInTeamCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String boardName = parameters.get(0);
        String teamName = parameters.get(1);


        return addBoardToTeam(boardName, teamName);
    }

    private String addBoardToTeam(String boardName, String teamName) {

        if (!taskManagementRepository.teamExist(teamName)) {
            throw new InvalidUserInputException
                    (String.format(CommandConstants.TEAM_NOT_EXISTS, teamName));
        }
        if (!taskManagementRepository.boardExist(boardName)) {
            throw new InvalidUserInputException(String.format(CommandConstants.BOARD_NOT_EXISTS, boardName));
        }

        Team team = taskManagementRepository.findByTeamName(teamName);
        Board board = taskManagementRepository.findByBoardName(boardName);

        taskManagementRepository.addBoardToTeam(board, team);


        return String.format(CommandConstants.BOARD_ADDED_TO_TEAM_SUCCESSFULLY);


    }
}
