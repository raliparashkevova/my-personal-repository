package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.*;
import com.taskmanagеment.Constants.*;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.Bug;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.models.enums.TaskType;
import com.taskmanagеment.utils.ListingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static com.taskmanagеment.Constants.ModelConstants.BUG_STEPS_HEADER;
import static com.taskmanagеment.Constants.ModelConstants.NO_BUG_STEPS_HEADER;

public class ShowBugStepsCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private final TaskManagementRepository taskManagementRepository;

    public ShowBugStepsCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        List<Task> tasks = taskManagementRepository.getTasks();

        List<Bug> bugTasks = getBugSteps(tasks);


        return showBugSteps(bugTasks);
    }

    private List<Bug> getBugSteps(List<Task> tasks) {
        List<Bug> bugs = new ArrayList<>();

        for (Task task : tasks) {
            if (task.getType().equals(TaskType.BUG)) {

                bugs.add((Bug) task);
            }
        }
        return bugs;
    }

    private String showBugSteps(List<Bug> bugs) {

        StringBuilder sb = new StringBuilder();
        int count = 1;
        for (Bug bug : bugs) {
            sb.append(bug.getAsString());
            sb.append(System.lineSeparator());
            if (bug.getStepsToReproduce().isEmpty()) {
                sb.append(NO_BUG_STEPS_HEADER);
                sb.append(System.lineSeparator());
            } else {
                for (String s : bug.getStepsToReproduce()) {
                    sb.append(BUG_STEPS_HEADER);
                    sb.append(String.format("%d", count++));
                    sb.append(s);
                }
            }


        }
        return sb.toString();
    }


}
