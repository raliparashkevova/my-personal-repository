package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.ElementNotFoundException;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.*;
import com.taskmanagеment.models.enums.*;
import com.taskmanagеment.utils.ParsingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.List;

import static com.taskmanagеment.Constants.CommandConstants.*;

public class ChangeLabelCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private final TaskManagementRepository taskManagementRepository;

    public ChangeLabelCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        TaskType taskType = ParsingHelpers.tryParseEnum(parameters.get(0).toUpperCase(), TaskType.class);
        String filterType = parameters.get(1);
        String filter = parameters.get(2).toUpperCase();
        int taskId = ParsingHelpers.tryParseInt(parameters.get(3), CommandConstants.INVALID_TASK_INDEX);


        return changeLabelByType(taskType, filterType, filter, taskId);
    }

    private String changeLabelByType(TaskType taskType, String filterType, String filter, int taskId) {
        Task task = taskManagementRepository.findElementById(taskManagementRepository.getTasks(), taskId);
        FeedBack feedBack = getFeedBackTask(task);
        Story story = getStoryTask(task);
        Bug bug = getBugTask(task);


        switch (taskType) {

            case FEEDBACK:
                switch (filterType) {
                    case "RATING":
                        int rating = ParsingHelpers.tryParseInt(filter, CommandConstants.INVALID_INPUT_MESSAGE);
                        changeLabelRating(rating, task);
                        break;
                    case "STATUS":
                        FeedBackStatus feedBackStatus = ParsingHelpers.tryParseEnum(filter.toUpperCase(), FeedBackStatus.class);
                        changeLabelFeedbackStatus(feedBackStatus, task);
                        break;
                }

            case BUG:
                switch (filterType) {
                    case "PRIORITY":
                        Priority priority = ParsingHelpers.tryParseEnum(filter.toUpperCase(), Priority.class);
                        changeLabelPriority(priority, task);
                        break;
                    case "SEVERITY":
                        Severity severity = ParsingHelpers.tryParseEnum(filter.toUpperCase(), Severity.class);
                        changeLabelSeverity(severity, task);
                        break;
                    case "STATUS":
                        BugStatus bugStatus = ParsingHelpers.tryParseEnum(filter.toUpperCase(), BugStatus.class);
                        changeLabelBugStatus(bugStatus, task);
                        break;

                }

            case STORY:
                switch (filterType) {
                    case "PRIORITY":
                        Priority priority = ParsingHelpers.tryParseEnum(filter.toUpperCase(), Priority.class);
                        changeLabelStoryPriority(priority, task);
                        break;
                    case "SIZE":
                        Size size = ParsingHelpers.tryParseEnum(filter.toUpperCase(), Size.class);
                        changeLabelSize(size, task);
                        break;
                    case "STATUS":
                        StoryStatus storyStatus = ParsingHelpers.tryParseEnum(filter.toUpperCase(), StoryStatus.class);
                        changeLabelStoryStatus(storyStatus, task);
                        break;

                }
                break;

        }

        return String.format(CommandConstants.LABEL_CHANGED_SUCCESSFULLY);
    }

    private void changeLabelFeedbackStatus(FeedBackStatus feedBackStatus, Task task) {

        FeedBack feedBack = getFeedBackTask(task);
        taskManagementRepository.changeFeedBackStatus(feedBackStatus, feedBack);
    }

    private void changeLabelStoryStatus(StoryStatus storyStatus, Task task) {

        Story story = getStoryTask(task);
        taskManagementRepository.changeLabelStoryStatus(storyStatus, story);
    }


    private void changeLabelSize(Size size, Task task) {

        Story story = getStoryTask(task);
        taskManagementRepository.changeLabelSize(size, story);
    }

    private void changeLabelStoryPriority(Priority priority, Task task) {

        Story story = getStoryTask(task);
        taskManagementRepository.changeLabelStoryPriority(priority, story);
    }

    private void changeLabelRating(int rating, Task task) {
        FeedBack feedBack = getFeedBackTask(task);
        taskManagementRepository.changeLabelRating(rating, feedBack);
    }

    private void changeLabelBugStatus(BugStatus bugStatus, Task task) {
        Bug bug = getBugTask(task);
        taskManagementRepository.changeLabelBugStatus(bug, bugStatus);
    }

    private void changeLabelSeverity(Severity severity, Task task) {
        Bug bug = getBugTask(task);
        taskManagementRepository.changeLabelSeverityBug(bug, severity);
    }

    private Story getStoryTask(Task task) {
        if (!task.getType().equals(TaskType.STORY)) {
            throw new InvalidUserInputException(CommandConstants.MISSING_TASK_TYPE);
        }
        return (Story) task;
    }

    private FeedBack getFeedBackTask(Task task) {
        if (!task.getType().equals(TaskType.FEEDBACK)) {
            throw new InvalidUserInputException(CommandConstants.MISSING_TASK_TYPE);
        }
        return (FeedBack) task;
    }

    private void changeLabelPriority(Priority priority, Task task) {

        Bug bug = getBugTask(task);
        taskManagementRepository.changeLabelPriorityBug(bug, priority);
    }

    private Bug getBugTask(Task task) {

        if (!task.getType().equals(TaskType.BUG)) {
            throw new InvalidUserInputException(CommandConstants.MISSING_TASK_TYPE);
        }
        return (Bug) task;
    }
}
