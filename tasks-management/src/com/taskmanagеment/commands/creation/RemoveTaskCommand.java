package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.utils.ParsingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.List;

import static com.taskmanagеment.Constants.CommandConstants.INVALID_TASK_INDEX;

public class RemoveTaskCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final TaskManagementRepository taskManagementRepository;

    public RemoveTaskCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int taskIndex = ParsingHelpers.tryParseInt(parameters.get(0), INVALID_TASK_INDEX);
        return removeTask(taskIndex);
    }

    private String removeTask(int taskIndex) {

        Task task = taskManagementRepository.findElementById(taskManagementRepository.getTasks(), taskIndex);

        taskManagementRepository.removeTask(task);


        return String.format(CommandConstants.TASK_REMOVED_SUCCESSFULLY, task.getTitle());
    }
}
