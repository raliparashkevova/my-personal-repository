package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.Bug;
import com.taskmanagеment.models.contracts.BugStory;
import com.taskmanagеment.models.contracts.Story;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.models.enums.BugStatus;
import com.taskmanagеment.models.enums.StoryStatus;
import com.taskmanagеment.models.enums.TaskType;
import com.taskmanagеment.utils.ListingHelpers;
import com.taskmanagеment.utils.ParsingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;
import jdk.swing.interop.SwingInterOpUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.taskmanagеment.Constants.CommandConstants.INVALID_TASK_INDEX;
import static com.taskmanagеment.Constants.CommandConstants.INVALID_TASK_TYPE;

public class ListTaskByAssigneeCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private final TaskManagementRepository taskManagementRepository;

    public ListTaskByAssigneeCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);


        TaskType taskType = ParsingHelpers.tryParseEnum(parameters.get(0).toUpperCase(), TaskType.class);


        String filterType = parameters.get(1).toUpperCase();

        String filterStatus = parameters.get(2).toUpperCase();
        String nameAssignee = parameters.get(3);


        return listTaskByAssignee(taskType, filterType, filterStatus, nameAssignee);
    }

    private String listTaskByAssignee(TaskType taskType, String filterType, String status, String nameAssignee) {


        if (!taskManagementRepository.memberExist(nameAssignee)) {
            throw new InvalidUserInputException(String.format(CommandConstants.MEMBER_NOT_EXISTS, nameAssignee));
        }
        List<Bug> bugs = taskManagementRepository.getBugTasks().stream().filter(bug -> bug.getAssignee().equals(nameAssignee)).collect(Collectors.toList());

        List<Story> stories = taskManagementRepository.getStoryTask().stream().filter(story -> story.getAssignee().equalsIgnoreCase(nameAssignee)).collect(Collectors.toList());


        switch (filterType) {
            case "STATUS":
                switch (taskType) {
                    case BUG:

                        List<Bug> bugListByStatus = getFilteredByStatus(status, bugs);

                        ListingHelpers.elementsToString(bugListByStatus);
                        break;

                    case STORY:
                        List<Story> storyListByStatus = getFiltered(status, stories);

                        ListingHelpers.elementsToString(storyListByStatus);
                        break;
                }
                break;
            case "ASSIGNEE":
                ListingHelpers.elementsToString(bugs);
                ListingHelpers.elementsToString(stories);
                break;
            case "SORT":

                List<Bug> sortedBugs = bugs.stream()
                        .sorted(Comparator.comparing(Bug::getTitle)).collect(Collectors.toList());
                ListingHelpers.elementsToString(sortedBugs);

                List<Story> sortedStory = stories.stream()
                        .sorted(Comparator.comparing(Story::getTitle)).collect(Collectors.toList());

                ListingHelpers.elementsToString(sortedStory);

                break;
        }

        return null;

    }

    private List<Story> getFiltered(String status, List<Story> stories) {

        StoryStatus storyStatus = ParsingHelpers.tryParseEnum(status.toUpperCase(), StoryStatus.class);

        List<Story> storiesFilteredByStatus = new ArrayList<>();
        for (Story story : stories) {
            if (story.getStoryStatus().equals(storyStatus)) {
                storiesFilteredByStatus.add(story);
            }
        }
        return storiesFilteredByStatus;
    }

    private List<Bug> getFilteredByStatus(String status, List<Bug> bugs) {

        BugStatus bugStatus = ParsingHelpers.tryParseEnum(status.toUpperCase(), BugStatus.class);

        List<Bug> filteredByStatus = new ArrayList<>();
        for (Bug bug : bugs) {
            if (bug.getBugStatus().equals(bugStatus)) {
                filteredByStatus.add(bug);
            }
        }
        return filteredByStatus;
    }


}
