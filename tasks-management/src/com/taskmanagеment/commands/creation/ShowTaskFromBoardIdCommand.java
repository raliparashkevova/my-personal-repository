package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.models.contracts.Board;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.models.contracts.Team;
import com.taskmanagеment.utils.ListingHelpers;
import com.taskmanagеment.utils.ParsingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class ShowTaskFromBoardIdCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final TaskManagementRepository taskManagementRepository;

    public ShowTaskFromBoardIdCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = ParsingHelpers.tryParseInt(parameters.get(0), CommandConstants.INVALID_BOARD_INDEX);

        return showTaskFromBoardId(id);
    }

    private String showTaskFromBoardId(int id) {
        StringBuilder sb = new StringBuilder();
        List<Task> tasks = new ArrayList<>();


        int count = 1;
        sb.append(ModelConstants.TASK_HEADER);
        sb.append(System.lineSeparator());

        List<Board> boards = taskManagementRepository.getBoards();
        for (Board board : boards) {
            if (board.getId() == id) {
                tasks.addAll(board.getTasks());
            }
        }

        for (Task task : tasks) {
            sb.append(String.format("%d: ", count++));
            sb.append(System.lineSeparator());
            sb.append(task.getAsString());
        }

        return sb.toString();
    }
}
