package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.ActivityHistory;
import com.taskmanagеment.models.contracts.Member;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.List;

import static com.taskmanagеment.Constants.CommandConstants.*;

public class CreateMemberCommand implements Command {
    public static final String MEMBER_CREATED_MESSAGE = "Member with name %s was created.";
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final TaskManagementRepository taskManagementRepository;

    public CreateMemberCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);


        String memberName = parameters.get(0);

        return createMember(memberName);
    }

    private String createMember(String memberName) {

        if (taskManagementRepository.memberExist(memberName)) {
            throw new InvalidUserInputException(String.format(MEMBER_ALREADY_EXISTS, memberName));
        }

        taskManagementRepository.createMember(memberName);

        return String.format(MEMBER_CREATED, memberName);
    }
}
