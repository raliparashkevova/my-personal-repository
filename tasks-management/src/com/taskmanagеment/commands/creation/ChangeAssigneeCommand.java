package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.utils.ParsingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.List;

public class ChangeAssigneeCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final TaskManagementRepository taskManagementRepository;

    public ChangeAssigneeCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int taskId = ParsingHelpers.tryParseInt(parameters.get(0), CommandConstants.INVALID_TASK_INDEX);

        String assignee = parameters.get(1);


        return validateMemberFromTeam(taskId, assignee);
    }

    private String validateMemberFromTeam(int taskId, String assignee) {

        taskManagementRepository.validateMemberIsFromTeam(taskId, assignee);
        Task task = taskManagementRepository.findElementById(taskManagementRepository.getTasks(), taskId);


        return changeAssignee(task, assignee);

    }

    private String changeAssignee(Task task, String assignee) {

        return null;
    }
}
