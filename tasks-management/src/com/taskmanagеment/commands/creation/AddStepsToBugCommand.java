package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.contracts.Bug;
import com.taskmanagеment.models.contracts.Task;
import com.taskmanagеment.utils.ParsingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.Arrays;
import java.util.List;

public class AddStepsToBugCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final TaskManagementRepository taskManagementRepository;

    public AddStepsToBugCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String memberName = parameters.get(0);
        String step = parameters.get(1);
        int taskId = ParsingHelpers.tryParseInt(parameters.get(0), CommandConstants.INVALID_TASK_INDEX);


        return addStepToBug(memberName, step, taskId);
    }

    private String addStepToBug(String memberName, String step, int taskId) {

        Task task = taskManagementRepository
                .findElementById(taskManagementRepository.getTasks(), taskId);

        Bug bug = (Bug) task;

        if (!bug.getAssignee().equalsIgnoreCase(memberName)) {
            throw new InvalidUserInputException(String.format(CommandConstants.MEMBER_NOT_EXISTS, memberName));
        }
        bug.addStepToReproduce(step);

        return CommandConstants.STEP_ADD_TO_BUG;
    }
}
