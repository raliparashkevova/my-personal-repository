package com.taskmanagеment.commands.creation;

import com.taskmanagеment.Constants.CoreConstants;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.utils.ListingHelpers;
import com.taskmanagеment.utils.ValidationHelpers;

import java.util.List;

import static com.taskmanagеment.Constants.CoreConstants.INVALID_COMMAND;

public class ShowAllTeamBoardsMembersCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final TaskManagementRepository taskManagementRepository;

    public ShowAllTeamBoardsMembersCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String filterType = parameters.get(0).toUpperCase();


        return shallAllByFilterType(filterType);
    }

    private String shallAllByFilterType(String filterType) {

        switch (filterType) {
            case "TEAM":
                return ListingHelpers.elementsToString(taskManagementRepository.getTeams());

            case "BOARD":
                return ListingHelpers.elementsToString(taskManagementRepository.getBoards());

            case "MEMBER":
                return ListingHelpers.elementsToString(taskManagementRepository.getMembers());

            default:
                throw new InvalidUserInputException(String.format(INVALID_COMMAND, filterType));

        }
    }
}
