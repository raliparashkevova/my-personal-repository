package com.taskmanagеment.utils;

import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.enums.TaskType;

import java.util.List;

import static com.taskmanagеment.Constants.CommandConstants.INVALID_TASK_TYPE;
import static com.taskmanagеment.Constants.ModelConstants.NO_SUCH_ENUM;
import static com.taskmanagеment.Constants.OutputMessages.*;

public class ValidationHelpers {

    public static void validateInRange(int value, int minValue, int maxValue, String message) {

        if (value < minValue || value > maxValue) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateArgumentsCount(List<String> parameters, int expectedNumberOfParameters) {

        if (parameters.size() < expectedNumberOfParameters) {
            throw new IllegalArgumentException(
                    String.format(INVALID_NUMBER_OF_ARGUMENTS, expectedNumberOfParameters, parameters.size())
            );
        }
    }

    public static void validateTaskType(TaskType taskType) {
        if (taskType == TaskType.FEEDBACK) {
            throw new InvalidUserInputException(String.format(INVALID_TASK_TYPE, taskType));
        }
    }


}
