package com.taskmanagеment.Constants;

public class CommandConstants {

    public static final String CANNOT_CREATE_THIS_TYPE_OF_TASK = "Cannot create this type of task.";
    public static final String TASK_ADDED_SUCCESSFULLY = "%s added to Board successfully!";
    public static final String TEAM_CREATED = "Team with name %s was created!";
    public static final String MEMBER_CREATED = "User with name %s was created!";
    public static final String TEAM_ALREADY_EXISTS = "Team with name %s already exists!";
    public static final String MEMBER_ALREADY_EXISTS = "User with username %s already exists!";
    public static final String TEAM_NOT_EXISTS = "Team with name %s not exists!";
    public static final String BOARD_NOT_EXISTS = "Board with title %s not exists!";
    public static final String MEMBER_ADDED_TO_TEAM_SUCCESSFULLY = "%s add to team %s!";
    public static final String BOARD_ADDED_TO_TEAM_SUCCESSFULLY = "Board with tittle %s add to team %s!";
    public final static String BOARD_REMOVED_SUCCESSFULLY = "Board with title %s removed successfully!";
    public final static String COMMENT_REMOVED_SUCCESSFULLY = "%s removed comment successfully!";
    public final static String TASK_REMOVED_SUCCESSFULLY = "%s removed task successfully!";
    public final static String INVALID_FEEDBACK_RATING = "Invalid feedback rating. Expected a number.";
    public final static String INVALID_COMMENT_INDEX = "Invalid comment index. Expected a number.";
    public final static String INVALID_TASK_INDEX = "Invalid task index. Expected a number.";
    public final static String INVALID_BOARD_INDEX = "Invalid board index. Expected a number.";
    public static final String BOARD_ALREADY_EXISTS = "Board with name %s already exists!";
    public static final String BOARD_CREATED = "Board with name %s was created!";
    public static final String STEP_ADD_TO_BUG = "Step was add to bug with title %s.";
    public static final String INVALID_INDEX = "There is no record on this index.";
    public static final String USER_NOT_MEMBER = "User with username %s is not member from team %s.";
    public static final String ASSIGNEE_CHANGED = "Assignee changed to %s.";
    public static final String INVALID_INPUT_MESSAGE = "Invalid input. Expected a number.";
    public static final String COMMENT_ADDED_SUCCESSFULLY = "%s added comment successfully!";
    public static final String INVALID_COMMENT = "Comment %s not Exist.";
    public static final String INVALID_TASK_TYPE = "Invalid command %s, not supported for changing assignee !";
    public static final String NOT_SUPPORTED_OPERATION = "%s is not supported.";

    public static final String MEMBER_NOT_EXISTS = "Member with name %s not exists!";
    public static final String MISSING_TASK_TYPE = "This type bug is missing!";


    public static final String LABEL_CHANGED_SUCCESSFULLY = "The label is changed successfully!";
}
