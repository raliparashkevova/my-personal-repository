package com.taskmanagеment.Constants;

public class CoreConstants {
    public static final String INVALID_COMMAND = "Invalid command name: %s!";
    public static final String ELEMENT_NOT_FOUND = "No record with tittle %s";
    public static final String TASK_NOT_ATTACHED_TO_BOARD = "Task not attached to board.";
    public static final String BOARD_NOT_ATTACHED_TO_TEAM = "Board not attached to team.";
}
