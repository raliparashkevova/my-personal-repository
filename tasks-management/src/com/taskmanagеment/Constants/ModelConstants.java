package com.taskmanagеment.Constants;

public class ModelConstants {

    public static final int TEAM_NAME_MIN_LENGTH = 5;
    public static final int TEAM_NAME_MAX_LENGTH = 15;
    public static final int MEMBER_NAME_MIN_LENGTH = 5;
    public static final int MEMBER_NAME_MAX_LENGTH = 15;
    public static final int BOARD_NAME_MIN_LENGTH = 5;
    public static final int BOARD_NAME_MAX_LENGTH = 10;
    public static final int TITLE_MIN_LENGTH = 10;
    public static final int TITLE_MAX_LENGTH = 50;
    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 500;

    public static final String NO_ID = "The Id should be unique!";
    public static final String NO_SUCH_RATING = "Rating can not be negative number";
    public static final String NO_SUCH_ENUM = "There is no %s in %s.";
    public static final String MISSING_MEMBER = "This member does not exists";
    public static final String EMPTY_BOARD_LIST = "There is no board in the list";
    public static final String UNIQUE_NAME = "The name always exists";
    public static final String EMPTY_MEMBERS_LIST = "There is no members in the list";
    public static final String DESCRIPTION_IS_EMPTY = "Description cannot be empty";
    public static final String NOT_SUPPORTED_ENUM = "%s Not supported enum.";
    public static final String BOARD_EXIST = "Board with name %s already exist";

    public final static String COMMENTS_HEADER = "--COMMENTS--";
    public final static String NO_COMMENTS_HEADER = "--NO COMMENTS--";
    public final static String ACTIVITY_HISTORY_HEADER = "--ACTIVITY HISTORY--";
    public final static String NO_ACTIVITY_HISTORY_HEADER = "--NO ACTIVITY HISTORY--";
    public final static String TASK_HEADER = "--TASK--";
    public final static String NO_TASK = "--NO TASK--";
    public final static String TEAM_HEADER = "--TEAM--";
    public final static String NO_TEAM_HEADER = "--NO TEAM--";
    public final static String MEMBER_HEADER = "--MEMBER--";
    public final static String NO_MEMBER_HEADER = "--NO MEMBER--";
    public final static String BOARD_HEADER = "--BOARD--";
    public final static String NO_BOARD_HEADER = "--NO BOARD--";
    public final static String BUG_STEPS_HEADER = "--BUG STEPS--";
    public final static String NO_BUG_STEPS_HEADER = "--NO BUG STEPS--";

}
