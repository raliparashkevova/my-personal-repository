package com.taskmanagеment.core;

import com.taskmanagеment.commands.creation.*;
import com.taskmanagеment.commands.contracts.Command;
import com.taskmanagеment.commands.enums.CommandType;
import com.taskmanagеment.core.contracts.CommandFactory;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.utils.ParsingHelpers;

public class CommandFactoryImpl implements CommandFactory {
    @Override
    public Command createCommandFromCommandName(String commandTypeAsString, TaskManagementRepository taskManagementRepository) {

        CommandType commandType = ParsingHelpers.tryParseEnum(commandTypeAsString, CommandType.class);

        switch (commandType) {
            case CREATENEWMEMBER:
                return new CreateMemberCommand(taskManagementRepository);
            case REMOVECOMMENTFROMTASK:
                return new RemoveCommentFromTaskCommand(taskManagementRepository);
            case UNASSIGNTASKTOMEMBER:
                return new UnassignTaskFromMemberCommand(taskManagementRepository);
            case ADDCOMMENTTOTASK:
                return new AddCommentTaskCommand(taskManagementRepository);
            case ADDMEMBERTOTEAM:
                return new AddMemberToTeamCommand(taskManagementRepository);
            case CREATENEWTEAM:
                return new CreateNewTeamCommand(taskManagementRepository);
            case REMOVETASK:
                return new RemoveTaskCommand(taskManagementRepository);
            case CHANGELABEL:
                return new ChangeLabelCommand(taskManagementRepository);
            case REMOVEBOARD:
                return new RemoveBoardCommand(taskManagementRepository);
            case LISTALLTASKS:
                return new ListAllTasksCommand(taskManagementRepository);
            case LISTTYPETASK:
                return new ListTypeTaskCommand(taskManagementRepository);
            case SHOWACTIVITY:
                return new ShowActivityCommand(taskManagementRepository);
            case SHOWBUGSTEPS:
                return new ShowBugStepsCommand(taskManagementRepository);
            case ADDSTEPSTOBUG:
                return new AddStepsToBugCommand(taskManagementRepository);
            case CREATENEWTASK:
                return new CreateNewTaskCommand(taskManagementRepository);
            case CHANGEASSIGNEE:
                return new ChangeAssigneeCommand(taskManagementRepository);
            case CREATENEWBOARD:
                return new CreateNewBoardCommand(taskManagementRepository);
            case SHOWTASKALLINFO:
                return new ShowTaskAllInfoCommand(taskManagementRepository);
            case ADDNEWBORDINTEAM:
                return new AddNewBoardInTeamCommand(taskManagementRepository);
            case LISTTASKBYASSIGNEE:
                return new ListTaskByAssigneeCommand(taskManagementRepository);
            case SHOWALLTEAMSMEMBERS:
                return new ShowAllTeamsMembersCommand(taskManagementRepository);
            case SHOWTASKSFROMBOARDID:
                return new ShowTaskFromBoardIdCommand(taskManagementRepository);
            case SHOWALLTEAMBOARDSMEMBERS:
                return new ShowAllTeamBoardsMembersCommand(taskManagementRepository);

            default:
                throw new IllegalArgumentException("This commandType does not exists");


        }

    }
}
