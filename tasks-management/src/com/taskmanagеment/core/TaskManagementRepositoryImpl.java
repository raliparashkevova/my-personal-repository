package com.taskmanagеment.core;

import com.taskmanagеment.Constants.CommandConstants;
import com.taskmanagеment.Constants.CoreConstants;
import com.taskmanagеment.Constants.ModelConstants;
import com.taskmanagеment.core.contracts.TaskManagementRepository;
import com.taskmanagеment.exceptions.ElementNotFoundException;
import com.taskmanagеment.exceptions.InvalidUserInputException;
import com.taskmanagеment.models.*;
import com.taskmanagеment.models.contracts.*;
import com.taskmanagеment.models.enums.*;

import java.util.ArrayList;
import java.util.List;

import static com.taskmanagеment.Constants.CommandConstants.MEMBER_NOT_EXISTS;
import static com.taskmanagеment.Constants.CoreConstants.*;

public class TaskManagementRepositoryImpl implements TaskManagementRepository {

    private int id;
    private final List<Member> members;
    private final List<Task> tasks;
    private final List<ActivityHistory> activityHistories;
    private final List<Team> teams;
    private final List<Board> boards;


    public TaskManagementRepositoryImpl() {
        this.id = 0;
        this.members = new ArrayList<>();
        this.tasks = new ArrayList<>();
        this.activityHistories = new ArrayList<>();
        this.teams = new ArrayList<>();
        this.boards = new ArrayList<>();
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public List<ActivityHistory> getHistory() {
        return new ArrayList<>(activityHistories);
    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public Member findByMemberName(String memberName) {


        for (Member member : members) {
            if (member.getName().equalsIgnoreCase(memberName)) {
                return member;
            }
        }
        throw new IllegalArgumentException(String.format(MEMBER_NOT_EXISTS, memberName));
    }

    @Override
    public Member createMember(String name) {
        Member member = new MemberImpl(++id, name);
        this.members.add(member);

        return member;
    }

    @Override
    public Team findByTeamName(String teamName) {

        for (Team team : teams) {
            if (team.getName().equals(teamName)) {
                return team;
            }
        }
        throw new InvalidUserInputException(String.format(CommandConstants.TEAM_NOT_EXISTS, teamName));
    }

    @Override
    public Team createTeam(String name) {
        Team team = new TeamImpl(name);

        teams.add(team);
        return team;
    }


    @Override
    public Board findByBoardName(String boardName) {

        for (Board board : boards) {
            if (board.getName().equals(boardName)) {
                return board;
            }
        }
        throw new InvalidUserInputException(String.format(CommandConstants.BOARD_NOT_EXISTS, boardName));
    }

    @Override
    public Board createBoard(String name) {
        Board board = new BoardImpl(++id, name);

        if (boardExist(name)) {
            throw new InvalidUserInputException(String.format(CommandConstants.BOARD_ALREADY_EXISTS, name));
        }

        boards.add(board);
        return board;
    }


    @Override
    public Bug createBug(int id, String title, String description, Priority priority, Severity severity, BugStatus bugStatus, String assignee) {

        Bug bug = new BugImpl(++id, title, description, priority, severity, bugStatus, assignee);


        this.tasks.add(bug);
        return bug;
    }


    @Override
    public Story createStory(int id, String title, String description
            , Priority priority, Size size, StoryStatus storyStatus, String assignee) {

        Story story = new StoryImpl(++id, title, description, priority, size, storyStatus, assignee);

        this.tasks.add(story);

        return story;
    }

    @Override
    public FeedBack createFeedBack(int id, String title, String description, int rating, FeedBackStatus feedBackStatus) {

        FeedBack feedBack = new FeedBackImpl(++id, title, description, rating, feedBackStatus);

        this.tasks.add(feedBack);


        return feedBack;
    }


    @Override
    public List<ActivityHistory> getHistoryByName(String name) {
        for (Member member : members) {
            member.getName().equalsIgnoreCase(name);
            List<ActivityHistory> resultTask = member.getActiveHistory();
        }
        throw new IllegalArgumentException("Member with this name does not exists");
    }

    @Override
    public boolean memberExist(String memberName) {

        for (Member member : members) {
            if (member.getName().equals(memberName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <T extends Identifiable> T findElementById(List<T> elements, int id) {

        for (T element : elements) {
            if (element.getId() == id) {
                return element;
            }
        }
        throw new ElementNotFoundException(String.format("This %d does not exists", id));
    }

    @Override
    public void validateMemberIsFromTeam(int taskIndex, String author) {

        Task task = findElementById(getTasks(), taskIndex);

        Board board = getBoards().stream()
                .filter(board1 -> board1.getTasks().contains(task))
                .findAny()
                .orElseThrow(() -> new InvalidUserInputException(TASK_NOT_ATTACHED_TO_BOARD));

        Team team = getTeams().stream()
                .filter(team1 -> team1.getBoards().contains(board))
                .findAny().orElseThrow(() -> new InvalidUserInputException(BOARD_NOT_ATTACHED_TO_TEAM));
        Member member = findByMemberName(author);

        if (!member.getTeams().contains(team)) {
            throw new InvalidUserInputException(String.format(CommandConstants.USER_NOT_MEMBER, member, team.getName()));
        }

    }

    @Override
    public Comment createComment(String content, String author) {
        return new CommentImpl(content, author);
    }

    @Override
    public boolean teamExist(String teamName) {

        return teams.stream().anyMatch(team -> team.getName().equals(teamName));
    }

    @Override
    public boolean boardExist(String boardName) {
        return boards.stream().anyMatch(board -> board.getName().equals(boardName));
    }

    @Override
    public void addMemberToTeam(Member member, Team team) {

        boolean isAdded = false;
        for (Team team1 : teams) {
            if (team1.getName().equals(team.getName())) {
                team1.addMember(member);
                isAdded = true;
            }
        }
        if (isAdded) {
            throw new ElementNotFoundException(String.format(ELEMENT_NOT_FOUND, team.getName())); // тук да проверя
        }
    }

    @Override
    public void addBoardToTeam(Board board, Team team) {

        boolean isAdded = false;

        for (Team team1 : teams) {
            if (team1.getName().equals(team.getName())) {
                team1.addBoard(board);
                isAdded = true;
            }
        }
        if (isAdded) {
            throw new ElementNotFoundException(String.format(ELEMENT_NOT_FOUND, team.getName()));
        }
    }

    @Override
    public void changeLabelPriorityBug(Bug bug, Priority priority) {

        if (bug.getPriority() == priority) {
            throw new InvalidUserInputException("The priority is the same as previous!");
        }
        bug.changeBugPriority(priority);


    }

    @Override
    public void changeLabelSeverityBug(Bug bug, Severity severity) {

        if (bug.getSeverity() == severity) {
            throw new InvalidUserInputException("The severity is the same as previous!");
        }
        bug.changeBugSeverity(severity);
    }

    @Override
    public void changeLabelBugStatus(Bug bug, BugStatus bugStatus) {
        if (bug.getBugStatus() == bugStatus) {
            throw new InvalidUserInputException("The bug status is the same as previous!");
        }

        bug.changeBugStatus(bugStatus);
    }

    @Override
    public void changeLabelRating(int rating, FeedBack feedBack) {
        if (feedBack.getRating() == rating) {
            throw new InvalidUserInputException("The rating is the same as previous!");
        }
        feedBack.changeFeedbackRating(rating);
    }

    @Override
    public void changeLabelStoryPriority(Priority priority, Story story) {
        if (story.getPriority() == priority) {
            throw new InvalidUserInputException("The priority is the same as previous!");
        }

        story.changeStoryPriority(priority);
    }

    @Override
    public void changeLabelSize(Size size, Story story) {
        if (story.getSize() == size) {
            throw new InvalidUserInputException("The size is the same as previous!");
        }

        story.changeSize(size);

    }

    @Override
    public void changeLabelStoryStatus(StoryStatus storyStatus, Story story) {
        if (story.getStoryStatus() == storyStatus) {
            throw new InvalidUserInputException("The Story status is the same as previous!");
        }
        story.changeStoryStatus(storyStatus);
    }

    @Override
    public void changeFeedBackStatus(FeedBackStatus feedBackStatus, FeedBack feedBack) {
        if (feedBack.getFeedBackStatus() == feedBackStatus) {
            throw new InvalidUserInputException("The Story status is the same as previous!");
        }

        feedBack.changeFeedbackStatus(feedBackStatus);
    }

    @Override
    public void removeBoard(String boardName) {

        boards.removeIf(board -> board.getName().equals(boardName));
    }

    @Override
    public void removeTask(Task task) {

        tasks.removeIf(task1 -> task.getTitle().equalsIgnoreCase(task.getTitle()));

    }

    @Override
    public List<Bug> getBugTasks() {
        List<Bug> bugs = new ArrayList<>();

        for (Task task : getTasks()) {
            if (task.getType().equals(TaskType.BUG)) {
                bugs.add((Bug) task);
            }

        }
        return bugs;
    }

    @Override
    public List<Story> getStoryTask() {
        List<Story> stories = new ArrayList<>();

        for (Task task : getTasks()) {
            if (task.getType().equals(TaskType.STORY)) {
                stories.add((Story) task);
            }
        }


        return stories;
    }

    @Override
    public Board findBoardInTeam(String name) {
        return getBoards().stream()
                .filter(board -> name.equals(board.getName())).findAny()
                .orElseThrow(() -> new ElementNotFoundException(String.format(CoreConstants.ELEMENT_NOT_FOUND, name)));
    }

    @Override
    public boolean checkMemberIsFromTeam(String memberName, String teamName) {
        Member member = findMemberByName(memberName);
        Team team = findTeamByName(teamName);

        return member.getTeams().contains(team);
    }

    @Override
    public Team findTeamByName(String teamName) {
        return getTeams().stream()
                .filter(team -> teamName.equals(team.getName())).findAny()
                .orElseThrow(() -> new ElementNotFoundException(String.format(CoreConstants.ELEMENT_NOT_FOUND, teamName)));
    }

    @Override
    public Member findMemberByName(String name) {
        return getMembers().stream()
                .filter(member -> name.equals(member.getName())).findAny()
                .orElseThrow(() -> new ElementNotFoundException(String.format(CoreConstants.ELEMENT_NOT_FOUND, name)));

    }

    @Override
    public Board getBoard(Board board) {
        return getBoards().stream()
                .filter(board1 -> board1.equals(board)).findAny()
                .orElseThrow(() -> new ElementNotFoundException(String.format(CoreConstants.ELEMENT_NOT_FOUND, board.getName())));
    }

}
