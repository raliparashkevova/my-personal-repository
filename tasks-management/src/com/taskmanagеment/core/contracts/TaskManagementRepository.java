package com.taskmanagеment.core.contracts;

import com.taskmanagеment.commands.creation.CreateNewTaskCommand;
import com.taskmanagеment.models.contracts.*;
import com.taskmanagеment.models.enums.*;

import java.util.List;

public interface TaskManagementRepository {

    List<Member> getMembers();

    List<Task> getTasks();

    List<ActivityHistory> getHistory();


    Member findByMemberName(String memberName);

    Member createMember(String name);

    Team findByTeamName(String teamName);

    Team createTeam(String name);

    List<Team> getTeams();

    Board findByBoardName(String boardName);

    Board createBoard(String name);

    List<Board> getBoards();

    Bug createBug(int id, String title, String description, Priority priority, Severity severity, BugStatus bugStatus, String assignee);

    Story createStory(int id, String title, String description, Priority priority, Size size, StoryStatus storyStatus, String assignee);

    FeedBack createFeedBack(int id, String title, String description, int rating, FeedBackStatus feedBackStatus);


    List<ActivityHistory> getHistoryByName(String name);

    boolean memberExist(String memberName);

    <T extends Identifiable> T findElementById(List<T> elements, int id);

    void validateMemberIsFromTeam(int taskIndex, String author);

    Comment createComment(String content, String author);

    boolean teamExist(String teamName);

    boolean boardExist(String boardName);

    void addMemberToTeam(Member member, Team team);

    void addBoardToTeam(Board board, Team team);

    void changeLabelPriorityBug(Bug bug, Priority priority);

    void changeLabelSeverityBug(Bug bug, Severity severity);

    void changeLabelBugStatus(Bug bug, BugStatus bugStatus);

    void changeLabelRating(int rating, FeedBack feedBack);

    void changeLabelStoryPriority(Priority priority, Story story);

    void changeLabelSize(Size size, Story story);

    void changeLabelStoryStatus(StoryStatus storyStatus, Story story);

    void changeFeedBackStatus(FeedBackStatus feedBackStatus, FeedBack feedBack);

    void removeBoard(String boardName);

    void removeTask(Task task);


    List<Bug> getBugTasks();

    List<Story> getStoryTask();

    Board findBoardInTeam(String s);

    boolean checkMemberIsFromTeam(String additionalParam, String name);

    Team findTeamByName(String teamName);

    Member findMemberByName(String name);

    Board getBoard(Board board);


}
