package com.telerikacademy.oop.cosmetics.exceptions;

import org.junit.jupiter.api.Test;

public class CreateCategoryCommandTests {
    // @BeforeEach may help here

    @Test
    public void execute_should_addNewCategoryToRepository_when_validParameters() {

    }

    @Test
    public void execute_should_throwException_when_missingParameters() {

    }

    @Test
    public void execute_should_throwException_when_duplicateCategoryName() {

    }

}
