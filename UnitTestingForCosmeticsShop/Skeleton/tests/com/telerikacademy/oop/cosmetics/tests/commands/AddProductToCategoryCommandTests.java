package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddProductToCategoryCommandTests {

    Command command;
    ProductRepository productRepository;
    private Product product;


    @BeforeEach
    public void initBeforeEach() {
        productRepository = new ProductRepositoryImpl();
        this.command = new AddProductToCategoryCommand(productRepository);
        product = new ProductImpl("Deva", "Nivea", 4.5, GenderType.MEN);

    }

    @Test
    public void should_addProductToCategory_when_productIsValid() {
        // Arrange


        productRepository.createProduct(product.getName(),
                product.getBrand(),
                product.getPrice(),
                product.getGender());
        // Act, Assert
        Assertions.assertEquals(1, productRepository.getProducts().size());
    }

    @Test
    public void shouldThrowInvalidUserInputException__when_productNameDoesNotExists() {
        // Arrange


        productRepository.createProduct(product.getName(),
                product.getBrand(),
                product.getPrice(),
                product.getGender());
        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> productRepository.findProductByName(TestData.Product.INVALID_NAME));
    }

}
