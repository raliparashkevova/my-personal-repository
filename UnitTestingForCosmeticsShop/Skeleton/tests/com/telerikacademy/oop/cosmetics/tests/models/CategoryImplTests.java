package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import java.util.ArrayList;
import java.util.List;

import static utils.TestData.Category.VALID_NAME;


public class CategoryImplTests {
    Category category;
    Product product;
    private List<Category> categories;
    private List<Product> products;


    @BeforeEach
    public void initBeforeEach() {
        category = new CategoryImpl(VALID_NAME);
        product = new ProductImpl(TestData.Product.VALID_NAME,
                TestData.Product.VALID_BRAND_NAME,
                Double.parseDouble(TestData.Product.VALID_PRICE),
                GenderType.MEN);
        this.categories = new ArrayList<>();
        categories.add(category);
        this.products = new ArrayList<>();
        this.products.add(product);
    }


    @Test
    public void constructor_should_createCategory_when_argumentsAreValid() {
        Category category = new CategoryImpl(VALID_NAME);

        Assertions.assertEquals(VALID_NAME, categories.get(0).getName());
    }

    @Test
    public void constructor_should_throwException_when_nameShorterThanExpected() {


        Assertions.assertThrows(InvalidUserInputException.class, () -> new CategoryImpl("C"));
    }

    @Test
    public void constructor_should_throwException_when_nameLongerThanExpected() {


        Assertions.assertThrows(InvalidUserInputException.class, () -> new CategoryImpl("Coracoracora"));
    }

    @Test
    public void constructor_should_throwException_when_nameIsCorrect() {


        Assertions.assertEquals(category.getName(), VALID_NAME);
    }

    @Test
    public void addProduct_should_addProductToList() {

        Assertions.assertEquals(1, products.size());

    }

    @Test
    public void should_DecreaseSize_When_ElementExists() {
        products.remove(product);

        Assertions.assertEquals(0, products.size());
    }


    @Test
    public void should_RemoveElement_When_ElementExists() {


        Assertions.assertTrue(products.remove(product));

    }

    @Test
    public void removeProduct_should_notRemoveProductFromList_when_productNotExist() {


        Product productTest = new ProductImpl("Sara", "Nivea", 4.5, GenderType.WOMEN);


        Assertions.assertFalse(category.getProducts().remove(productTest));
    }

}
