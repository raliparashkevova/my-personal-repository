package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import java.util.ArrayList;

import static utils.TestData.Category.VALID_NAME;

public class ProductImplTests {

    private Product product;

    @BeforeEach
    public void initBeforeEach() {
        product = new ProductImpl(TestData.Product.VALID_NAME,
                TestData.Product.VALID_BRAND_NAME,
                Double.parseDouble(TestData.Product.VALID_PRICE),
                GenderType.MEN);
    }

    @Test
    public void should_ProductImpl_extend_productInterface() {
        ProductImpl product = new ProductImpl(TestData.Product.VALID_NAME,
                TestData.Product.VALID_BRAND_NAME,
                Double.parseDouble(TestData.Product.VALID_PRICE),
                GenderType.MEN);
        Assertions.assertTrue(product instanceof Product);
    }

    @Test
    public void constructor_should_Product_when_argumentsAreValid() {
        Category category = new CategoryImpl(VALID_NAME);

        Assertions.assertEquals(VALID_NAME, category.getName());
    }

    @Test
    public void should_nameLength_in_bounds() {
        //Arrange
        int size = product.getName().length();

        //Act

        //Assertion
        Assertions.assertEquals(size, 4);
    }

    @Test
    public void should_throwException_whenNameLengthWithLessLength() {

        Assertions.assertThrows(InvalidUserInputException.class
                , () -> new ProductImpl("D", "Nivea", 5.5, GenderType.MEN));

    }

    @Test
    public void should_throwException_whenNameLengthWithMoreLength() {

        Assertions.assertThrows(InvalidUserInputException.class, () -> new ProductImpl("Devarararra", "Nivea", 5.5, GenderType.MEN));

    }

    @Test
    public void should_create_correctProduct() {
        Product product = new ProductImpl("Deva", "Nivea", 4.5, GenderType.MEN);

        Assertions.assertEquals("Deva", product.getName());
        Assertions.assertEquals("Nivea", product.getBrand());
        Assertions.assertEquals(4.5, product.getPrice());
        Assertions.assertEquals(GenderType.MEN, product.getGender());

    }

    @Test
    public void should_brandLength_isCorrect() {
        //Arrange
        Product product = new ProductImpl("Deva", "Nivea", 4.5, GenderType.MEN);
        int size = product.getBrand().length();

        //Act

        //Assertion
        Assertions.assertEquals(size, 5);
    }

    @Test
    public void should_throwException_whenBrandLengthWithLessLength() {

        Assertions.assertThrows(InvalidUserInputException.class
                , () -> new ProductImpl("Deva", "N", 5.5, GenderType.MEN));

    }

    @Test
    public void should_throwException_whenBrandLengthWithMoreLength() {

        Assertions.assertThrows(InvalidUserInputException.class, () -> new ProductImpl("Deva", "NiveaNiveaNivea", 5.5, GenderType.MEN));

    }

    @Test
    public void should_price_isPositive() {
        //Arrange
        Product product = new ProductImpl("Deva", "Nivea", 4.5, GenderType.MEN);

        double price = product.getPrice();

        //Act

        //Assertion
        Assertions.assertEquals(price, 4.5);
    }

    @Test
    public void should_throwException_whenPriceIsNegative() {

        Assertions.assertThrows(InvalidUserInputException.class, () -> new ProductImpl("Deva", "NiveaNiveaNivea", -5.5, GenderType.MEN));

    }
}
