package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static utils.TestData.Category.INVALID_NAME;
import static utils.TestData.Category.VALID_NAME;

public class ShowCategoryCommandTests {

    Command command;
    ProductRepository productRepository;
    List<String> parameters;


    @BeforeEach
    public void initBeforeEach() {
        productRepository = new ProductRepositoryImpl();
        this.command = new AddProductToCategoryCommand(productRepository);
        this.parameters = new ArrayList<>();
        parameters.add(VALID_NAME);
    }

    @Test
    public void should_throwException_when_argumentsCountDifferentThenExpected() {

        parameters.remove(0);
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }

    @Test
    public void should_throwException_when_categoryNameDoesNotExists() {

        parameters.set(0, INVALID_NAME);

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }

}
