package com.telerikacademy.oop.cosmetics.tests.core;

import org.junit.jupiter.api.Test;

public class ProductRepositoryImplTests {

    @Test
    public void constructor_should_initializeAllCollections() {

    }

    @Test
    public void getCategories_should_returnCopyOfCollection() {
    }

    @Test
    public void getProducts_should_returnCopyOfCollection() {
    }

    @Test
    public void categoryExists_should_returnTrue_whenCategoryExists() {

    }

    @Test
    public void productExists_should_returnTrue_whenProductExists() {

    }

    @Test
    public void createProduct_should_createSuccessfully_whenArgumentsAreValid() {

    }

    @Test
    public void createCategory_should_createSuccessfully_whenArgumentsAreValid() {

    }

    @Test
    public void findCategoryByName_should_returnCategory_ifExists() {

    }

    @Test
    public void findCategoryByName_should_throwException_ifDoesNotExist() {

    }

    @Test
    public void findProductByName_should_returnCategory_ifExists() {

    }

    @Test
    public void findProductByName_should_throwException_ifDoesNotExist() {

    }

}
