package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateProductCommandTests {

    Command command;
    ProductRepository productRepository;
    List<String> parameters;


    @BeforeEach
    public void initBeforeEach() {
        productRepository = new ProductRepositoryImpl();
        this.command = new AddProductToCategoryCommand(productRepository);
        this.parameters = new ArrayList<>();
        parameters.add(TestData.Product.VALID_NAME);
        parameters.add(TestData.Product.VALID_BRAND_NAME);
        parameters.add(TestData.Product.VALID_PRICE);
        parameters.add(String.valueOf(GenderType.MEN));


    }

    @Test
    public void should_throwException_when_argumentsCountDifferentThenExpected() {

        parameters.remove(0);
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }

    @Test
    public void should_throwException_when_brandShorterThanExpected() {
        //Arrange

        parameters.set(1, "D");
        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }

    @Test
    public void should_throwException_when_BrandLongerThanExpected() {
        //Arrange

        parameters.set(2, "DevaDevaDevaDeva");
        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }

    @Test
    public void should_throwException_when_priceInvalid() {
        //Arrange

        parameters.set(2, String.valueOf(-5));
        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }

    @Test
    public void should_throwException_when_nameShorterThanExpected() {
        //Arrange

        parameters.set(0, "D");
        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }

    @Test
    public void should_throwException_when_nameLongerThanExpected() {
        //Arrange

        parameters.set(0, "DevaDevaDevaDeva");
        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(parameters));
    }


}
