package utils;

import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;

public class TestData {

    public static class Product {

        public static final String VALID_NAME = "Deva";
        public static final String VALID_BRAND_NAME = "Nivea";
        public static final String VALID_PRICE = String.valueOf(4.5);
        public static final String INVALID_NAME = "Dove";


    }

    public static class Category {

        public static final String VALID_NAME = "Cream";
        public static final String INVALID_NAME = "Dove";

    }
}
