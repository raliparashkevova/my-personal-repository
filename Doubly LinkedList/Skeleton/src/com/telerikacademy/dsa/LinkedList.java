package com.telerikacademy.dsa;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    private Node head;
    private Node tail;
    private int size = 0;

    public LinkedList() {
        head = null;
        tail = null;
    }

    public LinkedList(Iterable<T> iterable) {
        iterable.forEach(this::addLast);
    }

    @Override
    public void addFirst(T value) {
        Node nodeToAdd = new Node(value);

        if (size() == 0) {
            addFirstLastElementWhenIsEmpty(nodeToAdd);
            return;
        }

        connectRightAndLeftNode(nodeToAdd, head);
        head = nodeToAdd;
        size++;

    }


    @Override
    public void addLast(T value) {
        Node nodeToAdd = new Node(value);
        if (size() == 0) {
            addFirstLastElementWhenIsEmpty(nodeToAdd);
            return;
        }
        connectRightAndLeftNode(tail, nodeToAdd);
        tail = nodeToAdd;
        size++;

    }

    @Override
    public void add(int index, T value) {
        if (index == 0) {
            addFirst(value);
            return;
        }
        if (index == size()) {
            addLast(value);
            return;
        }
        Node previous = getNodeByIndex(index - 1);
        Node next = previous.next;
        Node newest = new Node(value);

        connectRightAndLeftNode(previous, newest);
        connectRightAndLeftNode(newest, next);
        size++;


    }

    @Override
    public T getFirst() {
        throwIfEmpty();
        return head.value;

    }

    @Override
    public T getLast() {
        throwIfEmpty();
        return tail.value;
    }

    @Override
    public T get(int index) {
        throwIfEmpty();
        return getNodeByIndex(index).value;
    }

    @Override
    public int indexOf(T value) {

        return getIndexByElement(value);
    }

    @Override
    public T removeFirst() {
        throwIfEmpty();

        if (size() == 1) {
            return removeElementWhenListSizeIsOne();
        }
        T element = head.value;
        head = head.next;
        head.prev = null;
        size--;
        return element;
    }

    @Override
    public T removeLast() {
        throwIfEmpty();

        if (size() == 1) {

            return removeElementWhenListSizeIsOne();
        }
        T element = tail.value;
        tail = tail.prev;
        tail.next = null;
        size--;

        return element;
    }

    @Override
    public int size() {
        return size;
    }

    private void addFirstLastElementWhenIsEmpty(Node nodeToAdd) {

        head = nodeToAdd;
        tail = nodeToAdd;
        size++;
    }


    private void throwIfEmpty() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
    }


    private int getIndexByElement(T value) {
        Node current = head;

        for (int i = 0; i < size; i++) {
            if (current.value == value) {
                return i;
            }
            current = current.next;
        }

        return -1;
    }


    private T removeElementWhenListSizeIsOne() {
        T element = head.value;
        head = null;
        tail = null;
        size--;
        return element;
    }


    @Override
    public Iterator<T> iterator() {
        return new IteratorImpl();
    }

    private Node getNodeByIndex(int index) {
        throwIfInvalidIndex(index);
        Node temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return temp;
    }

    private void throwIfInvalidIndex(int index) {
        if (index < 0 || index > size()) {
            throw new NoSuchElementException();
        }
    }

    private void connectRightAndLeftNode(Node leftNode, Node rightNode) {
        leftNode.next = rightNode;
        rightNode.prev = leftNode;
    }

    private class Node {
        T value;
        Node prev;
        Node next;

        Node(T value) {
            this.value = value;
            this.prev = null;
            this.next = null;
        }
    }

    private class IteratorImpl implements Iterator<T> {

        private Node current = head;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {

            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            T element = current.value;
            current = current.next;
            return element;
        }
    }
}
