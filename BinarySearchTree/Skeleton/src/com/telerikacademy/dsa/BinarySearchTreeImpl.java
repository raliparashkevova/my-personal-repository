package com.telerikacademy.dsa;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinarySearchTreeImpl<E extends Comparable<E>> implements BinarySearchTree<E> {
    private E value;
    private BinarySearchTreeImpl<E> left;
    private BinarySearchTreeImpl<E> right;

    public BinarySearchTreeImpl(E value) {
        this.value = value;
        left = null;
        right = null;
    }

    public BinarySearchTreeImpl(E value, BinarySearchTreeImpl<E> left
            , BinarySearchTreeImpl<E> right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    //BinarySearchTree<E> root;

    @Override
    public E getRootValue() {
        return value;
    }


    @Override
    public BinarySearchTree<E> getLeftTree() { // ни връща лявото дърво
        return left;
    }

    @Override
    public BinarySearchTree<E> getRightTree() {
        return right;
    }

    @Override
    public void insert(E value) {

        if (isEmpty()) {
            throw new IllegalArgumentException("Value cannot be empty");
        }

        if (value.compareTo(getRootValue()) < 0) {
            if (getLeftTree() == null) {
                this.left = new BinarySearchTreeImpl<>(value);
            } else {
                getLeftTree().insert(value);
            }
        } else if (value.compareTo(getRootValue()) > 0) {
            if (getRightTree() == null) {
                this.right = new BinarySearchTreeImpl<>(value);
            } else {
                getRightTree().insert(value);
            }
        } else {
            throw new IllegalArgumentException("Value is already present!");
        }
    }

    private boolean isEmpty() {
        return this.value == null;
    }

    @Override
    public boolean search(E value) { // ако сме в класа да използваме самите variables, гетерите са публични методи, прези които могат да се достъпват стойностите отвън
        if (value.compareTo(getRootValue()) == 0) {
            return true;
        }
        if (value.compareTo(getRootValue()) < 0 && getLeftTree() != null) { // тук проверяваме дали стойността е по-малка от тази в руут=а за да знаем дали ще бъде отляво или отдясно
            return getLeftTree().search(value);
        }
        if (value.compareTo(getRootValue()) > 0 && getRightTree() != null) {
            return getRightTree().search(value);
        }

        return false;
    }

    @Override
    public List<E> inOrder() {
        List<E> result = new ArrayList<>();

        if (left != null) {
            result.addAll(left.inOrder());
        }
        result.add(getRootValue());
        if (right != null) {
            result.addAll(right.inOrder());
        }
        return result;
    }

    @Override
    public List<E> postOrder() {
        List<E> list = new ArrayList<>();

        if (left != null) {
            list.addAll(left.postOrder());
        }
        if (right != null) {
            list.addAll(right.postOrder());
        }
        list.add(getRootValue());
        return list;
    }

    @Override
    public List<E> preOrder() {
        List<E> result = new ArrayList<>();
        result.add(getRootValue());
        if (left != null) {
            result.addAll(left.preOrder());
        }
        if (right != null) {
            result.addAll(right.preOrder());
        }
        return result;
    }


    @Override
    public List<E> bfs() { // обхождаме по нива от root към децата
        Queue<BinarySearchTreeImpl<E>> queue = new LinkedList<>();
        List<E> result = new ArrayList<>();
        queue.offer(this); // добавяме корена
        while (!queue.isEmpty()) { // докато опашкато има нещо в нея
            BinarySearchTreeImpl<E> current = queue.poll(); // първово нещо което трябва да извадим
            result.add(current.getRootValue());
            System.out.print(current.value + " ");
            if (current.left != null) {
                queue.offer(current.left);
            }
            if (current.right != null) {
                queue.offer(current.right);
            }
        }

        return result;
    }

    @Override
    public int height() {
        //Check whether tree is empty
        if (getRootValue() == null) {
            return 0;
        } else {
            int leftHeight = -1, rightHeight = -1;
            //Calculate the height of left subtree
            if (getLeftTree() != null)
                leftHeight = getLeftTree().height();
            //Calculate the height of right subtree
            if (getRightTree() != null)
                rightHeight = getRightTree().height();
            //Compare height of left subtree and right subtree
            //and store maximum of two in variable max
            int max = Math.max(leftHeight, rightHeight);
            //Calculate the total height of tree by adding height of root
            return max + 1;
        }
    }

    @Override
    public boolean remove(E value) {

        if (value.compareTo(this.value) == 0 && left == null && right == null) {
            throw new IllegalArgumentException("Tree cannot be empty");
        }


        return removeNodeRecursively(value, this); // this в момента е root
    }

    private boolean removeNodeRecursively(E value
            , BinarySearchTreeImpl<E> parent) {

        if (value.compareTo(this.value) == 0) { // ако стойността, която подаваме
            // отвън съвпада с стойност от дървото, тогава ще изтрием
            if (isLeaf(this)) {
                removeLeaf(this, parent);
                return true;
            }
            BinarySearchTreeImpl<E> nodeToRemove;
            if (left != null) {
                nodeToRemove = left.getMaxValueNode();
            } else {
                nodeToRemove = right.getMinValueNode();
            }

            BinarySearchTreeImpl<E> nodeToRemoveParent = getParent(nodeToRemove, this);
            E tempValue = nodeToRemove.value;
            removeNodeRecursively(nodeToRemove.value, nodeToRemoveParent);
            this.value = tempValue;

            return true;

        } else if (value.compareTo(getRootValue()) < 0 && getLeftTree() != null) {
            return left.removeNodeRecursively(value, this); // тук This e roditel na left

        } else if (value.compareTo(this.value) > 0 && getRightTree() != null) {
            return right.removeNodeRecursively(value, this);
        } else {
            return false;
        }
    }

    private BinarySearchTreeImpl<E> getParent(BinarySearchTreeImpl<E> node
            , BinarySearchTreeImpl<E> root) {

        if (root.left == node || root.right == node) {
            return root;
        }
        if (node.value.compareTo(root.value) <= 0) {
            return getParent(node, root.left);
        }
        return getParent(node, root.right);
    }

    private BinarySearchTreeImpl<E> getMinValueNode() {
        if (left == null) {
            return this;
        }

        return left.getMinValueNode();
    }

    private BinarySearchTreeImpl<E> getMaxValueNode() {
        if (right == null) {
            return this;
        }
        return right.getMaxValueNode();
    }

    private void removeLeaf(BinarySearchTreeImpl<E> leaf
            , BinarySearchTreeImpl<E> parent) {

        if (parent.left != null && parent.left.getRootValue().compareTo(leaf.getRootValue()) == 0) {
            parent.left = null;

        } else {
            parent.right = null;
        }
    }

    private boolean isLeaf(BinarySearchTreeImpl<E> node) {
        return node.left == null && node.right == null;
    }
}
