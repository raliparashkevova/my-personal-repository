package web.stuffleavetool.controlers.mvcControlers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    @GetMapping
    public String showHomePage() {
        return "login";
    }

}
