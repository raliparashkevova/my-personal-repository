package web.stuffleavetool.controlers.mvcControlers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import web.stuffleavetool.dtos.UserLoginDto;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new UserLoginDto());
        return "login";
    }


    }



