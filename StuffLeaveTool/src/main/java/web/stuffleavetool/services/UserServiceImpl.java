package web.stuffleavetool.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import web.stuffleavetool.models.Role;
import web.stuffleavetool.models.User;
import web.stuffleavetool.repositories.contracts.RoleRepository;
import web.stuffleavetool.repositories.contracts.UserRepository;
import web.stuffleavetool.services.comtracts.UserService;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;


    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void registerAdmin() {
//        if (userRepository.getAll().size() == 0){
//            User user = new User();
//            user.setFirstName("Ralitsa");
//            user.setMiddleName("Ralieva");
//            user.setLastName("Ralova");
//            user.setPassword(bCryptPasswordEncoder.encode("ralili27"));
//            user.setEmail("ralili@abv.bg");
//            user.setEnabled(true);
//
//            Role role = roleRepository.getByName("ADMIN");
//            user.getRoles().add(role);
//            userRepository.create(user);
//        }
    }

    @Override
    public User findUserByUsername(String userName) {
        return userRepository.getUserByUsername(userName);
    }
}
