package web.stuffleavetool.services.comtracts;

import web.stuffleavetool.models.User;

public interface UserService {

    void registerAdmin();

    User findUserByUsername(String userName);
}
