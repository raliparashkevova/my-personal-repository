package web.stuffleavetool.repositories.implementations;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import web.stuffleavetool.models.Role;
import web.stuffleavetool.repositories.contracts.BaseCrudRepository;
import web.stuffleavetool.repositories.contracts.RoleRepository;

import java.util.List;

@Repository
public class RoleRepositoryImpl extends CrudRepositoryImpl<Role> implements RoleRepository {


//    private SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(Class<Role> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);

    }

    @Override
    public void create(Role entity) {

    }

    @Override
    public void update(Role entity) {

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public List<Role> getAll() {
        return null;
    }

    @Override
    public Role getById(int id) {
        return super.getByField("id",id);
    }

    @Override
    public Role getByName(String admin) {
        return super.getByField("admin",admin);
    }
}
