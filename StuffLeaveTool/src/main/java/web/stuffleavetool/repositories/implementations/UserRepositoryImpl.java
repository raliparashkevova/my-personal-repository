package web.stuffleavetool.repositories.implementations;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import web.stuffleavetool.models.User;
import web.stuffleavetool.repositories.contracts.UserRepository;

@Repository
public class UserRepositoryImpl extends CrudRepositoryImpl<User> implements UserRepository {


    @Autowired
    public UserRepositoryImpl(Class<User> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }

    @Override
    public User getUserByUsername(String username) {
        return super.getByField("username",username);
    }
}
