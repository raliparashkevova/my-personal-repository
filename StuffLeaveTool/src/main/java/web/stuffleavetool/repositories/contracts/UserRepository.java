package web.stuffleavetool.repositories.contracts;

import web.stuffleavetool.models.User;

public interface UserRepository extends BaseCrudRepository<User>{
    User getUserByUsername(String username);
}
