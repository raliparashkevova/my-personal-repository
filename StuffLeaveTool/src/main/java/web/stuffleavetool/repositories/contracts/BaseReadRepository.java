package web.stuffleavetool.repositories.contracts;

import java.util.List;

public interface BaseReadRepository<T>{

    List<T> getAll();

    T getById(int id);

}
