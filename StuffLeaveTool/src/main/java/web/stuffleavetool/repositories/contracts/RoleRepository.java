package web.stuffleavetool.repositories.contracts;

import web.stuffleavetool.models.Role;

public interface RoleRepository extends BaseCrudRepository<Role>{
    Role getByName(String admin);
}
