package web.stuffleavetool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StuffLeaveToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(StuffLeaveToolApplication.class, args);
    }

}
