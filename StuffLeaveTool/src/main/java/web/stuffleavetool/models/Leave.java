package web.stuffleavetool.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "leaves")
public class Leave implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "leave_id")
    private int leaveId;

    @Enumerated(EnumType.STRING)
    private LeaveType leaveType;

    @Column(name = "rest_days")
    private int restLeaveDays;
    @Column(name = "approved_date")
    private LocalDateTime approvedDate;
    @Column(name = "start_leave_date")
    private LocalDateTime startDateLeave;
    @Column(name = "end_leave_date")
    private LocalDateTime endDateLeave;

    @Column(name = "duration_leave")
    private int durationLeave;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    public Leave() {
    }

    public int getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(int leaveId) {
        this.leaveId = leaveId;
    }

    public LeaveType getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(LeaveType leaveType) {
        this.leaveType = leaveType;
    }

    public int getRestLeaveDays() {
        return restLeaveDays;
    }

    public void setRestLeaveDays(int restLeaveDays) {
        this.restLeaveDays = restLeaveDays;
    }

    public LocalDateTime getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDateTime approvedDate) {
        this.approvedDate = approvedDate;
    }

    public int getDurationLeave() {
        return durationLeave;
    }

    public void setDurationLeave(int durationLeave) {
        this.durationLeave = durationLeave;
    }

    public LocalDateTime getStartDateLeave() {
        return startDateLeave;
    }

    public void setStartDateLeave(LocalDateTime startDateLeave) {
        this.startDateLeave = startDateLeave;
    }

    public LocalDateTime getEndDateLeave() {
        return endDateLeave;
    }

    public void setEndDateLeave(LocalDateTime endDateLeave) {
        this.endDateLeave = endDateLeave;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Leave leave = (Leave) o;

        if (leaveId != leave.leaveId) return false;
        if (restLeaveDays != leave.restLeaveDays) return false;
        if (durationLeave != leave.durationLeave) return false;
        if (leaveType != leave.leaveType) return false;
        if (approvedDate != null ? !approvedDate.equals(leave.approvedDate) : leave.approvedDate != null) return false;
        if (startDateLeave != null ? !startDateLeave.equals(leave.startDateLeave) : leave.startDateLeave != null)
            return false;
        if (endDateLeave != null ? !endDateLeave.equals(leave.endDateLeave) : leave.endDateLeave != null) return false;
        return user != null ? user.equals(leave.user) : leave.user == null;
    }

    @Override
    public int hashCode() {
        int result = leaveId;
        result = 31 * result + (leaveType != null ? leaveType.hashCode() : 0);
        result = 31 * result + restLeaveDays;
        result = 31 * result + (approvedDate != null ? approvedDate.hashCode() : 0);
        result = 31 * result + (startDateLeave != null ? startDateLeave.hashCode() : 0);
        result = 31 * result + (endDateLeave != null ? endDateLeave.hashCode() : 0);
        result = 31 * result + durationLeave;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }
}
