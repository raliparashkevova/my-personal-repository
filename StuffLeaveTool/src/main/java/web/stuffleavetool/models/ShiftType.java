package web.stuffleavetool.models;

public enum ShiftType {

    DAILY("07:00-19:00"),
    NIGHT("19:00-07:00"),
    REGULAR("08:00-17:00");

    private String type;

    ShiftType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
