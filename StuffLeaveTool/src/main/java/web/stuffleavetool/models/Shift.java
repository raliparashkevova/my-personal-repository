package web.stuffleavetool.models;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "shifts")
public class Shift implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shift_id")
    private int shiftId;
    @Column(name = "shift_type")
    private ShiftType shiftType;
    @Column(name = "start_shift")
    private LocalDateTime starShift;
    @Column(name = "end_shift")
    private LocalDateTime endShift;



    public Shift() {
    }

    public int getShiftId() {
        return shiftId;
    }

    public void setShiftId(int shiftId) {
        this.shiftId = shiftId;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }

    public LocalDateTime getStarShift() {
        return starShift;
    }

    public void setStarShift(LocalDateTime starShift) {
        this.starShift = starShift;
    }

    public LocalDateTime getEndShift() {
        return endShift;
    }

    public void setEndShift(LocalDateTime endShift) {
        this.endShift = endShift;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shift shift = (Shift) o;

        if (shiftId != shift.shiftId) return false;
        if (shiftType != shift.shiftType) return false;
        if (starShift != null ? !starShift.equals(shift.starShift) : shift.starShift != null) return false;
        return endShift != null ? endShift.equals(shift.endShift) : shift.endShift == null;
    }

    @Override
    public int hashCode() {
        int result = shiftId;
        result = 31 * result + (shiftType != null ? shiftType.hashCode() : 0);
        result = 31 * result + (starShift != null ? starShift.hashCode() : 0);
        result = 31 * result + (endShift != null ? endShift.hashCode() : 0);
        return result;
    }
}
