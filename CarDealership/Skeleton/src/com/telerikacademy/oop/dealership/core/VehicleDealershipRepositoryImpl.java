package com.telerikacademy.oop.dealership.core;


import com.telerikacademy.oop.dealership.core.contracts.VehicleDealershipRepository;
import com.telerikacademy.oop.dealership.models.*;
import com.telerikacademy.oop.dealership.models.contracts.*;
import com.telerikacademy.oop.dealership.models.enums.UserRole;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;


public class VehicleDealershipRepositoryImpl implements VehicleDealershipRepository {

    private static final String NO_LOGGED_IN_USER = "There is no logged in user.";
    private final static String NO_SUCH_USER = "There is no user with username %s!";
    public static final int CONTENT_LEN_MIN = 3;
    public static final int CONTENT_LEN_MAX = 200;
    private static final String CONTENT_LEN_ERR = format(
            "Content must be between %d and %d characters long!",
            CONTENT_LEN_MIN,
            CONTENT_LEN_MAX);

    private final List<User> users;
    private User loggedUser;

    public VehicleDealershipRepositoryImpl() {
        this.users = new ArrayList<>();
        this.loggedUser = null;
    }

    @Override
    public List<User> getUsers() {
        return new ArrayList<>(users);
    }

    @Override
    public void addUser(User userToAdd) {
        if (!users.contains(userToAdd)) {
            this.users.add(userToAdd);
        }
    }

    @Override
    public User findUserByUsername(String username) {
        User user = users
                .stream()
                .filter(u -> u.getUsername().equalsIgnoreCase(username))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_USER, username)));
        return user;
    }

    @Override
    public User getLoggedInUser() {
        if (loggedUser == null) {
            throw new IllegalArgumentException(NO_LOGGED_IN_USER);
        }
        return loggedUser;
    }

    @Override
    public boolean hasLoggedInUser() {
        return loggedUser != null;
    }

    @Override
    public void login(User user) {
        loggedUser = user;
    }

    @Override
    public void logout() {
        loggedUser = null;
    }

    @Override
    public Car createCar(String make, String model, double price, int seats) {
        return new CarImpl(make, model, price, seats);
    }

    @Override
    public Motorcycle createMotorcycle(String make, String model, double price, String category) {
        return new MotorcycleImpl(make, model, price, category);
    }

    @Override
    public Truck createTruck(String make, String model, double price, int weightCapacity) {
        return new TruckImpl(make, model, price, weightCapacity);
    }

    @Override
    public User createUser(String username, String firstName, String lastName, String password, UserRole userRole) {
        return new UserImpl(username, firstName, lastName, password, userRole);
    }

    @Override
    public Comment createComment(String content, String author) {
        ValidationHelpers.validateIntRange(content.length(), CONTENT_LEN_MIN, CONTENT_LEN_MAX, CONTENT_LEN_ERR);
        return new CommentImpl(content, author);
    }
}
