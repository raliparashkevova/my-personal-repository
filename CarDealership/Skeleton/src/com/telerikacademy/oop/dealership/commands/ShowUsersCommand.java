package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.VehicleDealershipRepository;
import com.telerikacademy.oop.dealership.models.UserImpl;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.models.enums.UserRole;
import com.telerikacademy.oop.dealership.utils.ParsingHelpers;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import java.util.List;

public class ShowUsersCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;
    private final static String USER_HEADER = "--USER %s--";

    private final VehicleDealershipRepository vehicleDealershipRepository;

    public ShowUsersCommand(VehicleDealershipRepository vehicleDealershipRepository) {
        this.vehicleDealershipRepository = vehicleDealershipRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        StringBuilder sb = new StringBuilder();

        sb.append(USER_HEADER);
        UserRole user = vehicleDealershipRepository.getLoggedInUser().getRole();

        if (String.valueOf(user).equals("Admin")) {

            List<User> users = vehicleDealershipRepository.getUsers();
            int count = 1;
            for (User user1 : users) {
                sb.append(String.format("%d", count++));
                sb.append(user1.toString());
            }
            return sb.toString();
        }

        throw new IllegalArgumentException("You are not an admin!");
    }


}
