package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Car;
import com.telerikacademy.oop.dealership.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import static java.lang.String.format;

public class CarImpl extends BaseVehicle implements Car {
    public static final int CAR_WHEELS = 4;
    public static final int CAR_SEATS_MIN = 1;
    public static final int CAR_SEATS_MAX = 10;
    private static final String CAR_SEATS_ERR = format(
            "Seats must be between %d and %d!",
            CAR_SEATS_MIN,
            CAR_SEATS_MAX);

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(VehicleType.CAR, make, model, CAR_WHEELS, price);
        setSeats(seats);
    }

    @Override
    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        validateSeats(seats);
        this.seats = seats;
    }

    protected void validateSeats(int seats) {

        ValidationHelpers.validateIntRange(seats, CAR_SEATS_MIN, CAR_SEATS_MAX
                , CAR_SEATS_ERR);
    }

    @Override
    String printAdditionalInfo() {
        return String.format("Seats: %d\n", getSeats());
    }


}
