package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Car;
import com.telerikacademy.oop.dealership.models.contracts.Motorcycle;
import com.telerikacademy.oop.dealership.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import static java.lang.String.format;

public class MotorcycleImpl extends BaseVehicle implements Motorcycle {

    public static final int MOTORCYCLE_WHEELS_COUNT = 2;
    public static final int CATEGORY_LEN_MIN = 3;
    public static final int CATEGORY_LEN_MAX = 10;
    private static final String CATEGORY_LEN_ERR = format(
            "Category must be between %d and %d characters long!",
            CATEGORY_LEN_MIN,
            CATEGORY_LEN_MAX);

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(VehicleType.MOTORCYCLE, make, model, MOTORCYCLE_WHEELS_COUNT, price);
        setCategory(category);
    }

    private void setCategory(String category) {
        validateCategoryMotorCycle(category);
        this.category = category;
    }

    protected void validateCategoryMotorCycle(String category) {

        int categoryLength = category.length();

        ValidationHelpers.validateIntRange(categoryLength, CATEGORY_LEN_MIN, CATEGORY_LEN_MAX, CATEGORY_LEN_ERR);

    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    String printAdditionalInfo() {
        return String.format("Category: %s\n", getCategory());
    }
}
