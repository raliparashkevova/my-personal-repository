package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.FormattingHelpers;
import com.telerikacademy.oop.dealership.utils.ParsingHelpers;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.dealership.models.ModelConstants.*;

public abstract class BaseVehicle implements Vehicle {

    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private VehicleType vehicleType;
    private String make;
    private String model;
    private int wheelsCount;
    private double price;
    private final List<Comment> comments;

    public BaseVehicle(VehicleType vehicleType, String make, String model, int wheelsCount, double price) {
        this.vehicleType = vehicleType;
        setMake(make);
        setModel(model);
        this.wheelsCount = wheelsCount;
        setPrice(price);
        comments = new ArrayList<>();
    }


    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public int getWheels() {
        return wheelsCount;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void addComment(Comment comment) {

        comments.add(comment);
    }

    @Override
    public void removeComment(Comment comment) {


        getComments().remove(comment);
    }

    private void setMake(String make) {
        validateMake(make);
        this.make = make;
    }

    private void setModel(String model) {
        validateModel(model);
        this.model = model;
    }

    private void setPrice(double price) {
        validatePrice(price);

        this.price = price;
    }

    protected void validateMake(String make) {
        int value = make.length();
        ValidationHelpers.validateIntRange(value, MAKE_NAME_LEN_MIN, MAKE_NAME_LEN_MAX
                , MAKE_NAME_LEN_ERR);
    }

    protected void validateModel(String model) {
        int valueToValidate = model.length();
        ValidationHelpers.validateIntRange(valueToValidate, MODEL_NAME_LEN_MIN, MODEL_NAME_LEN_MAX
                , MODEL_NAME_LEN_ERR);
    }

    protected void validatePrice(double price) {

        ValidationHelpers.validateDecimalRange(price, PRICE_VAL_MIN, PRICE_VAL_MAX
                , PRICE_VAL_ERR);
    }

    abstract String printAdditionalInfo();

    private String printComments() {

        StringBuilder sb = new StringBuilder();


        if (comments.isEmpty()) {
            sb.append(NO_COMMENTS_HEADER);
            sb.append(String.format("%s\n", COMMENTS_HEADER));
        } else {
            for (Comment comment1 : comments) {
                sb.append("----------");
                sb.append(String.format("%s\n", comment1.getContent()));
                sb.append(String.format("%s\n", comment1.getAuthor()));
                sb.append("----------");
            }
        }
        return sb.toString();
    }

    @Override
    public String toString() {

        int count = 1;
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("%s\n", getType()));
        sb.append(String.format("Make: %s\n", getMake()));
        sb.append(String.format("Model: %s\n", getModel()));
        sb.append(String.format("Wheels: %d\n", getWheels()));
        sb.append(String.format("Price: $%s\n", FormattingHelpers.removeTrailingZerosFromDouble(getPrice())));
        sb.append(printAdditionalInfo());

        sb.append(printComments());
        return sb.toString();
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }
}
