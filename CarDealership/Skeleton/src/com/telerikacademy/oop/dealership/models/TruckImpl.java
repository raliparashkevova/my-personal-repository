package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Truck;
import com.telerikacademy.oop.dealership.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import static java.lang.String.format;

public class TruckImpl extends BaseVehicle implements Truck {
    public static final int WHEELS_COUNT = 8;
    public static final int WEIGHT_CAP_MIN = 1;
    public static final int WEIGHT_CAP_MAX = 100;
    private static final String WEIGHT_CAP_ERR = format(
            "Weight capacity must be between %d and %d!",
            WEIGHT_CAP_MIN,
            WEIGHT_CAP_MAX);

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(VehicleType.TRUCK, make, model, WHEELS_COUNT, price);
        setWeightCapacity(weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    private void setWeightCapacity(int weightCapacity) {
        validateWeightCapacity(weightCapacity);
        this.weightCapacity = weightCapacity;
    }

    protected void validateWeightCapacity(int weightCapacity) {
        ValidationHelpers.validateIntRange(weightCapacity, WEIGHT_CAP_MIN, WEIGHT_CAP_MAX, WEIGHT_CAP_ERR);
    }

    @Override
    String printAdditionalInfo() {
        return String.format("Weight Capacity: %dt\n", getWeightCapacity());
    }

}
