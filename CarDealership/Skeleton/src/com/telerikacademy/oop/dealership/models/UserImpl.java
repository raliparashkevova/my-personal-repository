package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.models.enums.UserRole;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.dealership.models.enums.UserRole.NORMAL;
import static java.lang.String.format;

public class UserImpl implements User {

    public static final int USERNAME_LEN_MIN = 2;
    public static final int USERNAME_LEN_MAX = 20;
    private static final String USERNAME_REGEX_PATTERN = "^[A-Za-z0-9]+$";
    private static final String USERNAME_PATTERN_ERR = "Username contains invalid symbols!";
    private static final String USERNAME_LEN_ERR = format(
            "Username must be between %d and %d characters long!",
            USERNAME_LEN_MIN,
            USERNAME_LEN_MAX);
    private final static String USER_HEADER = "--USER %s--";
    public static final int PASSWORD_LEN_MIN = 5;
    public static final int PASSWORD_LEN_MAX = 30;
    private static final String PASSWORD_REGEX_PATTERN = "^[A-Za-z0-9@*_-]+$";
    private static final String PASSWORD_PATTERN_ERR = "Password contains invalid symbols!";
    private static final String PASSWORD_LEN_ERR = format(
            "Password must be between %s and %s characters long!",
            PASSWORD_LEN_MIN,
            PASSWORD_LEN_MAX);

    public static final int LASTNAME_LEN_MIN = 2;
    public static final int LASTNAME_LEN_MAX = 20;
    private static final String LASTNAME_LEN_ERR = format(
            "Lastname must be between %s and %s characters long!",
            LASTNAME_LEN_MIN,
            LASTNAME_LEN_MAX);

    public static final int FIRSTNAME_LEN_MIN = 2;
    public static final int FIRSTNAME_LEN_MAX = 20;
    private static final String FIRSTNAME_LEN_ERR = format(
            "Firstname must be between %s and %s characters long!",
            FIRSTNAME_LEN_MIN,
            FIRSTNAME_LEN_MAX);

    private final static String NOT_AN_VIP_USER_VEHICLES_ADD = "You are not VIP and cannot add more than %d vehicles!";
    private final static String ADMIN_CANNOT_ADD_VEHICLES = "You are an admin and therefore cannot add vehicles!";
    private static final int VIP_MAX_VEHICLES_TO_ADD = 5;

    private static final String YOU_ARE_NOT_THE_AUTHOR = "You are not the author of the comment you are trying to remove!";
    private final static String USER_TO_STRING = "Username: %s, FullName: %s %s, Role: %s";
    private final static String NO_VEHICLES_HEADER = "--NO VEHICLES--";

    private static final int NORMAL_ROLE_VEHICLE_LIMIT = 5;

    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private final UserRole userRole;
    private final List<Vehicle> vehicles;

    public UserImpl(String userName, String firstName, String lastName, String password, UserRole userRole) {
        setUserName(userName);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        this.userRole = userRole;
        this.vehicles = new ArrayList<>();
    }


    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public UserRole getRole() {
        return userRole;
    }

    private void setUserName(String userName) {
        int value = userName.length();
        ValidationHelpers.validateIntRange(value, USERNAME_LEN_MIN, USERNAME_LEN_MAX, USERNAME_LEN_ERR);
        ValidationHelpers.validatePattern(userName, USERNAME_REGEX_PATTERN, USERNAME_PATTERN_ERR);
        this.userName = userName;
    }

    private void setFirstName(String firstName) {
        int value = firstName.length();
        ValidationHelpers.validateIntRange(value, FIRSTNAME_LEN_MIN, FIRSTNAME_LEN_MAX, FIRSTNAME_LEN_ERR);
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        int value = lastName.length();

        ValidationHelpers.validateIntRange(value, LASTNAME_LEN_MIN, LASTNAME_LEN_MAX, LASTNAME_LEN_ERR);
        this.lastName = lastName;
    }

    private void setPassword(String password) {
        int value = password.length();
        ValidationHelpers.validatePattern(password, PASSWORD_REGEX_PATTERN, PASSWORD_PATTERN_ERR);
        ValidationHelpers.validateIntRange(value, PASSWORD_LEN_MIN, PASSWORD_LEN_MAX, PASSWORD_LEN_ERR);

        this.password = password;
    }


    @Override
    public List<Vehicle> getVehicles() {
        return new ArrayList<>(vehicles);
    }

    @Override
    public void addVehicle(Vehicle vehicle) {
        switch (userRole) {
            case NORMAL:
                if (vehicles.size() == VIP_MAX_VEHICLES_TO_ADD) {
                    throw new IllegalArgumentException(String.format(NOT_AN_VIP_USER_VEHICLES_ADD, VIP_MAX_VEHICLES_TO_ADD));
                }
                vehicles.add(vehicle);
                break;
            case VIP:
                vehicles.add(vehicle);
                break;
            case ADMIN:
                throw new IllegalArgumentException(ADMIN_CANNOT_ADD_VEHICLES);


        }
    }

    @Override
    public void removeVehicle(Vehicle vehicle) {
        getVehicles().remove(vehicle);
    }

    @Override
    public void addComment(Comment commentToAdd, Vehicle vehicleToAddComment) {

        vehicleToAddComment.addComment(commentToAdd);
    }

    @Override
    public void removeComment(Comment commentToRemove, Vehicle vehicleToRemoveComment) {

        if (commentToRemove.getAuthor().equals(userName)) {
            vehicleToRemoveComment.removeComment(commentToRemove);
        }


        throw new IllegalArgumentException("You are not the author of the comment you are trying to remove!");

    }

    @Override
    public String printVehicles() {


        StringBuilder sb = new StringBuilder();
        int count = 1;
        sb.append(String.format(USER_HEADER, userName));
        if (vehicles.isEmpty()) {
            sb.append(NO_VEHICLES_HEADER);
            sb.append(System.lineSeparator());
        } else {
            for (Vehicle vehicle : vehicles) {
                sb.append(String.format("%d. %s", count++, vehicle.getType()));
                sb.append(vehicle);
            }
        }
        return sb.toString();
    }

    @Override
    public boolean isAdmin() {

        String roleValue = String.valueOf(getRole());
        return roleValue.equals("ADMIN");
    }

    @Override
    public String toString() {

        return String.format(USER_TO_STRING, getUsername(), getFirstName(), getLastName(), getRole());
    }
}
