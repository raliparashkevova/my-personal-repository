public class BinaryRepresentationOfInteger {

    public static void main(String[] args) {


        int first = 0;
        int second = 0;

        int result = solution(first, second);
        System.out.println(result);
    }

    private static int solution(int A, int B) {

        int multiplication = A * B;

        if (multiplication == 0) {
            return 0;
        }


        int count = 0;
        while (multiplication > 0) {
            int remainder = multiplication % 2;

            if (remainder == 1) {
                count++;
            }
            multiplication /= 2;

        }




        return count;
    }
}
