import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParkingBill {

    public static final int ENTRANCE_FEE = 2;
    public static final int FIRST_HOUR_FEE = 3;
    public static final int NEXT_HOUR_FEE = 4;
    public static final int ALLOWED_TIME = 60;
    public static void main(String[] args)  {


        String firstTime = "rr:42";

        String secondTime = "11:42";

        int totalCost = solution(firstTime, secondTime);


        System.out.println(totalCost);



    }

    private static int solution(String E, String L) {
        long firstHours = 0;
        long secondHours = 0;

        try {

            Date firstDate = new SimpleDateFormat("HH:mm").parse(E);
            Date secondDate = new SimpleDateFormat("HH:mm").parse(L);
            firstHours = firstDate.getTime();
            secondHours = secondDate.getTime();

        }
        catch (ParseException e) {
            //Handle exception here

            System.out.println("Invalid hour or minutes input parameters" + e.getMessage());
//            e.printStackTrace();
        }

        long differenceInMiliSecond = secondHours - firstHours;
        long differenceInHours = (differenceInMiliSecond / (60 *60*1000)) % 24;
        long differenceInMinutes = (differenceInMiliSecond / (60*1000)) % 60;

        long hourInMinute = differenceInHours * 60 + differenceInMinutes;


        double sum = ENTRANCE_FEE;



        if (hourInMinute <= ALLOWED_TIME){
            sum= sum+FIRST_HOUR_FEE;
        } else {
            sum = sum + FIRST_HOUR_FEE;
            hourInMinute = hourInMinute -60;
            double restedTime = Math.ceil(hourInMinute*1.00 / 60);
            sum = sum + (restedTime * NEXT_HOUR_FEE);
        }
        return (int) sum;
    }
}
