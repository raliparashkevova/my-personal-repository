import java.util.*;

public class MedianOfTwoSortedArray {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int [] nums1 = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int [] nums2 = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        int median = getMedianFromTwoSortedArray(nums1,nums2);

    }

    private static int getMedianFromTwoSortedArray(int[] nums1, int[] nums2) {

        int [] result = new int[nums1.length+ nums2.length];

        System.arraycopy(nums1, 0, result, 0, result.length);

        System.arraycopy(nums2, 0, result, 0, result.length);

        Arrays.sort(result);

        if (result.length % 2 == 0){
            
        }

return 0;
    }
}
