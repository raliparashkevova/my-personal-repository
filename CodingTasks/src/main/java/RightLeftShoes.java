import java.util.ArrayDeque;
import java.util.List;
import java.util.Stack;

public class RightLeftShoes {

    public static final char LEFT_SHOES ='L';

    public static final char RIGHT_SHOES ='R';
    public static void main(String[] args) {

        String input = "RLRRLLRLRRLL";
        int result = solution(input);
        System.out.println(result);


    }

    private static int solution(String S) {
        int employeeCount = 0;
        char []  shoes = S.toCharArray();

        int rightCount = S.split("R",-1).length;



        int leftCount = 0;


        for (char shoe : shoes) {

            if (shoe == RIGHT_SHOES) {
                rightCount++;
            } else if (shoe == LEFT_SHOES){
                leftCount++;
            }

            if (rightCount == leftCount) {
                employeeCount++;
            }
        }


        return employeeCount;
    }
}
