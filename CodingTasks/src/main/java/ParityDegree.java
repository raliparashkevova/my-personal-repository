public class ParityDegree {

    public static void main(String[] args) {

        int input = 24;



        int result = solution(input);
    }

    private static int solution(int N) {

        int result = 0;
        for (int i = 0; i < N/2; i++) {
            int num = calculatePower(i);

            if (N % num == 0){
                result = i;
            }
        }

        return result;
    }

    private static int calculatePower(int i) {
        double number = Math.pow(2, i);


        return (int) number;
    }
}
