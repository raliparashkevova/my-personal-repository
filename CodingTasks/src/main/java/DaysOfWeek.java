public class DaysOfWeek {

    public static final String []  DAYS_OF_WEEK = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
    private static final int NUMBER_DAYS_OF_WEEK = 7;
    public static void main(String[] args) {

       
        String day = "Mon";
        int dayCount = 500;
        
        String result = soulution(day,dayCount);
        System.out.println(result);
        


    }

    private static String soulution(String S, int K) {

        int indexOfGivenDay = getIndexOfGivenDay(S, DAYS_OF_WEEK);

        int indexOfGivenDayPlusK =  (indexOfGivenDay + K) % NUMBER_DAYS_OF_WEEK;


        return DAYS_OF_WEEK[indexOfGivenDayPlusK];
    }

    private static int getIndexOfGivenDay(String s, String[] daysOfWeek) {
        for (int i = 0; i < daysOfWeek.length; i++) {
            if (daysOfWeek[i].equals(s)){
                return i;
            }
        }
        return -1;
    }
}
