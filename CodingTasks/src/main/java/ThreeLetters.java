public class ThreeLetters {

    public static final String FIRST_LETTER = "a";
    public static final String SECOND_LETTER = "b";

    public static void main(String[] args) {

        int firstNumber = 5;
        int secondNumber = 3;

        String result = solution(firstNumber, secondNumber);

        System.out.println(result);

    }

    private static String solution(int A, int B) {

        StringBuilder sb = new StringBuilder();

        while (A > 0 || B > 0) {

            boolean writeA = false;
            boolean writeB = false;

            int size = sb.length();

            if (size >= 2 && sb.charAt(size - 1) == 'b' && sb.charAt(size - 2) == 'b') {
                writeA = true;
            } else {

                if (A > B) {
                    writeA = true;
                }
            }
            if (writeA) {
                A--;
                sb.append(FIRST_LETTER);

            } else {
                B--;
                sb.append(SECOND_LETTER);

            }
        }


        return sb.toString();
    }


}
