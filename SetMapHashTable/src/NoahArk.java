import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class NoahArk {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int numberOfLines = Integer.parseInt(scanner.nextLine());

        Map<String, Integer> animals = new TreeMap<>();

        while (numberOfLines-- > 0) {
            String animal = scanner.nextLine();
            if (!animals.containsKey(animal)) {
                animals.put(animal, 1);
            } else {
                animals.put(animal, animals.get(animal) + 1);

            }

        }
        for (Map.Entry<String, Integer> outputAnimal : animals.entrySet()) {

            if (outputAnimal.getValue() % 2 == 0) {
                System.out.printf("%s %d Yes%n", outputAnimal.getKey(), outputAnimal.getValue());
            } else {
                System.out.printf("%s %d No%n", outputAnimal.getKey(), outputAnimal.getValue());
            }
        }

    }
}
