import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.sort;

public class JustCount {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        Map<Character, Integer> specialCharacters = new HashMap<>();
        Map<Character, Integer> lowerLetters = new HashMap<>();
        Map<Character, Integer> upperLetters = new HashMap<>();
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (Character.isLowerCase(ch)) {
                if (!lowerLetters.containsKey(ch)) {
                    lowerLetters.put(ch, 1);
                } else {
                    lowerLetters.put(ch, lowerLetters.get(ch) + 1);
                }
            } else if (Character.isUpperCase(ch)) {
                if (!upperLetters.containsKey(ch)) {
                    upperLetters.put(ch, 1);
                } else {
                    upperLetters.put(ch, upperLetters.get(ch) + 1);
                }
            } else {
                if (!specialCharacters.containsKey(ch)) {
                    specialCharacters.put(ch, 1);
                } else {
                    specialCharacters.put(ch, specialCharacters.get(ch) + 1);
                }
            }
        }
        printMaxSymbols(specialCharacters);
        printMaxSymbols(lowerLetters);
        printMaxSymbols(upperLetters);
    }

    public static void printMaxSymbols(Map<Character, Integer> symbols) {
        if (symbols.isEmpty()) {
            System.out.println("-");
            return;
        }
        int maxValueInSymbols = (Collections.max(symbols.values()));
        List<Character> maxSymbols = new ArrayList<>();
        for (Map.Entry<Character, Integer> entry : symbols.entrySet()) {
            if (entry.getValue() == maxValueInSymbols) {
                maxSymbols.add(entry.getKey());
            }
        }
        sort(maxSymbols);
        char symbol = maxSymbols.get(0);

        System.out.printf("%c %d%n", symbol, maxValueInSymbols);


    }
}