import java.util.*;
import java.util.stream.Collectors;

public class GottaCatch {
    static Map<Pokemon, Integer> pocketPosition = new HashMap<>();
    static List<Pokemon> pokemonList = new ArrayList<>();
    static HashMap<String, Set<Pokemon>> pokemonByType = new HashMap<>();
    static int start = 0;
    static int end = 0;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String type = "";

        while (!"end".equals(input)) {
            List<String> lines = Arrays.stream(input.split(" "))
                    .collect(Collectors.toList());

            if (lines.size() == 5) {

                String name = lines.get(1);

                if (lines.get(2).length() >= 1 && lines.get(2).length() <= 10) {
                    type = lines.get(2);
                }
                int power = Integer.parseInt(lines.get(3));


                Pokemon pokemonToAdd = new Pokemon(name, power);
                //names.add(itemName);
                pokemonList.add(pokemonToAdd);
                pokemonByType.putIfAbsent(type, new TreeSet<>());
                pokemonByType.get(type).add(pokemonToAdd);

                int position = Integer.parseInt(lines.get(4));

                if (pocketPosition.size() == 0) {
                    pocketPosition.put(pokemonToAdd, 1);
                } else {

                    movePosition(position, pokemonToAdd);
                }


                System.out.println(String.format("Added pokemon %s to position %s", name, position));
            } else if (lines.size() == 2) {
                String pokemonType = lines.get(1);

                if (!pokemonByType.containsKey(type)) {
                    System.out.println(String.format("POKEMON_TYPE: %s", type));
                } else {
                    int count1 = 0;
                    StringBuilder sb = new StringBuilder();

                    sb.append(String.format("Type %s: ", type)).append(pokemonByType.get(type).stream()
                            .limit(5)

                            .map(Pokemon::toString)

                            .collect(Collectors.joining("; ", "", "")));

                    System.out.println(sb);

                }

            } else if (lines.size() == 3) {

                if (Integer.parseInt(lines.get(1)) >= 1 && Integer.parseInt(lines.get(1)) <= pokemonList.size()) {
                    start = Integer.parseInt(lines.get(1));
                }
                if (Integer.parseInt(lines.get(2)) >= 1
                        && Integer.parseInt(lines.get(2)) <= pokemonList.size() && Integer.parseInt(lines.get(2)) >= start) {
                    end = Integer.parseInt(lines.get(2));

                }
                int count = 0;
                List<Pokemon> pokemonListResult = new ArrayList<>();
                for (int i = start; i < end; i++) {
                    pokemonListResult.add(pokemonList.get(i));
                }
                String output = pokemonListResult.stream()
                        .map(Pokemon::toString)
                        .collect(Collectors.joining("; ", "", ""));
                //  String output = pocketPosition.entrySet().stream()
                //          .filter(v -> v.getValue() >= finalStart && v.getValue() <= finalEnd)
                //          .map(p -> p.getKey().toString())
                //          .collect(Collectors.joining("; ", "", ""));
//
                String[] result = output.split("; ");
                int count1 = 0;
                StringBuilder sb = new StringBuilder();

                for (String s : result) {
                    sb.append(String.format("%d. ", ++count1)).append(s).append("; ");
                }
                // sb.append(System.lineSeparator());
                System.out.println(sb.deleteCharAt(sb.length() - 2));

            }


            input = scanner.nextLine();


        }
    }

    private static void movePosition(int position, Pokemon pokemon) {

        // ако позицията на новият покемон е по-голяма няма влезе в проверката и направо ще се добави , 87 ред
        // ако позицията е в рамките на съществуващите, тези които са по-големи или равни на тази позиция се отместват
        //позицията остава празна и се добавя новият покемон
        if (position <= pocketPosition.size()) {
            for (Map.Entry<Pokemon, Integer> pokemonIntegerEntry : pocketPosition.entrySet()) {

                if (pokemonIntegerEntry.getValue() >= position) {
                    int value = pokemonIntegerEntry.getValue() + 1;
                    pocketPosition.put(pokemonIntegerEntry.getKey(), value);
                }


            }
        }

        pocketPosition.put(pokemon, position);
    }

    private static class Pokemon implements Comparable<Pokemon> {

        private String name;
        private int power;


        public Pokemon(String name, int power) {
            this.name = name;
            this.power = power;

        }

        public String getName() {
            return name;
        }

        public int getPower() {
            return power;
        }


        //     @Override
        //     public boolean equals(Object o) {
        //         if (this == o) return true;
        //         if (o == null || getClass() != o.getClass()) return false;
        //       Pokemon item = (Pokemon) o;
        //      return Objects.equals(name, item.name)  &&   Integer.compare(item.power, power) == 0 ;
        //     }

        @Override
        public int hashCode() {
            return Objects.hash(name, power);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pokemon pokemon = (Pokemon) o;
            return false;
        }

        private void setName(String name) {
            if (name.length() >= 1 && name.length() <= 20) {
                this.name = name;
            }
        }

        private void setPower(int power) {
            if (power >= 10 && power <= 50) {
                this.power = power;
            }

        }


        @Override
        public int compareTo(Pokemon item) {
            return Comparator.comparing(Pokemon::getName)
                    .thenComparing(p -> this.power - p.power)
                    .compare(this, item);
        }


        public String toString() {
            return String.format("%s(%d)", name, power);
        }


    }
}
