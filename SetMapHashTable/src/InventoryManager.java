import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InventoryManager {

    static HashSet<String> names = new HashSet<>();
    static TreeSet<Item> inventory = new TreeSet<>();
    static HashMap<String, TreeSet<Item>> itemByType = new HashMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();


        while (!"end".equals(input)) {
            List<String> lines = Arrays.stream(input.split(" "))
                    .collect(Collectors.toList());


            if (lines.size() == 4 && !lines.contains("by")) {
                String itemName = lines.get(1);
                double itemPrice = Double.parseDouble(lines.get(2));
                String itemType = lines.get(3);


                if (names.contains(itemName)) {
                    System.out.printf("Error: Item %s already exists%n", itemName);

                } else {
                    Item itemToAdd = new Item(itemName, itemPrice);
                    names.add(itemName);
                    inventory.add(itemToAdd);
                    itemByType.putIfAbsent(itemType, new TreeSet<>());
                    itemByType.get(itemType).add(itemToAdd);
                    System.out.printf("Ok: Item %s added successfully%n", itemName);
                }
            } else if (lines.size() == 4 && lines.contains("by")) {
                String type = lines.get(3);

                if (!itemByType.containsKey(type)) {
                    System.out.println(String.format("Error: Type %s does not exist", type));
                } else {
                    System.out.println(itemByType.get(type).stream()
                            .limit(10)
                            .map(Item::toString)
                            .collect(Collectors.joining(", ", "Ok: ", "")));
                }


            } else if (lines.size() == 7 && lines.contains("by")) {

                double minPrice = Double.parseDouble(lines.get(4));
                double maxPrice = Double.parseDouble(lines.get(6));

                System.out.println(inventory.stream()
                        .filter(product -> product.getPrice() > minPrice && product.getPrice() < maxPrice)
                        .limit(10)
                        .map(Item::toString)
                        .collect(Collectors.joining(", ", "Ok: ", "")));

            } else if (lines.size() == 5) {

                if (lines.get(3).equals("from")) {
                    double minPrice = Double.parseDouble(lines.get(4));

                    System.out.println(inventory.stream()
                            .filter(product -> product.getPrice() >= minPrice)
                            .limit(10)
                            .map(Item::toString)
                            .collect(Collectors.joining(", ", "Ok: ", "")));
                } else {
                    double maxPrice = Double.parseDouble(lines.get(4));

                    System.out.println(inventory.stream()
                            .filter(product -> product.getPrice() <= maxPrice)
                            .limit(10)
                            .map(Item::toString)
                            .collect(Collectors.joining(", ", "Ok: ", "")));
                }
            }


            input = scanner.nextLine();
        }
    }

    private static class Item implements Comparable<Item> {

        private String name;
        private double price;
        private String type;

        public Item(String name, Double price) {
            this.name = name;
            this.price = price;

        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public String getType() {
            return type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Item item = (Item) o;
            return Double.compare(item.price, price) == 0 && Objects.equals(name, item.name) && Objects.equals(type, item.type);
        }


        @Override
        public int compareTo(Item item) {
            return Comparator.comparing(Item::getPrice)
                    .thenComparing(Item::getName)
                    .compare(this, item);
        }

        @Override
        public String toString() {
            return String.format("%s(%.2f)", name, price);
        }
    }
}