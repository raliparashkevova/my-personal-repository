import java.util.Scanner;
import java.util.Stack;

public class ValidParentheses {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        Stack<Character> stack = new Stack<>();


        boolean isCorrect = isValid(input);

        System.out.println(isCorrect);
    }

    private static boolean isValid(String input) {
        boolean isMatched = false;
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < input.length(); i++) {
            char currentSymbol = input.charAt(i);
            if (currentSymbol == '(' || currentSymbol == '{' || currentSymbol == '[') {
                stack.push(currentSymbol);
            } else {
                if (stack.isEmpty()) {
                    isMatched = false;
                    break;
                }
                char lastElement = stack.peek();
                if ((currentSymbol == ')' && lastElement == '(')
                        || (currentSymbol == '}' && lastElement == '{')
                        || (currentSymbol == ']' && lastElement == '[')) {
                    isMatched = true;
                    stack.pop();
                } else {
                    stack.pop();
                    isMatched = false;
                    break;
                }


            }

        }
        return isMatched;
    }
}
