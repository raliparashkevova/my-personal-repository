import java.util.*;
import java.util.stream.Collectors;

public class MiddleOfLinkedList {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        LinkedList<Integer> linkedList = Arrays.stream(scanner.nextLine().split(","))
                .map(Integer::parseInt).collect(Collectors.toCollection(LinkedList::new));


        int size = linkedList.size();
        List<Integer> result;
        int middleIndex;
        if (size % 2 == 0) {

            middleIndex = size / 2;
            result = linkedList.subList(middleIndex, linkedList.size());

        } else {
            middleIndex = (size / 2) + 1;
            result = linkedList.subList(middleIndex - 1, linkedList.size());
        }


        System.out.println(result);

    }
}
