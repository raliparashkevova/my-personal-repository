import java.util.*;
import java.util.stream.Collectors;

public class Jumps {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        int[] numbers = Arrays.stream(scanner.nextLine()
                .split(" ")).mapToInt(Integer::parseInt).toArray();

        List<Integer> result = new ArrayList<>();

        int maxValue = 0;
        //   int jumps = 0;
        // int current = numbers[0];
        //     for (int i = 1; i < numbers.length; i++) {
//
        //         int next = numbers[i];
//
//
        //         if (current > next){
        //             jumps++;
        //             current = next;
        //             if (jumps > maxValue){
        //                 maxValue = jumps;
        //             }
        //         }
//
        //         result.add(maxValue);
        //     }


        int index = 0;

        while (index != numbers.length) {
            int jumps = 0;

            int next = numbers[index];
            for (int i = index; i < numbers.length; i++) {
                int current = numbers[i];

                if (current > next) {
                    next = current;
                    jumps++;
                    if (jumps > maxValue) {
                        maxValue = jumps;
                    }
                }
            }

            result.add(jumps);
            index++;
        }


        System.out.println(maxValue);
        System.out.println(result.stream().map(String::valueOf).collect(Collectors.joining(" ")));

    }
}