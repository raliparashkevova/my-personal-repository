import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StudentsOrder {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] inputSize = Arrays.stream(scanner.nextLine().split(" "))
                .mapToInt(Integer::parseInt).toArray();

        int numberOfStudents = inputSize[0];
        int numberOfStudentsCouple = inputSize[1];


        List<String> students = Arrays.stream(scanner.nextLine().split(" "))
                .collect(Collectors.toList());

        while (numberOfStudentsCouple-- > 0) {
            String[] names = scanner.nextLine().split(" ");

            String firstStudents = names[0];
            String secondStudents = names[1];


            students.remove(firstStudents);

            int indexOfSecondStudents = students.indexOf(secondStudents);

            students.add(indexOfSecondStudents, firstStudents);


        }
        System.out.println(String.join(" ", students));


    }
}
