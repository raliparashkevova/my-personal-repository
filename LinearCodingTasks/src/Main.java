import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        Stack<Character> stack = new Stack<>();


        boolean isCorrect = isValid(input);

        System.out.println(isCorrect);
    }

    private static boolean isValid(String input) {
        boolean isMatched = false;
        Stack<Character> stack = new Stack<>();


        for (int i = 0; i < input.length(); i++) {
            char currentSymbol = input.charAt(i);
            if (currentSymbol == '(' || currentSymbol == '{' || currentSymbol == '[' || currentSymbol == ')'
                    || currentSymbol == '}' || currentSymbol == ']') {
                stack.push(currentSymbol);
            }
        }
        if (stack.isEmpty()) {
            isMatched = false;

        } else {

            for (int i = 0; i < stack.size(); i++) {
                char firstSymbol = stack.get(i);
                char lastElement = stack.peek();
                if (firstSymbol == '{' && lastElement == '}') {
                    isMatched = true;
                    stack.pop();

                } else if (firstSymbol == '[' && lastElement == ']') {
                    isMatched = true;
                    stack.pop();

                } else if (firstSymbol == '(' && lastElement == ')') {
                    isMatched = true;
                    stack.pop();

                } else {
                    isMatched = false;
                    break;
                }
            }
        }

        return isMatched;
    }
}
