import java.util.*;

public class HdnlToy {
    private static StringBuilder result = new StringBuilder();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int n = Integer.parseInt(scanner.nextLine());

        List<String> tags = new ArrayList<>();
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            String input = scanner.nextLine();

            StringBuilder sb = new StringBuilder();
            sb.append(input);
            int number = Integer.parseInt(sb.deleteCharAt(0).toString());
            tags.add(input);
            numbers.add(number);
        }

        Stack<Tag> stack = new Stack<>();
        int currentValue = numbers.get(0);
        int nestedLevel = 0;
        printOpenTag(nestedLevel, tags.get(0));
        stack.push(new Tag(tags.get(0), numbers.get(0), nestedLevel));
        for (int i = 1; i < n; i++) {
            if (numbers.get(i) > currentValue) {
                currentValue = numbers.get(i);
                nestedLevel++;
                printOpenTag(nestedLevel, tags.get(i));
                stack.push(new Tag(tags.get(i), numbers.get(i), nestedLevel));
            } else {
                currentValue = numbers.get(i);
                while (!stack.isEmpty() && currentValue <= stack.peek().value) {
                    Tag newTag = stack.pop();
                    printCloseTag(newTag.nestedLevel, newTag.tag);
                    if (currentValue < newTag.value) {
                        nestedLevel--;
                        nestedLevel = Math.max(nestedLevel, 0);
                    }
                }
                printOpenTag(nestedLevel, tags.get(i));
                stack.push(new Tag(tags.get(i), numbers.get(i), nestedLevel));
            }
        }
        while (!stack.isEmpty()) {
            Tag newTag = stack.pop();
            printCloseTag(newTag.nestedLevel, newTag.tag);
        }

        System.out.println(result);


    }

    private static void printCloseTag(int nestedLevel, String tag) {
        String spaces = String.join("", Collections.nCopies(nestedLevel, " "));
        result.append(spaces).append("<").append("/").append(tag).append(">").append("\n");
    }

    private static void printOpenTag(int nestedLevel, String tag) {
        String spaces = String.join("", Collections.nCopies(nestedLevel, " "));
        result.append(spaces).append("<").append(tag).append(">").append("\n");
    }

    private static class Tag {
        public String tag;
        public int value;
        public int nestedLevel;

        public Tag(String tag, int value, int nestedLevel) {
            this.tag = tag;
            this.value = value;
            this.nestedLevel = nestedLevel;
        }
    }
}