import java.util.Scanner;
import java.util.Stack;


public class BackspaceStringCompare {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();

        boolean isEquals = isEqualsCharactersAfterApplyingRules(first, second);
        System.out.println(isEquals);
    }

    private static boolean isEqualsCharactersAfterApplyingRules(String first, String second) {
        Stack<Character> firstStack = applyingRules(first);
        Stack<Character> secondStack = applyingRules(second);

        return firstStack.equals(secondStack);
    }

    private static Stack<Character> applyingRules(String first) {
        Stack<Character> firstStack = new Stack<>();
        for (int i = 0; i < first.length(); i++) {
            char symbol = first.charAt(i);

            if (symbol == '#' && !firstStack.empty()) {
                firstStack.pop();
            }
            if (symbol >= 97 && symbol <= 122) {
                firstStack.push(symbol);
            }
        }
        return firstStack;
    }
}
