import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MergeTwoSortedList {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        LinkedList<Integer> first = Arrays.stream(scanner.nextLine().split(","))
                .map(Integer::parseInt).collect(Collectors.toCollection(LinkedList::new));
        LinkedList<Integer> second = Arrays.stream(scanner.nextLine().split(","))
                .map(Integer::parseInt).collect(Collectors.toCollection(LinkedList::new));

        first.addAll(second);

        Collections.sort(first);

        System.out.println(first);
    }
}
