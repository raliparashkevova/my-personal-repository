import java.util.*;

public class BaseballGame {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(", ");

        Stack<Integer> stack = new Stack<>();

        for (String symbol : input) {

            if (symbol.equals("C")) {
                stack.pop();
            } else if (symbol.equals("D")) {
                int number = stack.peek();
                number = number * 2;
                stack.push(number);
            } else if (symbol.equals("+")) {
                int firstNumber = stack.peek();
                int secondNumber = stack.get(stack.size() - 2);
                int sum = firstNumber + secondNumber;
                stack.push(sum);
            } else {
                stack.push(Integer.parseInt(symbol));
            }

        }
        int sum = 0;
        while (!stack.empty()) {

            sum += stack.pop();
        }

        System.out.println(sum);
    }
}
