import java.util.*;

public class Test {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        Person p1 = new Person("Pesho");
        Person p2 = new Person("Gosho");

        p1 = p2;

        System.out.println(p1);

    }

    private static class Person {

        public String name;


        public Person(String name) {
            this.name = name;
        }
    }
}
